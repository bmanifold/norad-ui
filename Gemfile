# frozen_string_literal: true

source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.1.4'
# Use webpack to manage frontend assets
gem 'webpacker', '~> 3.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4', group: :doc

gem 'haml-rails', '1.0.0'
gem 'her', '0.9.0'
gem 'request_store', '1.3.2'

# Database Stuff
gem 'pg', '0.21.0'

# Secrets management
gem 'dotenv-rails', '~> 2.2'

# Use Unicorn as the app server
gem 'unicorn', '5.3.1'

# Notify us of vulnerable gems in the CI pipeline
gem 'bundler-audit', '~> 0.5'

gem 'jbuilder', '~> 2.7'
gem 'js-routes', '~> 1.4'
gem 'uglifier', '~> 3.2'

group :development, :test do
  gem 'brakeman', '~> 4.0'
  gem 'byebug', '~> 9.0'
  gem 'capybara', '~> 2.13'
  gem 'capybara-select2', '~> 1.0'
  gem 'factory_girl_rails', '~> 4.8'
  gem 'fuubar', '~> 2.2'
  gem 'haml_lint', '~> 0.26'
  gem 'launchy', '~> 2.4'
  gem 'poltergeist', '~> 1.14'
  gem 'rspec-rails', '~> 3.5'
  gem 'rubocop', '~> 0.49'
  gem 'selenium-webdriver', '~> 3.5'
end

group :development do
  gem 'annotate', '~> 2.7'
  gem 'pry-rails', '~> 0.3'
  gem 'web-console', '~> 3.5'
end

group :production do
  gem 'turnout', '2.4.1'

  # Add Errbit support
  gem 'airbrake', '~> 6.1'

  gem 'lograge', '~> 0.4'
  gem 'logstash-event', '~> 1.2'
end
