# Norad UI

This project is the Web UI that consumes the Norad API.

## Status

[![build status](https://gitlab.com/norad/ui/badges/master/build.svg)](https://gitlab.com/norad/ui/commits/master)
[![coverage report](https://gitlab.com/norad/ui/badges/master/coverage.svg)](https://gitlab.com/norad/ui/commits/master)

## Getting Started

The easiest way to get up and running is to visit the [Norad Dev Box](https://gitlab.com/norad/dev-box) Repo and follow the instructions there.

## Running the tests

The best way to run tests at the moment is inside the [Norad Dev Box](https://gitlab.com/norad/dev-box).  The Norad API
code has a built in Rake task to prepare the API for running the UI tests. It
also has dedicated environment to use when running the UI's tests against it:
`client_test`.

To prepare the API the following command is recommended.

```shell
$> RAILS_ENV=client_test rake db:reset
```

After the API is prepared you'll need to create the test DB in the UI repo:

```shell
$> RAILS_ENV=test rake db:reset
```

Next, start the API server in client_test mode as follows:

```shell
$> RAILS_ENV=client_test rails s -b 0.0.0.0
```

Finally, ensure selenium is running on your dev-box if it is not already:

```shell
$> bin/selenium-chrome
```

Then you should be able to run the UI tests by issuing the following command in the UI repo:

```shell
$> bin/test
```

## Testing Airbrake
Airbrake can be tested from the dev-box, but it must be tested in the
production environment. To test Airbrake from the dev-box, do the following:

1. Create a .env.production file if it does not exist.
2. Add the following environment variables to .env.production.  
   WARNING: Do not use the api key for the production Norad API or Norad UI.
   For testing, please use the Norad DevBox key so that everyone does not receive
   Airbrake notifications that appear to be production problems. Email
   notifications are not sent for the Norad DevBox, but they will be for the
   Norad API and Norad UI.

        ERRBIT_URL=http://localhost:8080
        AIRBRAKE_API_KEY=<The correct Norad DevBox key.>

3. When sshing to the dev-box from the host, use the following command:

        vagrant ssh -- -R 8080:localhost:8080

4. On the machine hosting the dev-box, run the following command:

        ssh -N -L 8080:192.168.2.36:8080 -o UserKnownHostsFile=~/.ssh/known_hosts.norad.prod -o IdentityFile=~/.ssh/id_rsa -o HostKeyAlias=jumphost.norad.prod errbit-user@128.107.32.180 

5. Run the following command form the ui root directory in the dev-box:

        RAILS_ENV=production bundle exec rake airbrake:test

To watch the message on the dev-box or host, use the following command:

```shell
sudo tcpdump -As0 -ni any port 8080
```

## Built With

* [Ruby on Rails](http://rubyonrails.org/)
* [Bootstrap](https://getbootstrap.com/)
* [AdminLTE](https://github.com/almasaeed2010/AdminLTE)
* [Her](https://github.com/remiprev/her)

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details
