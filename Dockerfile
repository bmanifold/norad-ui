############################################################
# Dockerfile for Norad UI
############################################################

FROM ruby:2.4.2

# File Authors / Maintainers
LABEL authors="Blake Hitchcock, Brian Manifold, Roger Seagle"

ARG NORAD_UI_VERSION_CI
ENV NORAD_UI_VERSION=${NORAD_UI_VERSION_CI}

# Update the system & add essential libs and programs
RUN apt-get update \
&& apt-get install -y --no-install-recommends \
  apt-transport-https \
  build-essential \
  libgmp-dev \
  postgresql-client \
  software-properties-common \
  vim \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
&& apt-get clean autoclean -y \
&& apt-get autoremove -y

# Install NodeJS and Yarn
RUN curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - \
&& echo "deb https://deb.nodesource.com/node_6.x jessie main" > /etc/apt/sources.list.d/nodesource.list \
&& echo "deb-src https://deb.nodesource.com/node_6.x jessie main" >> /etc/apt/sources.list.d/nodesource.list \
&& curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
&& echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
&& apt-get update \
&& apt-get install -y nodejs yarn \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
&& apt-get clean autoclean -y \
&& apt-get autoremove -y

RUN wget https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64.deb \
  && dpkg -i dumb-init_*.deb

# Create directory where app will live
ENV APP_PATH /norad/ui
RUN mkdir -p $APP_PATH
WORKDIR $APP_PATH

RUN mkdir -p $APP_PATH/tmp && chmod 777 $APP_PATH/tmp \
  && mkdir -p $APP_PATH/log && chmod 777 $APP_PATH/log \
  && touch $APP_PATH/log/lograge_production.log && chmod 777 $APP_PATH/log/lograge_production.log

ENV BUNDLE_PATH /norad/ruby_gems
ENV WORKER_PROCESSES 1
ENV LISTEN_ON 0.0.0.0:8000

COPY Gemfile .
COPY Gemfile.lock .

ARG BUNDLE_DEPLOYMENT="--deployment"
ARG BUNDLE_WITHOUT="development:test:linters:security_auditors:client_test"

# Install Gems
RUN gem install bundler --no-document \
  && bundle install --jobs 5 --retry 5 $BUNDLE_DEPLOYMENT --without $BUNDLE_WITHOUT

# Copy the actual rails app to the image
COPY . .

EXPOSE 8000

# Precompile assets
RUN yarn install \
&& bundle exec rake ERRBIT_URL=https://errbit:8080 AIRBRAKE_API_KEY=123456789 NODE_ENV=production RAILS_ENV=production assets:precompile \
&& rm -rf node_modules \
&& chmod -R 755 /norad/ui/public

# Start the Norad UI
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["bundle", "exec", "unicorn", "-c", "config/unicorn.rb", "-E", "production"]
