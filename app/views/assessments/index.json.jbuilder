# frozen_string_literal: true

json.array!(@assessments.map(&:attributes))
