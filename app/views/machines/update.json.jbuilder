# frozen_string_literal: true

json.machine do
  json.id @machine.id
  json.ip @machine.ip
  json.name @machine.name
  json.description @machine.description
  json.fqdn @machine.fqdn
  json.status @machine.status
  json.use_fqdn_as_target @machine.use_fqdn_as_target
  json.scheduled_service_discovery @machine.scheduled_service_discovery
  json.has_services @machine.services.present?
end
