# frozen_string_literal: true

json.machine do
  json.id @machine_id
end
