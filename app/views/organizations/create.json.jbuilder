# frozen_string_literal: true

json.call(@org, :slug, :uid, :token, :machine_count)
