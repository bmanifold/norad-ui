# frozen_string_literal: true

json.call(@check, :updated_at, :id)
