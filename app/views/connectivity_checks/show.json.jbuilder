# frozen_string_literal: true

json.call(@check, :status, :updated_at)
