# frozen_string_literal: true

module ApplicationHelper
  def format_timestamp(timestamp)
    timestamp&.to_datetime&.strftime('%b %d, %Y @ %I:%M:%S %p %Z')
  end

  def titleize(input_string)
    input_string&.titleize
  end

  def nav_link_css_class(path)
    current_page?(path) ? 'active' : ''
  end

  def security_container_help_title(security_test)
    "#{security_test.titleize} Docs"
  end

  def relay_error_message(org)
    return unless org&.configuration_errors?
    relay_errors =
      org.organization_errors.select { |x| x.type == 'RelayOfflineError' }
    return if relay_errors.blank?
    relay_errors.map(&:message).join(' ')
  end

  def organization_errors_message(error_count = 0)
    "#{pluralize(error_count, 'error')} detected.  This organization may not be performing scans correctly."
  end
end
