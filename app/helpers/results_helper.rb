# frozen_string_literal: true

module ResultsHelper
  def result_ignore_form_for(parent)
    form_for([parent, ResultIgnoreRule.new],
             html: {
               class: 'ignore-result-form ignore-result-action',
               style: 'display: inline-block;'
             }) do
      yield
    end
  end

  def result_exporter_form_for
    form_for(ResultExporter.new,
             remote: true,
             html: {
               class: 'export-result-form export-result-action',
               style: 'display: inline-block;'
             }) do
      yield
    end
  end

  def ignored_or_status(result)
    result.ignored ? 'ignored' : result.status
  end

  def latest_results?(scan_id)
    scan_id.to_i.zero?
  end

  def ignore_comment_for(parent, result)
    parent_identifier = parent.is_a?(Organization) ? parent.slug : parent.name
    result_detail = "[#{security_container_header_string(result.assessment)}] -- #{result.title}"
    "Ignored from results view for #{parent.class} #{parent_identifier}: #{result_detail}"
  end

  def result_label_color(result)
    return 'bg-gray' if result.ignored
    case result.status
    when 'pass' then 'bg-green'
    when 'fail' then 'bg-red'
    when 'error' then 'bg-orange'
    else 'bg-aqua'
    end
  end

  def results_page_title(scan_id, assessments)
    return 'Results' if scan_id.zero? && assessments.blank?
    sid = scan_id.zero? ? assessments.first.docker_command_id : scan_id
    menu_title = "Scan #{sid}"
    menu_title += ' - (Latest)' if scan_id.zero?
    menu_title
  end
end
