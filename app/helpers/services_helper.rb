# frozen_string_literal: true

module ServicesHelper
  ENC_TYPES = {
    'cleartext' => 'None',
    'ssl' => 'SSL / TLS',
    'ssh' => 'SSH'
  }.freeze

  SVC_TYPES = {
    'GenericService' => 'Generic Service',
    'SshService' => 'SSH',
    'WebApplicationService' => 'Web Application'
  }.freeze

  def enc_type_display_name(enc_type)
    ENC_TYPES[enc_type]
  end

  def svc_type_display_name(svc_type)
    SVC_TYPES[svc_type]
  end
end
