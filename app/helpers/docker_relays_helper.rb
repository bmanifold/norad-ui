# frozen_string_literal: true

module DockerRelaysHelper
  # It's possible the 'outdated' flag on a Relay could be 'true', even if
  # the versions are unknown. So try to be as helpful as possible.
  def relay_outdated_help_text(relay)
    latest = relay.latest_version
    current = relay.last_reported_version
    first_part =  latest.present? ? "Relay version #{latest} available." : 'Newer Relay version available.'
    second_part = current.present? ? "You have #{current} installed." : ''

    [first_part, second_part].join(' ').chomp
  end

  def relay_outdated_class(relay)
    return 'label-warning' if relay.outdated
    'label-success'
  end
end
