# frozen_string_literal: true

# rubocop:disable Metrics/ModuleLength
module MachinesHelper
  require 'base64'

  STATUS_COLORS = {
    'pass' => 'success',
    'warn' => 'warning',
    'error' => 'warning',
    'fail' => 'danger',
    'info' => 'info'
  }.tap do |h|
    h.default = 'default'
  end.freeze

  STATUS_HEX_COLORS = {
    'pass' => '#00a65a',
    'warn' => '#f39c12',
    'error' => '#f39c12',
    'fail' => '#f56954',
    'info' => '#00c0ef'
  }.tap do |h|
    h.default = '#d2d6de'
  end.freeze

  STATUS_ICONS = {
    'pass' => 'fa-check-circle',
    'warn' => 'fa-exclamation-triangle',
    'error' => 'fa-exclamation-triangle',
    'fail' => 'fa-times-circle',
    'info' => 'fa-info-circle'
  }.tap do |h|
    h.default = 'fa-question-circle'
  end.freeze

  def assessments_unavailable?(docker_command_state, assessments)
    docker_command_state == 'pending' || docker_command_state == 'errored' || assessments.empty?
  end

  def show_fqdn_or_ip(machine)
    return machine.fqdn if machine.fqdn.present?
    machine.ip
  end

  def machine_status_color(machine)
    status_color machine.status
  end

  def status_color(status)
    STATUS_COLORS[status]
  end

  def status_hex_color(status)
    STATUS_HEX_COLORS[status]
  end

  def status_icon(status)
    STATUS_ICONS[status]
  end

  def decode64(key)
    Base64.decode64(key)
  end

  def assessments_grouped_by_reqs(assessments)
    initial = Hash.new do |a, k|
      a[k] = { status: '', assessments: [] }
    end
    assessments.each_with_object(initial) do |a, m|
      next m = add_assessment_to_group('ungrouped', -1, m, a) if a['associated_requirements'].empty?
      a['associated_requirements'].each do |r|
        m = add_assessment_to_group(r['name'], r['requirement_group_id'], m, a)
      end
    end
  end

  def add_assessment_to_group(key, gid, memo, assessment)
    memo[key][:assessments] << assessment
    memo[key][:status] = dominant_status(assessment_status(assessment), memo[key][:status])
    memo[key][:requirement_group_id] = gid
    memo
  end

  def security_container_completion_time_string(assessment)
    return '' unless assessment.state == 'complete'
    timestamp = format_timestamp(assessment.updated_at)
    "Completed on #{timestamp}"
  end

  def security_container_header_string(assessment)
    "#{assessment.repository_name}/#{title(assessment)} - v#{version(assessment)}"
  end

  def service_name(assessment)
    assessment.service_id ? "[#{assessment.service_name}]" : ''
  end

  def remove_scan_button(dc_id, commandable, opts = {})
    remove_scan_confirm_msg = 'Are you sure? This will permanently remove the scan and all its associated results!'
    link_to 'Remove',
            docker_command_path(dc_id, commandable.to_commandable_param_hash),
            data: { confirm: remove_scan_confirm_msg },
            class: opts[:classes],
            title: 'Remove Scan',
            method: :delete,
            remote: opts[:remote]
  end

  def scan_button_title(machine)
    if machine.security_tests_present?
      'Press to scan this machine.'
    else
      'Scanning not available until security tests have been added.'
    end
  end

  def scan_button_class_string(machine)
    if machine.security_tests_present?
      'btn btn-primary btn-xs pull-right'
    else
      'btn btn-primary btn-xs pull-right disabled'
    end
  end

  private

  # Assessment#title is essentially SecurityContainer#full_path; Reusing those helpers here
  def title(assessment)
    image_name container_name(assessment.title)
  end

  def version(assessment)
    tag_name container_name(assessment.title)
  end

  def link_to_scan_id(machine, latest_scan_id, scan_id)
    latest_or_scan_id = latest_scan_id == scan_id ? 'latest' : scan_id
    link_to(scan_id, results_machine_path(machine, scan_id: latest_or_scan_id))
  end
end
# rubocop:enable Metrics/ModuleLength
