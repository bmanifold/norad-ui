# frozen_string_literal: true

module SecurityContainersHelper
  EXAMPLE_CONTAINERS_YAML = <<~YAMLDOC
    -
      name: "container_name_1:tag"
      category: "blackbox"
      prog_args: "%{target}:%{port}"
      default_config:
        key: "value"
      multi_host: false
      test_types:
        - "web_application"
      configurable: true
      help_url: "https://acmecorp.com/docs/container_name.html"
      service:
        name: "http"
        transport_protocol: "tcp"
        port: 80
    -
      name: "container_name_2:tag"
      category: "blackbox"
      prog_args: "%{target}:%{port}"
      default_config:
        key: "value"
      multi_host: false
      test_types:
        - "web_application"
      configurable: true
      help_url: "https://acmecorp.com/docs/container_name.html"
      service:
        name: "http"
        transport_protocol: "tcp"
        port: 80
  YAMLDOC
  EXAMPLE_CONTAINER_YAML = <<~YAMLDOC
    name: "container_name:tag"
    category: "blackbox"
    prog_args: "%{target}:%{port}"
    default_config:
      key: "value"
    multi_host: false
    test_types:
      - "web_application"
    configurable: true
    help_url: "https://acmecorp.com/docs/container_name.html"
    service:
      name: "http"
      transport_protocol: "tcp"
      port: 80
  YAMLDOC
  DISPLAYED_ATTRIBUTES = %i[name prog_args default_config category configurable help_url test_types multi_host].freeze

  def prefixed_name(container)
    "[#{container.security_test_repository.name}] #{image_name(container.name)}"
  end

  def container_name(container_full_path)
    container_full_path.split('/', 2).last
  end

  def image_name(full_name)
    if full_name_contains_tag?(full_name)
      full_name.to_s.split(SecurityContainer::DOCKER_TAG_DELIMITER, 2).first
    else
      full_name
    end
  end

  # See https://docs.docker.com/engine/reference/commandline/pull for valid container strings
  def tag_name(full_name)
    if full_name_contains_tag?(full_name)
      full_name.to_s.split(SecurityContainer::DOCKER_TAG_DELIMITER, 2).last
    else
      ''
    end
  end

  def full_name_contains_tag?(full_name)
    full_name =~ SecurityContainer::DOCKER_TAG_DELIMITER
  end

  def displayed_container_yaml(container)
    container.attributes.slice(*DISPLAYED_ATTRIBUTES).to_hash.to_yaml
  end
end
