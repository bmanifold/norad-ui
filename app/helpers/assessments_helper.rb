# frozen_string_literal: true

module AssessmentsHelper
  STATUS_WEIGHTS = {
    'info' => 0,
    'pass' => 10,
    'warn' => 20,
    'error' => 30,
    'fail' => 40
  }.freeze

  CSS_BOX_STATUS_COLOR_MAPPING = {
    'pass' => 'success',
    'info' => 'info',
    'warn' => 'warning',
    'error' => 'warning',
    'fail' => 'danger'
  }.freeze

  def assessment_status(assessment)
    result = assessment.results.max_by { |r| STATUS_WEIGHTS[r.status] }
    return 'unknown' if result.nil?
    result.status
  end

  def requirements_to_human_from_assessment(assessment)
    display_value =
      assessment.associated_requirements.collect { |r| r[:name] }.join(', ')
    display_value.present? ? "[#{display_value}]" : ''
  end

  def assessment_box_color(assessment)
    status = assessment_status(assessment)
    resolve_status_to_box_class(status)
  end

  def resolve_status_to_box_class(status)
    CSS_BOX_STATUS_COLOR_MAPPING[status] || 'default'
  end

  def dominant_status(s1, s2)
    STATUS_WEIGHTS[s1].to_i > STATUS_WEIGHTS[s2].to_i ? s1 : s2
  end

  def reduce_by_result_status(assessments)
    results = init_status_service_assessment_results_hash
    assessments.each do |assessment|
      build_results_for_assessment(results, assessment)
    end
    results
  end

  def total_ignored_for_assessments(assessments)
    assessments.collect { |assessment| assessment.results.count(&:ignored) }.reduce(0, :+)
  end

  def security_container_completion_time_string(assessment)
    return '' unless assessment.state == 'complete'
    timestamp = format_timestamp(assessment.updated_at)
    "Completed on #{timestamp}"
  end

  def security_container_header_string(assessment)
    "#{title(assessment)} - v#{version(assessment)}"
  end

  def service_name(assessment)
    assessment.service_id ? "[#{assessment.service_name}]" : ''
  end

  private

  def build_results_for_assessment(results, assessment)
    assessment.results.each do |result|
      result_state_hash = results[result.status.to_sym]
      result_state_hash[:count] += 1
      result_state_hash[:ignored] += 1 if result.ignored?
      result_state_hash[:data][assessment.service_name][assessment] << result
    end
  end

  def init_status_service_assessment_results_hash
    rv = Hash.new do |h, k|
      h[k] = Hash.new do |hsh, key|
        hsh[key] = Hash.new do |subhash, subkey|
          subhash[subkey] = hash_with_array_default
        end
      end
    end
    assign_default_values(rv)
  end

  def assign_default_values(rv)
    rv[:fail][:count] = rv[:pass][:count] = rv[:warn][:count] = rv[:info][:count] = rv[:error][:count] = 0
    rv[:fail][:ignored] = rv[:pass][:ignored] = rv[:warn][:ignored] = rv[:info][:ignored] = rv[:error][:ignored] = 0
    rv
  end

  def hash_with_array_default
    Hash.new { |hsh, key| hsh[key] = [] }
  end
end
