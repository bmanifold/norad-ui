# frozen_string_literal: true

class NotificationChannelsController < ApplicationController
  def update
    @notification_channel = NotificationChannel.find params[:id]
    @notification_channel.attributes = notification_channel_params_hash
    @notification_channel.save!
    head :no_content
  rescue Her::Errors::ResourceInvalid, NoradApiExceptions::NoradError # NoradError superclasses many 4xx error codes
    head 400
  end

  private

  def notification_channel_params
    params.require(:notification_channel).permit(:enabled)
  end

  def notification_channel_params_hash
    notification_channel_params.to_h
  end
end
