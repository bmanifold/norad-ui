# frozen_string_literal: true

class ServiceDiscoveriesController < ApplicationController
  def create
    @service = ServiceDiscovery.create(machine_id: params[:machine_id])
    @msg = 'Service discovery scheduled. This may take up to 12 hours.'
    render_opts = { json: { message: @msg } }
  rescue NoradApiExceptions::NoradError
    @msg = @service&.pretty_errors || 'Unable to schedule service discovery. Please try again.'
    render_opts = { json: { errors: @msg }, status: :unprocessable_entity }
  ensure
    respond_to do |format|
      format.js
      format.json { render(render_opts) }
    end
  end
end
