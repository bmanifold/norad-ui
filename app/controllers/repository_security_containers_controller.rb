# frozen_string_literal: true

class RepositorySecurityContainersController < ApplicationController
  # GET /security_test_repositories/:security_test_repository_id/repository_security_containers/new
  def new
    @repository = SecurityTestRepository.find(params[:security_test_repository_id])
    @container = @repository.security_containers.build
  end

  # POST /security_test_repositories/:security_test_repository_id/repository_security_containers
  # params:
  #   yaml_attributes_file: A StringIO or File object referencing the YAML configuration file uploaded by the user.
  #
  # To avoid file maintenance and security concerns, process the YAML synchronously here; Rails automatically unlinks
  # this file when the request is over.
  def create
    create_containers
    set_flash_to_container_errors
  rescue NoradApiExceptions::NoradError => e
    flash[:danger] = "Could not add Security Tests. Error details: #{e.message}"
  rescue Psych::SyntaxError => e
    flash[:danger] = "YAML syntax error: #{e.message}"
  ensure
    redirect_to security_test_repository_path(params[:security_test_repository_id])
  end

  # DELETE /repository_security_containers/:id
  def destroy
    @container_id = params[:id]
    RepositorySecurityContainer.destroy_existing @container_id
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid => e
    @error_msg = "Failed to remove Security Test. Error details: #{e.message}"
  end

  private

  def set_flash_to_container_errors
    return unless @containers.respond_to?(:response_errors) && @containers.pretty_errors.present?
    flash[:danger] = @containers.pretty_errors
  end

  def create_containers
    @containers = RepositorySecurityContainer.create_multiple(
      creation_params_hashes,
      params[:security_test_repository_id]
    )
  end

  def parsed_params_from_yaml
    file_reference = container_params_hash[:yaml_attributes_file]
    parsed_yaml = YAML.safe_load(file_reference.read)
    config_hashes = parsed_yaml.is_a?(Array) ? parsed_yaml : [parsed_yaml]
    @parsed_params ||= config_hashes.collect do |config_hash|
      ActionController::Parameters.new(config_hash)
    end
  end

  def creation_params
    parsed_params_from_yaml.collect do |parsed_params|
      parsed_params.permit(*permitted_params).tap do |whitelisted|
        whitelisted[:default_config] =
          parsed_params.delete(:default_config).tap do |config|
            config&.send(:permitted=, true)
          end
      end
    end
  end

  def creation_params_hashes
    creation_params.map(&:to_h)
  end

  def permitted_params
    [:name, :category, :prog_args, :multi_host, :configurable, :help_url, :default_config,
     test_types: [], service: %i[name port transport_protocol]]
  end

  def container_params
    params.require(:repository_security_container).permit(:yaml_attributes_file)
  end

  def container_params_hash
    container_params.to_h
  end
end
