# frozen_string_literal: true

class DockerCommandsController < ApplicationController
  before_action :set_docker_command

  def destroy
    @dc.destroy
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not delete scan!'
  ensure
    @error_msg ||= @dc.pretty_errors
    respond_to_js_and_html
  end

  private

  def respond_to_js_and_html
    respond_to do |format|
      format.js { render 'destroy', status: @error_msg ? :bad_request : :ok }
      format.html do
        set_flash
        redirect_to history_path
      end
    end
  end

  def history_path
    if params[:machine_id]
      history_machine_path(params[:machine_id])
    else
      history_organization_path(params[:organization_id])
    end
  end

  def set_flash
    if @error_msg.present?
      flash[:danger] = @error_msg
    else
      flash[:success] = 'Scan removed!'
    end
  end

  def set_docker_command
    @dc = DockerCommand.find params[:id]
  end
end
