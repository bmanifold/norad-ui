# frozen_string_literal: true

class OrganizationConfigurationsController < ApplicationController
  def update
    OrganizationConfiguration.save_existing(params[:id], update_params_hash)
    head :no_content
  end

  private

  def update_params
    params.require(:organization_configuration).permit(:auto_approve_docker_relays, :use_relay_ssh_key)
  end

  def update_params_hash
    update_params.to_h
  end
end
