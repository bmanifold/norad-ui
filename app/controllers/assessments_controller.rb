# frozen_string_literal: true

class AssessmentsController < ApplicationController
  # GET /docker_commands/:docker_command_id/assessments
  def index
    dc_id = params[:docker_command_id]
    @assessments = DockerCommandAssessment.all(_docker_command_id: dc_id).fetch
  end
end
