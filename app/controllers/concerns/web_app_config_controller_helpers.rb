# frozen_string_literal: true

module WebAppConfigControllerHelpers
  extend ActiveSupport::Concern

  included do
    private

    def web_app_config_params
      params.require(:web_application_config).permit(:auth_type, :url_blacklist, :starting_page_path,
                                                     :login_form_username_field_name, :login_form_password_field_name)
    end

    def web_app_config_params_hash
      web_app_config_params.to_h
    end

    def create_web_app_config(service)
      return unless service.web_app?
      web_app_config = WebApplicationConfig.new web_app_config_params_hash.merge(service_id: service.id)
      web_app_config.save!
    end

    def update_web_app_config(service)
      return unless service.web_app?
      return create_web_app_config(service) if service.web_application_config.nil?
      WebApplicationConfig.save_existing(service.web_application_config.id, web_app_config_params_hash)
    end

    def resolve_web_app_config
      return WebApplicationConfig.new unless @service.web_app?
      @service.web_application_config.nil? ? WebApplicationConfig.new : @service.web_application_config
    end
  end
end
