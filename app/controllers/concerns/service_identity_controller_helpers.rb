# frozen_string_literal: true

module ServiceIdentityControllerHelpers
  extend ActiveSupport::Concern

  included do
    private

    def identity_params
      params.require(:service_identity).permit(:username, :password)
    end

    def identity_params_hash
      identity_params.to_h
    end

    def identity_params_blank?
      return true if params[:service_identity].blank?
      identity_params[:username].blank? && identity_params[:password].blank?
    end

    def create_service_identity(service)
      return if identity_params_blank?
      identity = ServiceIdentity.new identity_params_hash.merge(service_id: service.id)
      identity.save!
    end

    def update_service_identity(service)
      return if identity_params_blank?
      if service.identity?
        ServiceIdentity.save_existing(service.service_identity.id, identity_params_hash)
      else
        service.service_identity.create(identity_params_hash)
      end
    end
  end
end
