# frozen_string_literal: true

module ServiceParamsHelpers
  def web_app?
    params[:service][:type] == 'WebApplicationService'
  end

  WEB_APPLICATION_CONFIG_SHARED_ATTRIBUTES = %i[
    auth_type
    starting_page_path
    login_form_username_field_name
    login_form_password_field_name
    url_blacklist
  ].freeze

  def service_specific_create_attributes
    return [] unless web_app?
    [{ web_application_config_attributes: WEB_APPLICATION_CONFIG_SHARED_ATTRIBUTES }]
  end

  def service_specific_update_attributes
    return [] unless web_app?
    [{ web_application_config_attributes: WEB_APPLICATION_CONFIG_SHARED_ATTRIBUTES + %i[id] }]
  end

  CREATE_AND_UPDATE_SHARED_ATTRIBUTES = %i[
    name
    description
    port
    port_type
    encryption_type
    type
    allow_brute_force
  ].freeze

  def service_create_params
    params.require(:service).permit(
      *(
        CREATE_AND_UPDATE_SHARED_ATTRIBUTES + [
          { application_type_attributes: %i[port name transport_protocol] },
          { service_identity_attributes: %i[username password] }
        ] + service_specific_create_attributes
      )
    )
  end

  def service_update_params
    params.require(:service).permit(
      *(
        CREATE_AND_UPDATE_SHARED_ATTRIBUTES + [
          { application_type_attributes: %i[port name transport_protocol] },
          { service_identity_attributes: %i[id username password] }
        ] + service_specific_update_attributes
      )
    )
  end
end
