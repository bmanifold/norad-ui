# frozen_string_literal: true

class SshKeyPairsController < ApplicationController
  def new
    @org = Organization.new slug: params[:organization_id]
  end

  def create
    org = Organization.new slug: params[:organization_id]
    key_pair = org.ssh_key_pairs.create(ssh_key_params_hash)
    flash[:danger] = 'Unable to create SSH Key Pair.' unless key_pair.id
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to create SSH Key Pair.'
  ensure
    redirect_to settings_organization_path(org.slug)
  end

  def destroy
    SshKeyPair.destroy_existing(params[:id])
  rescue NoradApiExceptions::NoradError
    head 400
  else
    head :no_content
  end

  private

  def ssh_key_params
    params.require(:ssh_key_pair).permit(:name, :description, :username, :key).tap do |p|
      p[:key] = Base64.strict_encode64(p[:key].gsub(/\r\n/, "\n"))
    end
  end

  def ssh_key_params_hash
    ssh_key_params.to_h
  end
end
