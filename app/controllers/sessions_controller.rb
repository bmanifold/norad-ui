# frozen_string_literal: true

class SessionsController < ApplicationController
  layout 'authentication'

  private

  def authenticate_user(auth_params)
    user = User.authenticate auth_params
    user_info_to_session(user)
  end

  def user_info_to_session(user)
    reset_session
    session[:api_token] = user.api_token
    session[:user_uid] = user.uid
    session[:user_id] = user.id
    session[:expires_at] = Time.zone.now + Rails.configuration.x.session_timeout
  end
end
