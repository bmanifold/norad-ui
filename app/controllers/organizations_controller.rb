# frozen_string_literal: true

class OrganizationsController < ApplicationController
  layout 'organization', only: :settings
  after_action :remove_default_org, only: [:destroy]
  before_action :set_org, only: %i[update destroy results settings history]

  def index
    @orgs = Organization.all
    @default_org = @user_profile.default_org || ''
  end

  def show
    @org = Organization.where(include_assessment_summary: true).find(params[:id])
  rescue NoradApiExceptions::ForbiddenError
    render :forbidden
  end

  def create
    @org = Organization.new org_params_hash
    @org.save!
  rescue Her::Errors::ResourceInvalid, NoradApiExceptions::NoradError
    render status: :unprocessable_entity, json: { errors: @org.pretty_errors }
  end

  def update
    @org.safe_update_uid(org_params_hash)
  rescue Her::Errors::ResourceInvalid
    error_msg ||= @org.pretty_errors
    flash[:danger] = error_msg if error_msg
  ensure
    redirect_to action: 'settings', id: @org.slug
  end

  def destroy
    @org.destroy if @org.present?
    status = :no_content
  rescue NoradApiExceptions::NoradError
    @org = nil
    status = :unprocessable_entity
  ensure
    respond_to do |format|
      format.html { redirect_to organizations_path }
      format.js { render status: status }
    end
  end

  def scan
    scan_response = Organization.scan slug: params[:id]
    if scan_response.response_errors.present?
      render(json: { errors: scan_response.pretty_errors }, status: :unprocessable_entity)
    else
      head :ok
    end
  rescue NoradApiExceptions::NoradError
    render(json: { errors: 'Error starting scan' }, status: :unprocessable_entity)
  end

  def history
  rescue NoradApiExceptions::ForbiddenError
    flash.now[:danger] = 'You do not have access to view this organization!'
    render :forbidden
  end

  # GET /organizations/:id/results?scan_id=1
  def results
    @scan_id = params[:scan_id].to_i
    @scan_title = @scan_id.zero? ? 'latest' : @scan_id
    @machine_summary = DockerCommandMachineSummary.find(@scan_title)
    @export_queues_present = @org.result_export_queues.present?
  rescue NoradApiExceptions::ForbiddenError
    flash.now[:danger] = 'You do not have access to view this machine!'
    render :forbidden
  end

  def settings
    @memberships = @org.memberships
    @security_containers = SecurityContainer.all_by_id if current_user_admin_for_org?(@org)
    @iaas_config = @org.iaas_configuration
    @ssh_key_pairs = @org.ssh_key_pairs
    @notification_channels = @org.notification_channels
    @result_export_queues = @org.result_export_queues.map(&:cast_to_subclass)
  end

  def default
    @user_profile['default_org'] = params[:org]
    head :no_content if @user_profile.save
  end

  private

  # Strong Parameter methods
  def org_params
    params.require(:organization).permit(:uid)
  end

  def org_params_hash
    org_params.to_h
  end

  # Helper methods
  def set_org
    @org = Organization.find params[:id]
  end

  def remove_default_org
    @user_profile.update!(default_org: nil) if current_org_default?
  end

  def current_org_default?
    @user_profile.default_org == @org.slug if @org.present?
  end
end
