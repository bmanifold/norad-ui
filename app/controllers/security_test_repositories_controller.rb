# frozen_string_literal: true

class SecurityTestRepositoriesController < ApplicationController
  # GET /security_test_repositories/new
  def new; end

  # POST /security_test_repositories
  def create
    @repository = SecurityTestRepository.create(repository_params_hash)
  rescue NoradApiExceptions::NoradError => e
    @error_msg = "Could not create repository. Error details: #{e.message}"
  else
    @error_msg = @repository.pretty_errors
  end

  # GET /security_test_repositories
  def index
    @repositories = SecurityTestRepository.all
  end

  # GET /security_test_repositories/:id
  def show
    @repository = SecurityTestRepository.find(params[:id])
    @containers = @repository.security_containers_in_response
    @organizations = Organization.all.select { |org| current_user_admin_for_org?(org) }
    set_members
  end

  # PUT /security_test_repositories/:id
  def update
    SecurityTestRepository.save_existing(params[:id], repository_params_hash)
    flash[:success] = 'Successfully updated test repository.'
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to update repository.'
  ensure
    redirect_back fallback_location: root_path
  end

  # DELETE /security_test_repositories/:id
  def destroy
    SecurityTestRepository.destroy_existing(params[:id])
    flash.now[:success] = 'Test Repository removed.'
    redirect_to security_test_repositories_path
  rescue NoradApiExceptions::NoradError => e
    flash.now[:danger] = "Test Repository could not be removed. Error details: #{e.message}"
    redirect_to security_test_repository_path(params[:id])
  end

  private

  def set_members
    @members = RepositoryMember.all(security_test_repository_id: params[:id]).fetch
  rescue NoradApiExceptions::ForbiddenError
    @members = nil
  end

  def repository_params
    # prevent clearing existing password when not specified
    params[:security_test_repository].delete(:password) if params[:security_test_repository].fetch(:password).blank?

    params.require(:security_test_repository).permit(:name, :username, :password, :host, :public)
  end

  def repository_params_hash
    repository_params.to_h
  end
end
