# frozen_string_literal: true

class MembershipsController < ApplicationController
  def create
    return(@error_msg = 'User ID cannot be blank') if membership_params[:uid].blank?
    @membership = organization.memberships.create(membership_params_hash)
    @error_msg = @membership.pretty_errors if @membership.response_errors.present?
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not create membership!'
  ensure
    respond_to do |format|
      format.js
    end
  end

  def destroy
    Membership.destroy_existing params[:id]
    @membership_id = params[:id]
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not delete membership!'
  ensure
    respond_to do |format|
      format.js
    end
  end

  private

  def organization
    @organization ||= Organization.find(params[:organization_id])
  end

  def membership_params
    params.require(:membership).permit(:uid, :admin)
  end

  def membership_params_hash
    membership_params.to_h
  end
end
