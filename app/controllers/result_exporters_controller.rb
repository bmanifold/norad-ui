# frozen_string_literal: true

class ResultExportersController < ApplicationController
  def create
    issue = ResultExporter.new(result_ids: [params[:result_exporter][:result_id]])
    issue.save!
    respond_to do |format|
      format.js
      format.json { head :no_content }
    end
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    @error_msg = 'Failed to export result.'
    respond_to do |format|
      format.js
      format.json { render json: { errors: @error_msg }, status: :unprocessable_entity }
    end
  end
end
