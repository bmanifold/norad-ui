# frozen_string_literal: true

class ConnectivityChecksController < ApplicationController
  def create
    @check = controller_name.classify.constantize.new(machine_id: params[:machine_id])
    @check.save!
  rescue Her::Errors::ResourceInvalid, NoradApiExceptions::NoradError
    render json: { errors: @check.pretty_errors }, status: :unprocessable_entity
  end

  def show
    @check = controller_name.classify.constantize.find(params[:id], _machine_id: params[:machine_id])
  rescue NoradApiExceptions::NotFoundError
    # Prevent noisy 404s for this route since the latest checks may not be created for this machine
    head :not_found
  end
end
