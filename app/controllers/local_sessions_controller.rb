# frozen_string_literal: true

class LocalSessionsController < SessionsController
  before_action -> { redirect_to root_url if user_signed_in? }, only: :new

  skip_before_action :require_login, only: %i[new create]
  skip_before_action :set_user_profile, only: %i[new create]
  skip_around_action :set_user_api_token, only: %i[new create]

  def new; end

  def create
    authenticate_user(login_params_hash)
    redirect_to root_url
  rescue StandardError
    flash.now[:danger] = 'Invalid login attempt'
    render :new
  end

  def destroy
    reset_session
    flash.now[:success] = 'Logout successful'
    render :new
  end

  private

  def login_params
    params.require(:session).permit(:email, :password)
  end

  def login_params_hash
    login_params.to_h
  end
end
