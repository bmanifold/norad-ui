# frozen_string_literal: true

class OrganizationEnforcementsController < ApplicationController
  def create
    org = Organization.find(params[:organization_id])
    params['requirement_group_ids'].try(:map) do |rgid|
      org.enforcements.create requirement_group_id: rgid
    end
  rescue NoradApiExceptions::NoradError
    flash[:error] = 'The API returned an error!'
  ensure
    redirect_to organization_security_tests_path(org)
  end

  def destroy
    @enforcement_id = params[:id]
    OrganizationEnforcement.destroy_existing(@enforcement_id)
    respond_to do |format|
      format.js
    end
  end
end
