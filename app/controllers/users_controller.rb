# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user

  skip_around_action :set_user_api_token, only: %i[new create]
  skip_before_action :require_login, only: %i[new create]
  skip_before_action :set_user_profile, only: %i[new create]

  def index
    # For the time being it doesn't seem useful to display a list of all users.
    # So, let's just redirect them to their own user settings page
    redirect_to user_path(@user.uid)
  end

  def new
    render layout: 'authentication'
  end

  def create
    if @user.save
      flash[:success] = 'Sign up successful. Please check your email for a confirmation link.'
      redirect_to root_path
    else
      flash[:danger] = @user.pretty_errors
      render :new, layout: 'authentication'
    end
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'There was an error signing up.'
    render :new, layout: 'authentication'
  end

  def edit; end

  def update
    return unless @user
    @user.assign_attributes(update_params_hash)
    @user.save!
    flash[:success] = 'Successfully updated user.'
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    flash[:danger] = 'Unable to update user.'
  ensure
    redirect_to user_path(@user)
  end

  def show; end

  private

  def set_user
    @user =
      case action_name
      when 'index'  then current_user
      when 'new'    then User.new
      when 'create' then User.new(create_params_hash)
      else               current_user if same_user?
      end
  end

  def same_user?
    current_user.uid == params[:id]
  end

  def update_params
    params.require(:user).permit(:firstname, :lastname)
  end

  def update_params_hash
    update_params.to_h
  end

  def create_params
    params.require(:user).permit(:email, local_authentication_method_attributes: {
                                   local_authentication_record_attributes: %i[password password_confirmation]
                                 })
  end

  def create_params_hash
    create_params.to_h
  end
end
