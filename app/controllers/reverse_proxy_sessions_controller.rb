# frozen_string_literal: true

class ReverseProxySessionsController < SessionsController
  before_action -> { redirect_to root_url if user_signed_in? }, only: :new

  skip_before_action :require_login, only: %i[new create]
  skip_before_action :set_user_profile, only: %i[new create]
  skip_around_action :set_user_api_token, only: %i[new create]

  def new
    prod_new if Rails.env.production?
  end

  def create
    # With SSO, this method is not necessary so just send back a 204
    head :no_content && return if Rails.env.production?

    authenticate_user(login_params_hash)
    redirect_to root_url
  rescue StandardError
    flash.now[:danger] = 'Invalid login attempt'
    render :new
  end

  def destroy
    reset_session
    clear_sso_cookie if Rails.env.production?
    unless Rails.env.production?
      flash.now[:success] = 'Logout successful'
      render(:new) && return
    end
    redirect_to root_url
  end

  private

  def login_params
    params.require(:session).permit(:uid, :password)
  end

  def login_params_hash
    login_params.to_h
  end

  def clear_sso_cookie
    cookies.delete AuthStrategy.sso_cookie_name, domain: AuthStrategy.sso_domain
  end

  def sso_authenticate_user
    authenticate_user(prod_params_hash)
  rescue StandardError
    clear_sso_cookie
  end

  def prod_params
    ActionController::Parameters.new(uid: request.headers['HTTP_PI_USER']).permit(:uid)
  end

  def prod_params_hash
    prod_params.to_h
  end

  def prod_new
    sso_authenticate_user
    redirect_to root_url
  end
end
