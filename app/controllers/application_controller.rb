# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Set current user's authentication token
  around_action :set_user_api_token
  before_action :require_login
  before_action :set_user_profile

  # Resolves current user.
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id].present?
  end
  helper_method :current_user

  # Checks whether the user is signed in and his/her token hasn't expired.
  def user_signed_in?
    !session_expired? && session[:api_token].present?
  end
  helper_method :user_signed_in?

  private

  def require_login
    redirect_to login_path unless !session_expired? && current_user
    update_session_expiration
  end

  def session_expired?
    session[:expires_at].blank? || (session[:expires_at] <= Time.zone.now)
  end

  def update_session_expiration
    session[:expires_at] = Time.zone.now + Rails.configuration.x.session_timeout
  end

  # Add some meta information to the Event payload
  def append_info_to_payload(payload)
    super
    payload[:user] = session[:user_id] || 'no_valid_credentials_provided'
    payload[:remote_ip] = request.remote_ip
  end

  protected

  def current_user_admin_for_org?(org)
    org.memberships.detect { |m| m.user == current_user }&.role == 'admin'
  end
  helper_method :current_user_admin_for_org?

  def set_user_api_token
    RequestStore.store[:api_token] = session[:api_token]
    yield
  rescue NoradApiExceptions::NotFoundError
    raise(ActionController::RoutingError.new('Not Found'), 'Norad API raised Not Found Error')
  rescue NoradApiExceptions::UnauthorizedError
    reset_session
    redirect_to login_url
  ensure
    RequestStore.store[:api_token] = nil
  end

  def set_user_profile
    @user_profile = UserProfile.find_or_create_by(user_uid: @current_user.uid) do |profile|
      profile.default_org = "#{@current_user.uid}-org-default"
    end
  end
end
