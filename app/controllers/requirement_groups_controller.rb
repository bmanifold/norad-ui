# frozen_string_literal: true

class RequirementGroupsController < ApplicationController
  before_action :set_group, only: [:show]

  def index
    @groups = RequirementGroup.all
  end

  def create
    group = RequirementGroup.new requirement_group_params_hash
    group.save!
    redirect_to requirement_groups_path
  rescue Her::Errors::ResourceInvalid
    flash[:danger] = group.pretty_errors
    redirect_to new_requirement_group_path
  end

  def new; end

  def show; end

  def update
    RequirementGroup.save_existing(params[:id], requirement_group_params_hash)
    flash[:success] = 'Successfully updated requirement group.'
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Unable to update requirement group.'
  ensure
    redirect_back(fallback_location: { action: 'show', id: params[:id] })
  end

  def destroy
    RequirementGroup.destroy_existing(params[:id])
  end

  private

  def requirement_group_params
    params.require(:requirement_group).permit(:name, :description)
  end

  def requirement_group_params_hash
    requirement_group_params.to_h
  end

  def set_group
    @group = RequirementGroup.find params[:id]
  end
end
