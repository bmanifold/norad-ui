# frozen_string_literal: true

class ResultIgnoreRulesController < ApplicationController
  before_action :set_parent

  def create
    @rule = @parent.result_ignore_rules.create result_ignore_rule: creation_params_hash
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid => e
    handle_create_error(e)
  else
    handle_create_success
  end

  def destroy
    remove_result_ignore_rule
    error = false
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    error = true
  ensure
    respond_to do |format|
      format.html { assign_flash_message_and_redirect(error) }
      format.js { assign_result_id_and_render_destroy_js }
      format.json { head :no_content }
    end
  end

  def edit
    @scope = Organization.find(params[:organization_id]) if params[:organization_id]
    @scope ||= Machine.find(params[:machine_id]) if params[:machine_id]
    @rule = ResultIgnoreRule.find(params[:id])
  end

  def update
    @rule = ResultIgnoreRule.find(params[:id])
    raise NoradApiExceptions::NoradError unless @rule
    @rule.assign_attributes(update_params_hash)
    @rule.save!
    flash[:success] = 'Successfully updated result_ignore_rule.'
  rescue NoradApiExceptions::NoradError, Her::Errors::ResourceInvalid
    flash[:danger] = 'Unable to update result_ignore_rule.'
  ensure
    redirect_to scope_redirect_path
  end

  private

  def handle_create_success
    respond_to do |format|
      format.html do
        flash[:success] = 'Ignore rule created.'
      end
      format.json do
        head :no_content
      end
    end
  end

  def handle_create_error(e)
    respond_to do |format|
      format.html do
        flash[:danger] = 'There was an error creating this rule.'
      end
      format.json do
        render json: { errors: e.response_errors }, status: :unprocessable_entity
      end
    end
  end

  def assign_result_id_and_render_destroy_js
    @rir_id = @error ? nil : params[:id]
    render 'destroy_js'
  end

  def assign_flash_message_and_redirect(error)
    if error
      flash[:danger] = 'There was an error removing this ignore rule'
    else
      flash[:success] = 'Ignore rule removed.'
    end
    redirect_back fallback_location: root_path
  end

  def remove_result_ignore_rule
    if params[:machine_id] || params[:organization_id]
      destroy_rule_for_all_parents
    else
      ResultIgnoreRule.destroy_existing params[:id]
    end
  end

  def scope_redirect_path
    return organization_security_tests_path(params[:organization_id]) if params[:organization_id]
    return machine_security_tests_path(params[:machine_id]) if params[:machine_id]
  end

  def machine_destroy_rule
    params[:machine_id] && MachineResultIgnoreRule.destroy_existing(params[:id], machine_id: params[:machine_id])
  end

  def organization_destroy_rule
    params[:organization_id] &&
      OrganizationResultIgnoreRule.destroy_existing(params[:id], organization_id: params[:organization_id])
  end

  # Ensure either machine_destroy_rule or organization_destroy_rule or both executes successfully.
  # Otherwise, re-raise NotFound
  def destroy_rule_for_all_parents
    tries ||= 0
    tries.zero? && machine_destroy_rule
    organization_destroy_rule
  rescue NoradApiExceptions::NotFoundError => e
    tries += 1
    raise e if tries >= 2
    retry
  end

  def update_params
    params.require(:result_ignore_rule).permit(:comment)
  end

  def update_params_hash
    update_params.to_h
  end

  def creation_params
    params.require(:result_ignore_rule).permit(:signature, :comment)
  end

  def creation_params_hash
    creation_params.to_h
  end

  def set_parent
    @parent ||=
      params[:machine_id].present? ? Machine.find(params[:machine_id]) : Organization.find(params[:organization_id])
  end
end
