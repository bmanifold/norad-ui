# frozen_string_literal: true

class EmailConfirmationsController < ApplicationController
  skip_around_action :set_user_api_token
  skip_before_action :require_login
  skip_before_action :set_user_profile

  def show
    EmailConfirmation.create(confirmation_params_hash)
    flash[:success] = 'Your email has been successfully confirmed.'
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'There was an error confirming your email address.'
  ensure
    redirect_to root_path
  end

  private

  def confirmation_params
    params.permit(:email_confirmation_token)
  end

  def confirmation_params_hash
    confirmation_params.to_h
  end
end
