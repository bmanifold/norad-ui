# frozen_string_literal: true

class MachineSecurityTestsController < ApplicationController
  before_action :set_machine, only: %i[index create]
  layout 'machine', only: :index

  def index
    @org = @machine.organization
    @containers = OrganizationSecurityContainer.all_by_id(organization_id: @org.db_id)
  end

  def create
    requirement = params[:requirement].present? && params[:requirement] == 'true'
    @container_configs = SecurityContainerConfig.create_configs(@machine, container_ids, requirement)
  end

  private

  # Strong Parameters Helpers
  def container_ids
    params.require(:container_ids)
  end

  def set_machine
    @machine = Machine.find params[:machine_id]
  end
end
