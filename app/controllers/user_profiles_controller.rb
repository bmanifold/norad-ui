# frozen_string_literal: true

class UserProfilesController < ApplicationController
  def update
    org = Organization.find params[:default_org] if params[:default_org].present?
    new_default = org ? org.slug : nil
    @user_profile.update(default_org: new_default)
  rescue NoradApiExceptions::NoradError
    flash[:danger] = 'Error caught while trying to set default org'
  ensure
    redirect_to organizations_path
  end
end
