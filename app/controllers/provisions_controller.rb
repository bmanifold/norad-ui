# frozen_string_literal: true

class ProvisionsController < ApplicationController
  before_action :set_requirement, only: %i[index create]
  before_action :set_containers, only: %i[index create]

  def index
    @provisions = @requirement.provisions
  end

  def create
    container_ids = params[:container_ids]
    return(@error_msg = 'no provision(s) specified') if container_ids.blank?

    @provisions = Provision.create_provisions(@requirement, container_ids)
  rescue NoradApiExceptions::ForbiddenError
    @error_msg = "You don't have permission to add this provision"
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not add provision!'
  ensure
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @provision_id = params[:id]
    Provision.destroy_existing params[:id]
  rescue NoradApiExceptions::NoradError
    @error_msg = 'Could not remove provision!'
  ensure
    respond_to do |format|
      format.js
    end
  end

  private

  def set_requirement
    @requirement = Requirement.find params[:requirement_id]
  end

  def set_containers
    @containers = SecurityContainer.all
  end
end
