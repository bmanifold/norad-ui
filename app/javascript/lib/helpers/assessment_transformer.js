const statusToSortWeight = {
  fail: 4,
  error: 3,
  warn: 2,
  pass: 1,
  info: 0
}

const initRequirement = (req) => {
  return {
    id: req.id,
    requirement_name: req.name,
    requirement_description: req.description,
    total_assessments: 0,
    completed_assessments: 0,
    failing_count: 0,
    erroring_count: 0,
    warning_count: 0,
    passing_count: 0,
    informing_count: 0,
    ignoring_count: 0,
    failing_results: [],
    informing_results: [],
    erroring_results: [],
    passing_results: [],
    warning_results: []
  }
}

const assessmentSorter = (a, b) => {
  return a.assessment_id - b.assessment_id
}

const machineSorter = (a, b) => {
  return a.machine_id - b.machine_id
}

const dummy_requirement = {
  name: 'UNGROUPED',
  id: -1,
  description: 'This is a dummy requirement used to group assessments that do not belong to any requirement.',
}

export default {
  transformByRequirement (context, raw_assessments) {
    let requirements = {}

    for (let assessment of raw_assessments) {
      if (assessment.associated_requirements.length === 0) assessment.associated_requirements.push(dummy_requirement)

      for (let requirement of assessment.associated_requirements) {
        requirements[requirement.id] = requirements[requirement.id] || initRequirement(requirement)

        requirements[requirement.id].total_assessments += 1
        if (assessment.state === 'complete') requirements[requirement.id].completed_assessments += 1

        for (let result of assessment.results) {
          if (result.ignored) {
            requirements[requirement.id].ignoring_count += 1
            context.commit('ignoreResult', result.signature)
          }
          if (result.status === 'fail') {
            requirements[requirement.id].failing_count += 1
            requirements[requirement.id].failing_results.push(result)
          }
          if (result.status === 'error') {
            requirements[requirement.id].erroring_count += 1
            requirements[requirement.id].erroring_results.push(result)
          }
          if (result.status === 'warn') {
            requirements[requirement.id].warning_count += 1
            requirements[requirement.id].warning_results.push(result)
          }
          if (result.status === 'info') {
            requirements[requirement.id].informing_count += 1
            requirements[requirement.id].informing_results.push(result)
          }
          if (result.status === 'pass') {
            requirements[requirement.id].passing_count += 1
            requirements[requirement.id].passing_results.push(result)
          }
          result.machine_name = assessment.machine_name
          result.machine_id = assessment.machine_id
          result.test_title = `${assessment.repository_name}/${assessment.title.split('/')[1]}`
        }
      }
    }

    // Sort results by machine then assessment (backwards in JS)
    for (let req_id in requirements) {
      requirements[req_id].failing_results.sort(assessmentSorter).sort(machineSorter)
      requirements[req_id].erroring_results.sort(assessmentSorter).sort(machineSorter)
      requirements[req_id].warning_results.sort(assessmentSorter).sort(machineSorter)
      requirements[req_id].informing_results.sort(assessmentSorter).sort(machineSorter)
      requirements[req_id].passing_results.sort(assessmentSorter).sort(machineSorter)
    }

    return requirements
  }
}
