const toggleClass = (el, klass) => {
  if (el.classList.contains(klass)) return el.classList.remove(klass)
  return el.classList.add(klass)
}
export default toggleClass
