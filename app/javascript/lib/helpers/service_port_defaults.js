export function defaultProtocol (vm) {
  return vm.service_type === 'WebApplicationService' || vm.service_type === 'SshService' ? 'tcp' : null
}

export function applicationPort (vm) {
  if (vm.service_type === 'WebApplicationService') return '80'
  if (vm.service_type === 'SshService') return '22'
  return null
}

export function defaultPortNumber (vm) {
  let port_number = null
  if (vm.port_number_changed) port_number = vm.port_number // Don't overwrite user-specified port number
  else if (vm.service_type === 'WebApplicationService')
    port_number = vm.encryption_protocol === 'ssl' ? '443' : '80'
  else if (vm.service_type === 'SshService')
    port_number = '22'

  return port_number
}
