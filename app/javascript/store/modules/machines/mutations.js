export default {
  setMachines: (state, { machines }) => {
    state.items = machines
  },

  deleteMachine: (state, { machineId }) => {
    state.items = state.items.filter((machine) => {
      return machine.id == machineId ? false : true
    })
  },

  updateMachine: (state, { updatedMachine }) => {
    let machineIndex = state.items.findIndex(machine => machine.id == updatedMachine.id)
    state.items[machineIndex].ip = updatedMachine.ip
    state.items[machineIndex].fqdn = updatedMachine.fqdn
    state.items[machineIndex].use_fqdn_as_target = updatedMachine.use_fqdn_as_target
    state.items[machineIndex].name = updatedMachine.name
    state.items[machineIndex].description = updatedMachine.description
  },

  setActiveEditedMachine: (state, { machine }) => {
    state.activeEditedMachine = machine
  },

  setOrganization: (state, { organization }) => {
    state.organization = organization
  }
}
