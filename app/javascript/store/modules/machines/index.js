import mutations from './mutations'

export default {
  state: {
    items: [],
    activeEditedMachine: { ip: "", fqdn: "", use_fqdn_as_target: false, name: "", description: "" },
    organization: null
  },
  actions: {},
  mutations,
  getters: {},
  namespaced: true
}
