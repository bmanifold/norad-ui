export default {
  dataInitialized (state) {
    return state.dataInitialized
  },

  requirements (state) {
    return state.requirements
  },

  ignored_result_signatures (state) {
    return state.ignored_result_signatures
  },

  show_ignored_results (state) {
    return state.show_ignored_results
  },

  export_queues_present (state) {
    return state.export_queues_present
  },

  exported_result_ids (state) {
    return state.exported_result_ids
  },

  org_id (state) {
    return state.org_id
  }
}
