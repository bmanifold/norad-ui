import axios from 'axios'
import AssessmentTransformer from 'lib/helpers/assessment_transformer'

const handleRequestError = (error) => {
  alert(`Error: Could not complete request: ${error}`)
}

export default {
  initData: async (context, obj) => {
    context.commit('setOrgId', obj.org_id)
    context.commit('setDockerCommandId', obj.docker_command_id)
    context.commit('setExportQueuesPresent', obj.export_queues_present)

    await context.dispatch('loadData')
    context.commit('dataInitialized')
  },

  loadData: async (context) => {
    let req_obj = Routes.docker_command_assessments_path({
      docker_command_id: context.state.docker_command_id,
      format: 'json'
    })
    return axios.get(req_obj)
      .then(http_response => {
        context.dispatch('transformRawAssessments', http_response.data)
      }).catch(error => {
        alert(`Error: Could not fetch assessments for this scan. Error details: ${error}`)
      })
  },

  transformRawAssessments (context, response_data) {
    context.commit('setRequirements', AssessmentTransformer.transformByRequirement(context, response_data))
  },

  toggleIgnoredResults (context) {
    context.commit('toggleIgnoredResults', !context.state.show_ignored_results)
  },

  toggleIgnoreResult (context, result) {
    context.commit('toggleIgnoreResult', { result: result, boolean: !result.ignored })
  },

  exportResults (context, result_id) {
    let params = {
      result_exporter: {
        result_id: result_id
      },
      format: 'json'
    }
    let uri = Routes.result_exporters_path(params)
    axios.post(uri).then(() => { context.commit('exportResult', result_id) }).catch(handleRequestError)
  },

  createIgnoreRule (context, obj) {
    let params = {
      result_ignore_rule: {
        signature: obj.result_signature
      },
      format: 'json'
    }
    let uri = Routes[`${obj.ignore_scope}_result_ignore_rules_path`](obj.ignore_scope_id, params)
    axios.post(uri).then(() => { context.commit('ignoreResult', obj.result_signature) }).catch(handleRequestError)
  },

  stopIgnoring (context, obj) {
    let params = {
      format: 'json',
      machine_id: obj.machine_id,
      organization_id: obj.org_id
    }
    let uri = Routes.result_ignore_rule_path(obj.result_signature, params)
    axios.delete(uri).then(() => { context.commit('stopIgnoring', obj.result_signature) }).catch(handleRequestError)
  },

  incrementIgnoringCount (context, requirement_id) {
    context.commit('incrementIgnoringCount', requirement_id)
  },

  decrementIgnoringCount (context, requirement_id) {
    context.commit('decrementIgnoringCount', requirement_id)
  }
}
