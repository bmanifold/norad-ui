export default {
  nextView (state) {
    let next_view = state.next_view
    let prev_view = state.current_view
    state.current_view = next_view
    state.view_history.push(prev_view)
  },
  prevView (state) {
    let prev_view = state.view_history.pop()
    let next_view = state.current_view
    state.current_view = prev_view
    state.next_view = next_view
  },
  setNextView (state, view) {
    state.next_view = view
  },

  // If a user enters their own custom port number, don't override it with defaults
  // when the service type or encryption type changes.
  setPortNumberChanged (state) {
    state.port_number_changed = true
  },
  setApplicationTypeChanged (state) {
    state.application_type_changed = true
  },
  setCreationErrorString (state, error_string) {
    state.creation_error_string = error_string
  },
  setFinalServiceObj (state, service_obj) {
    state.final_service_obj = service_obj
  }
}
