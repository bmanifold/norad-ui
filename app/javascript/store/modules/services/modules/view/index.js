import mutations from './mutations'

export default {
  namespaced: true,
  state: {
    // Initial state for all views. Defining these here allows Vue
    // to listen for changes, much like data properties on components.
    view_history: [],
    current_view: 'service_type',
    next_view: null,
    port_number_changed: false,
    application_type_changed: false,
    creation_error_string: null,
    final_service_obj: null
  },
  mutations
}
