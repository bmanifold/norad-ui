import mutations from './mutations'

export default {
  namespaced: true,
  state: {
    port_number: null,
    transport_protocol: null,
    name: null,
    description: null,
    allow_brute_force: null,
    username_field: null,
    password_field: null,
    username: null,
    password: null,
    encryption_protocol: 'cleartext',
    service_type: 'generic',
    web_auth_method: null,
    web_start_url: null,
    web_blacklist: null,
    ident_required: null,
    application_port: null
  },
  mutations
}
