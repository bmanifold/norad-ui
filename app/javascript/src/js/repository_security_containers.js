$(function() {
  $(document).on('click', '#submit-container', function(e) {
    e.preventDefault();
    return $('#container-form').submit();
  });

  $(document).on('change', '#yaml-upload', function(e) {
    const filename = $(e.target).val().split('\\').pop();
    return $('#yaml-upload-button-txt').html(filename);
  });

  return $(document).on('click', '[data-target^="#container-details"]', function(e) {
    e.preventDefault();
    const $i = $(e.target);
    $i.toggleClass('fa-caret-right');
    return $i.toggleClass('fa-caret-down');
  });
});
