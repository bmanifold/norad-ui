$(function() {
  $('.show-output').on('click', function(e) {
    e.preventDefault();
    return $(e.target).closest('.result-row').find('.terminal-output').slideToggle();
  });

  $('.show-description').on('click', function(e) {
    e.preventDefault();
    return $(e.target).closest('.result-row').find('.result-description').slideToggle();
  });

  /* Settings Pane */
  $('#ssh-config').on('ajax:success', (e, data, status, xhr) => 
    Norad.alert('SSH Config:', 'SSH Config saved!', 'success')
  ).on('ajax:error', (e, xhr, status, error) => 
      Norad.alert('SSH Config:', 'Could not save SSH Config', 'danger')
  );

  return $('.group-by-button').change(function(e) {
    $('.result-views').children().toggle(false);
    return $($(e.currentTarget).data('results-target')).toggle();
  });
});
