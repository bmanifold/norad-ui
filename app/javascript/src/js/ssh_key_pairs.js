$(() =>
  $('.delete-ssh-key-pair').on('ajax:success', (e, data, status, xhr) =>
    $(e.target).closest('.ssh-key-box').remove()
  ).on('ajax:error', (e, xhr, status, error) =>
    Norad.alert('Delete SSH Key Pair:', 'Could not delete the SSH Key Pair!', 'danger')
  )
);

const ssh_key_value = () => $('#ssh-key .form-control').val().trim();

const valid_ssh_key = function(value) {
  if (!valid_characters(value)) { return false; }
  if (!valid_header(value)) { return false; }
  if (!valid_footer(value)) { return false; }
  $('#save-ssh-key-pair').prop('disabled', false);
  return true;
};

var valid_characters = function(value) {
  const cert = lines(value);
  const body = cert.slice(1,cert.length - 1).join('');
  if (!/^[a-zA-Z0-9+/=\n]+$/.test(body)) { return false; }
  return true;
};

var valid_header = function(value) {
  const first_line = lines(value)[0];
  if (first_line === '-----BEGIN RSA PRIVATE KEY-----') { return true; }
  if (first_line === '-----BEGIN DSA PRIVATE KEY-----') { return true; }
  if (first_line === '-----BEGIN PRIVATE KEY-----') { return true; }
  return false;
};

var valid_footer = function(value) {
  const last_line = lines(value)[lines(value).length - 1];
  if (last_line === '-----END RSA PRIVATE KEY-----') { return true; }
  if (last_line === '-----END DSA PRIVATE KEY-----') { return true; }
  if (last_line === '-----END PRIVATE KEY-----') { return true; }
  return false;
};

var lines = value => value.split('\n');

const validate_ssh_key_on_change = function(e) {
  if (valid_ssh_key(ssh_key_value())) {
    return remove_error();
  } else {
    return display_error();
  }
};

var display_error = function() {
  $('#ssh-key .ssh-private-key-error.error-text').text(
    `Must be a private key for SSH protocol version 2 using the DSA or RSA algorithm. \
PKCS#1 and PKCS#8 format with the body in PEM format is supported.`
  );
  $('#save-ssh-key-pair').prop('disabled', true);
};

var remove_error = function() {
  $('#ssh-key .ssh-private-key-error.error-text').text('');
  $('#save-ssh-key-pair').prop('disabled', false);
};

const save_callback = function(e) {
  e.preventDefault();
  $('form.ssh-key-pair-form').submit();
};

// This event listener is triggered from new.js.haml.
$('body').on('ssh_key_pair_form', function(e) {
  $('#ssh-key-pair-form-modal').modal('show');
  $('#ssh-key .form-control').on('input', validate_ssh_key_on_change);
  $('button#ssh-key-pair-save-button').on('click', save_callback);
  $('#save-ssh-key-pair').prop('disabled', true);
});
