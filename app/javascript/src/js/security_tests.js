$('.req-list-toggle').on('click', function(e) {
  e.preventDefault();
  $(e.currentTarget).closest('.row').next().slideToggle();
  return $(e.currentTarget).find('i').toggleClass('fa-plus-circle fa-minus-circle');
});

$(() =>
  $('form#add-security-tests-form').on('submit', function(e) {
    if ($(e.target).find('select#security-test-list').val().length === 0) {
      alert("Oops! You forgot to select which tests you'd like to enable.");
      return false;
    }
  })
);
