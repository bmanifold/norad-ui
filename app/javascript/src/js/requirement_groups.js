$(() =>
  $('.remove-requirement-group').on('ajax:success', (e, data, status, xhr) => {
    $(e.target).closest('tr').remove()

    if ($('#requirement-groups-table tr.requirement-group-row').length == 0) {
      $("#no-requirement-groups-message").removeClass('hide')
      $("#requirement-groups-table").addClass('hide')
    }
  }).on('ajax:error', (e, xhr, status, error) =>
    Norad.alert('Remove Requirement Group', 'Could not remove the group!', 'danger')
  )
);
