$('select#iaas-provider-field').on('change', function(e) {
  const provider = $(e.target).val();
  const $form = $(e.target).closest('form');
  if (provider === 'aws') {
    $form.find('.region-field').prop('disabled', false);
    $form.find('.project-field').prop('disabled', true);
    $form.find('.project-field').val('');
    $form.find('.auth-field').prop('disabled', true);
    return $form.find('.auth-field').val('');
  } else {
    $form.find('.region-field').prop('disabled', true);
    $form.find('.region-field').val('');
    $form.find('.project-field').prop('disabled', false);
    return $form.find('.auth-field').prop('disabled', false);
  }
});

$('form#iaas_config_form').find('input#discover-assets').on('click', e => $('form#discovery-form').submit());
