$(function() {
  if ($('#service-form-modal.show-modal-via-js').length > 0) {
    $.rails.handleRemote($('<a>', {href: 'services/new?show_ssh_service_warning=true', 'data-method': 'GET'}));
  }
});
