$('.select2').select2({width: '100%'}).toggle();

$('.verify').on('click', function(e) {
  const $el = $(e.currentTarget);
  const id = $el.data('id');
  return $.ajax(`/relays/${id}`, {
    type: 'PUT',
    datatype: 'json',
    data: {'docker_relay': {'verified': $el[0].checked}},
    error(jqxhr, textStatus, error) {
      Norad.alert('Warning', 'Relay verification status was not changed!', 'danger');
      return $el.prop('checked', !$el.prop('checked'));
    },
    success(data, status, jqxhr) {
      return Norad.alert('Success', `Relay[${id}] successfully updated!`, 'success');
    }
  });
});

$('#auto-verify').on('click', function(e) {
  const $el = $(e.currentTarget);
  const id = $el.data('config-id');
  const setting = $el.data('config-setting');
  return $.ajax(`/organization_configurations/${id}`, {
    type: 'PUT',
    datatype: 'json',
    data: {'organization_configuration': {[setting]: $el[0].checked}},
    error(xhr, status, error) {
      Norad.alert('Warning', 'Cannot update org config!', 'danger');
      return $el.prop('checked', !$el.prop('checked'));
    },
    success(data, status, xhr) {
      return Norad.alert('Success', 'Org config successfully updated!', 'success');
    }
  });
});

$('#toggle-relay-ssh-key').on('click', function(e) {
  const $el = $('#ssh-key');
  const id = $el.data('config-id');
  const setting = $el.data('config-setting');
  return $.ajax(`/organization_configurations/${id}`, {
    type: 'PUT',
    datatype: 'json',
    data: {'organization_configuration': {[setting]: !$el[0].checked}},
    error(xhr, status, error) {
      Norad.alert('Warning', 'Cannot update org config!', 'danger');
      $('#relay-ssh-key-modal').modal('hide');
      return $el.prop('checked', !$el.prop('checked'));
    },
    success(data, status, xhr) {
      $('#relay-ssh-key-modal').modal('hide');
      toggle_relay_ssh_key_checked();
      return Norad.alert('Success', 'Org config successfully updated!', 'success');
    }
  });
});

$('#ssh-key').on('click', function(e) {
  e.preventDefault();
  if ($('#ssh-key').prop('checked')) {
    $('#relay-ssh-key-modal-warning').text(
      'Are you sure? By checking this setting, authenticated scans will use SSH keys stored on the Relay associated with this Organization. Please make sure any SSH keys needed for authenticated scans are placed on your Relay.'
    );
  } else {
    $('#relay-ssh-key-modal-warning').text(
      'Are you sure? By unchecking this setting, authenticated scans will use SSH keys stored in the Norad Cloud. Please make sure any SSH keys needed for authenticated scans are uploaded to the Norad Cloud and are associated with their respective Machines.'
    );
  }
  $('#relay-ssh-key-modal').modal('show');
});

var toggle_relay_ssh_key_checked = function () {
  $('#ssh-key').prop("checked", !$('#ssh-key').prop("checked"));
}

$('.organization-scan').on('ajax:error', function(e, xhr, status, error) {
  const msg = 'Something went wrong while starting the scan.  Please contact Norad Administration.';
  return Norad.alert('Warning', msg, 'danger');
});

$('input[type=checkbox].notification_channel_enabled').on('change', function() {
  const $form = $($(this).closest('form'));
  return $form.submit();
});

$('form.edit_notification_channel').on('ajax:success', (e, xhr, status, success) => Norad.alert('Success', 'Org Notification Channel successfully updated!', 'success'));

$('form.edit_notification_channel').on('ajax:error', (e, xhr, status, error) => Norad.alert('Warning', 'There was an error updating this notification channel.', 'danger'));
