/* eslint no-console: 0 */

import Vue from 'vue'
import VeeValidate from 'vee-validate';
import axios from 'axios'
import Orgs from '../components/organizations/organizations.vue'

Vue.prototype.$http = axios;
Vue.use(VeeValidate);

VeeValidate.Validator.extend('org_name_chars', {
    getMessage: field => 'The organization name is not allowed!',
    validate: (value) => {
      return /^[A-Za-z][A-Za-z0-9 _-]+$/.test(value)
    }
});

VeeValidate.Validator.extend('org_name_default', {
    getMessage: field => 'The organization name cannot end with "default"!',
    validate: (value) => {
      return !(/default$/i.test(value))
    }
});

document.addEventListener('DOMContentLoaded', () => {
  Vue.prototype.$http.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content');

  new Vue({
    el: 'organizations',
    data: {
      orgs: null,
      default_org: ''
    },
    beforeMount: function(){
      this.orgs = JSON.parse(this.$el.attributes.orgs.value);
      this.default_org = this.$el.attributes.default_org.value;
    },
    render: function(h){
      return h(Orgs, { props: { init_orgs: this.orgs, init_default_org: this.default_org } })
    }
  })
})
