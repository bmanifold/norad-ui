/* eslint no-console: 0 */

import Vue from 'vue'
import axios from 'axios'
import ConnectivityChecksBox from '../components/machines/settings/connectivity_checks_box.vue'

Vue.prototype.$http = axios;

document.addEventListener('DOMContentLoaded', () => {
  Vue.prototype.$http.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content');

  new Vue({
    el: 'connectivity-checks-box',
    beforeMount: function() {
      this.machine_id = JSON.parse(this.$el.attributes.machine_id.value)
    },
    render: function(h) {
      return h(ConnectivityChecksBox, { props: { machine_id: this.machine_id } })
    }
  })
})
