/* eslint no-console: 0 */

import Vue from 'vue'
import VeeValidate from 'vee-validate'
import axios from 'axios'
import AddServiceWizard from '../components/services/add_service_wizard.vue'
import store from 'store'

Vue.use(VeeValidate)

Vue.prototype.$http = axios;
Vue.prototype.$http.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content');

// This is purposefully loaded synchrounously because at the time this
// JS is loaded, the modal is already shown.
new Vue({
  el: 'add-service-wizard',
  store,
  beforeMount: function() {
    this.machine_id = JSON.parse(this.$el.attributes.machine_id.value)
    this.application_type_options = JSON.parse(this.$el.attributes.application_type_options.value)
  },
  render: function(h) {
    return h(AddServiceWizard,
      { props: { application_type_options: this.application_type_options, machine_id: this.machine_id } }
    )
  }
})

$('.toggle-new-service-wizard').on('click', () => {
  $('#service-form-container').toggleClass('hide')
  $('#service-wizard-container').toggleClass('hide')
})
