/* eslint no-console: 0 */

import Vue from 'vue'
import VeeValidate from 'vee-validate'
import axios from 'axios'
import MachineList from '../components/machines/machine-list.vue'
import store from 'store'

Vue.prototype.$http = axios
Vue.use(VeeValidate)

VeeValidate.Validator.extend('machine_name_length', {
  getMessage: field => "Machine name must not exceed 36 characters",
  validate: (value) => {
    return value.length <= 36
  }
});

VeeValidate.Validator.extend('machine_ip_not_local_host', {
  getMessage: field => "IP Addresses cannot be \"127.0.0.1\"",
  validate: (value) => {
    return value != "127.0.0.1"
  }
});

VeeValidate.Validator.extend('machine_ip', {
  getMessage: field => "IP Addresses must be in the following format: X.X.X.X (where X represents a number from 0 to 255)",
  validate: (value) => {
    return(
      !!(value.match(/^\d{1,3}\.\d{1,3}\.\d{1,3}.\d{1,3}$/)) &&
        value.split(".").every(function (e) { return parseInt(e) >= 0 && parseInt(e) <= 255; })
    )
  }
});

VeeValidate.Validator.extend('machine_fqdn', {
  getMessage: field => "Must be a valid FQDN",
  validate: (value) => {
    return !!value.match(/^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/)
  }
});

document.addEventListener('DOMContentLoaded', () => {
  Vue.prototype.$http.defaults.headers.common['X-CSRF-Token'] = $('meta[name="csrf-token"]').attr('content');

  new Vue({
    el: 'machine-list',
    store,
    data: {
      organization_id: null
    },
    render: function(h){
      return h(MachineList, { props: { organizationId: this.$el.attributes.organization_id.value } })
    }
  })
})
