# frozen_string_literal: true

class DockerCommand
  include Her::Model
  include PrettyErrors
  resource_path 'docker_commands/:id'
  include_root_in_json true

  has_many :assessments
end
