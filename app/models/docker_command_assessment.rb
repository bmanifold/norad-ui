# frozen_string_literal: true

class DockerCommandAssessment
  include Her::Model

  collection_path 'docker_commands/:docker_command_id/assessments'
end
