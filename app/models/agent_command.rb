# frozen_string_literal: true

class AgentCommand
  include Her::Model
  collection_path 'machines/:machine_id/agent_commands'
  resource_path 'agent_commands/:id'
  include_root_in_json true

  has_many :assessments
end
