# frozen_string_literal: true

class MachineScanSchedule < ScanSchedule
  belongs_to :machine
  collection_path 'machines/:machine_id/scan_schedules'
  resource_path 'machine_scan_schedules/:id'
end
