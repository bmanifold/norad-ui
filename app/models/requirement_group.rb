# frozen_string_literal: true

class RequirementGroup
  include Her::Model
  include PrettyErrors

  include_root_in_json true

  resource_path 'requirement_groups/:id'

  has_many :requirements
end
