# frozen_string_literal: true

class SshKeyPair
  include Her::Model
  collection_path 'organizations/:organization_id/ssh_key_pairs'
  resource_path 'ssh_key_pairs/:id'
  include_root_in_json true

  belongs_to :organization
end
