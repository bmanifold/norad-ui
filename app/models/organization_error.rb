# frozen_string_literal: true

class OrganizationError
  include Her::Model

  belongs_to :organization
end
