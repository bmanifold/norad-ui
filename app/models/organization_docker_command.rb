# frozen_string_literal: true

class OrganizationDockerCommand
  include Her::Model
  collection_path 'organizations/:organization_id/docker_commands'
  resource_path 'docker_commands/:id'

  has_many :assessments
end
