# frozen_string_literal: true

class PasswordReset
  include Her::Model
  include PrettyErrors

  primary_key :password_reset_token
end
