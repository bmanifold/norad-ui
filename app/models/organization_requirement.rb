# frozen_string_literal: true

class OrganizationRequirement < Requirement
  belongs_to :organization

  collection_path 'organizations/:organization_id/requirements'
  resource_path 'organization_requirements/:id'
end
