# frozen_string_literal: true

class PingConnectivityCheck
  include Her::Model
  include PrettyErrors

  collection_path 'machines/:machine_id/ping_connectivity_checks'
  resource_path 'machines/:machine_id/ping_connectivity_checks/:id'

  belongs_to :machine
end
