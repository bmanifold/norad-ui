# frozen_string_literal: true

class MachineResultIgnoreRule
  include Her::Model
  collection_path 'machines/:machine_id/result_ignore_rules'
  resource_path 'machines/:machine_id/result_ignore_rules/:id'
end
