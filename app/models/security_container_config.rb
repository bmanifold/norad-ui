# frozen_string_literal: true

class SecurityContainerConfig
  include Her::Model
  include_root_in_json true

  def configurable
    return Machine.find(machine_id) if machine_id
    return Organization.find(organization_id) if organization_id
  end

  def self.create_configs(model, container_ids, requirement = false)
    valid_container_ids(container_ids, model).map do |cid|
      container = SecurityContainer.find(cid)
      config = model.security_container_configs.create(security_container_config: {
                                                         security_container_id: cid,
                                                         enabled_outside_of_requirement: !requirement,
                                                         values: container.default_config
                                                       })
      { container: container, config: config }
    end
  end

  def self.valid_container_ids(new_ids, model)
    configured_ids = model.security_container_configs.map(&:security_container_id)
    new_ids.map(&:to_i) - configured_ids
  end

  private_class_method :valid_container_ids
end
