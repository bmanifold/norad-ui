# frozen_string_literal: true

class Machine
  include Her::Model
  include PrettyErrors
  include Commandable

  collection_path 'organizations/:organization_id/machines'
  resource_path 'machines/:id'

  has_one :agent
  has_one :ssh_key_pair
  has_many :agent_commands
  has_many :docker_commands, inverse_of: :machine, class_name: 'MachineDockerCommand'
  has_many :assessments
  has_many :result_ignore_rules, inverse_of: :machine, class_name: 'MachineResultIgnoreRule'
  has_many :security_container_configs, inverse_of: :machine, class_name: 'MachineContainerConfig'
  has_many :scan_schedules, inverse_of: :machine, class_name: 'MachineScanSchedule'
  has_many :services
  has_many :service_discoveries
  has_many :ping_connectivity_checks
  has_many :ssh_connectivity_checks
  belongs_to :organization

  custom_post :scan

  def key_without_ssh_service?
    organization.organization_errors.any? do |e|
      e['type'] == 'KeyPairAssignmentWithoutSshServiceError' && e['errable_type'] == 'Machine' && e['errable_id'] == id
    end
  end

  def latest_service_discovery
    service_discoveries&.sort { |a, b| b.created_at <=> a.created_at }.first
  end

  def ssh_service_without_key?
    organization.organization_errors.any? do |e|
      e['type'] == 'SshServiceWithoutKeyPairAssignmentError' && e['errable_type'] == 'Machine' && e['errable_id'] == id
    end
  end

  def latest_docker_command
    @latest_docker_command ||= MachineDockerCommand.get(:latest, _machine_id: id)
  rescue NoradApiExceptions::NotFoundError
    nil
  end

  def latest_assessments
    return [] if latest_docker_command.nil?

    latest_docker_command.assessments
  rescue NoradApiExceptions::NotFoundError
    []
  end

  def assessments_by_docker_command_id(dcid)
    assessments.where(docker_command_id: dcid)
  end

  def assessments_summary
    MachineScanSummary.find(id).assessments_summary
  end

  def status
    get_status latest_assessment_stats
  end

  def can_scan_be_disabled?
    !security_tests_present?
  end

  def security_tests_present?
    security_tests_configured? || organization.security_tests_configured?
  end

  private

  def security_tests_configured?
    security_container_configs.present?
  end

  # rubocop:disable Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
  def get_status(results)
    return 'default' if results.blank?

    if results[:failing].positive?
      'fail'
    elsif results[:warning].positive? || results[:erroring].positive?
      'warn'
    elsif results[:passing].positive?
      'pass'
    elsif results[:informing].positive?
      'info'
    else
      'default'
    end
  end
  # rubocop:enable Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
end
