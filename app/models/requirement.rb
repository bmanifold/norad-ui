# frozen_string_literal: true

class Requirement
  include Her::Model
  include_root_in_json true

  collection_path 'requirement_groups/:requirement_group_id/requirements'
  resource_path 'requirements/:id'

  belongs_to :requirement_group
  has_many :provisions
end
