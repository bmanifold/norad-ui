# frozen_string_literal: true

class OrganizationSecurityContainer < SecurityContainer
  collection_path 'organizations/:organization_id/security_containers'
  resource_path 'security_containers/:id'

  include_root_in_json :security_container

  belongs_to :organization
end
