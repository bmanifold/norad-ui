# frozen_string_literal: true

class OrganizationEnforcement
  include Her::Model

  collection_path 'organizations/:organization_id/enforcements'
  resource_path 'enforcements/:id'

  include_root_in_json :enforcement

  belongs_to :organization
  has_many :organization_requirements
end
