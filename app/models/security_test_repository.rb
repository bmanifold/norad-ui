# frozen_string_literal: true

class SecurityTestRepository
  include Her::Model
  include PrettyErrors

  include_root_in_json true

  has_many :security_containers, inverse_of: :security_test_repository, class_name: 'RepositorySecurityContainer'
  has_many :repository_whitelist_entries

  def enabled_for_org_id?(org_id)
    entry_for_org_id(org_id).present?
  end

  def entry_for_org_id(org_id)
    repository_whitelist_entries.detect { |entry| org_id.to_s == entry.organization.db_id.to_s }
  end

  # Prevents Her from trying to fetch the association when it has been redacted from the API
  def security_containers_in_response
    id && has_association?(:security_containers) ? security_containers : nil
  end

  def authorizer
    attributes['authorizer']&.with_indifferent_access
  end
end
