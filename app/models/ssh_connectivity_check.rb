# frozen_string_literal: true

class SshConnectivityCheck
  include Her::Model
  include PrettyErrors

  collection_path 'machines/:machine_id/ssh_connectivity_checks'
  resource_path 'machines/:machine_id/ssh_connectivity_checks/:id'

  belongs_to :machine
end
