# frozen_string_literal: true

class ScanSchedule
  include Her::Model
  include_root_in_json true

  def frequency
    at_defined? ? day_abbrev_to_full(day) : ''
  end

  def at_time
    at_defined? ? "#{time} UTC" : ''
  end

  def hour
    at_defined? ? time.split(':').first : ''
  end

  def padded_hour
    return '' unless at_defined?
    h = hour.to_i % 24
    h.to_i > 9 ? h : "0#{h}"
  end

  def minute
    at_defined? ? time.split(':').last : ''
  end

  def padded_minute
    return '' unless at_defined?
    m = minute
    m.to_i > 9 ? m : "0#{m}"
  end

  def am_pm
    return '' unless at_defined?
    hour.to_i > 11 ? 'PM' : 'AM'
  end

  def user_can_modify?(user, org)
    org.memberships.detect { |m| m.user == user }.role == 'admin'
  end

  private

  def time
    at.scan(/[0-2]?[0-9]:[0-5][0-9]\z/).first
  end

  def day
    at.scan(/\A[a-zA-Z]+/).first
  end

  def day_abbrev_to_full(day)
    return 'Daily' unless day
    return 'Tuesdays' if day.match?(/\Atue/i)
    return 'Wednesdays' if day.match?(/\Awed/i)
    return 'Thursdays' if day.match?(/\Athu/i)
    return 'Saturdays' if day.match?(/\Asat/i)
    day.slice(0, 3).capitalize + 'days'
  end

  def at_defined?
    defined?(at) && at
  end
end
