# frozen_string_literal: true

class ResultExportQueue
  include Her::Model
  collection_path 'organizations/:organization_id/result_export_queues'
  resource_path 'result_export_queues/:id'
  include_root_in_json true

  belongs_to :organization

  def cast_to_subclass
    return self unless valid_subclass_type
    type.constantize.new(attributes)
  end

  def custom_configuration
    configuration_name = "custom_#{type.underscore.gsub('_export_queue', '')}_configuration".to_sym
    begin
      send(configuration_name)
    rescue StandardError
      nil
    end
  end

  def edit_resource_path
    "edit_#{type.underscore}_path".to_sym
  end

  def resource_path
    "#{type.underscore}_path".to_sym
  end

  def valid_subclass_type
    %w[InfosecExportQueue JiraExportQueue].include? type
  end
end
