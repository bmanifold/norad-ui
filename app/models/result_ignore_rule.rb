# frozen_string_literal: true

class ResultIgnoreRule
  include Her::Model
  include PrettyErrors
  include_root_in_json true
end
