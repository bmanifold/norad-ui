# frozen_string_literal: true

class DockerRelay
  include Her::Model
  collection_path 'organizations/:organization_id/docker_relays'
  resource_path 'docker_relays/:id'
  include_root_in_json true

  belongs_to :organization
end
