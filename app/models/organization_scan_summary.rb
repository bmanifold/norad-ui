# frozen_string_literal: true

class OrganizationScanSummary
  include Her::Model

  resource_path 'organizations/:id/scan_summary'
end
