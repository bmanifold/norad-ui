# frozen_string_literal: true

class UserProfile < ApplicationRecord
  self.primary_key = :user_uid

  def self.update_defaults(original_slug, org)
    # using update_all avoids the (n+1) query problem and the time required
    # to instantiate each UserProfile object. However, update_all skips
    # validations and triggers a rubocop violation due to skipped validation.
    # rubocop:disable Rails/SkipsModelValidations
    where(default_org: original_slug).update_all(default_org: org.slug)
    # rubocop:enable Rails/SkipsModelValidations
  end
end
