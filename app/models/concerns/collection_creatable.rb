# frozen_string_literal: true

module CollectionCreatable
  extend ActiveSupport::Concern

  included do
    def self.create_collection(url, payload)
      parsed_data = post_raw(url, payload)[:parsed_data]

      # Her doesn't like when the API returns an array of errors with empty hashes, so manually build a resource
      # that properly responds to response_errors here if errors are present.
      if parsed_data[:errors].present? && parsed_data[:errors].any?(&:present?)
        dummy_record = new
        dummy_record.response_errors = parsed_data[:errors]
        dummy_record
      else
        new_collection parsed_data
      end
    end
  end
end
