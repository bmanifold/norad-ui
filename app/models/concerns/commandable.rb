# frozen_string_literal: true

module Commandable
  extend ActiveSupport::Concern

  included do
    def to_commandable_param_hash
      { "#{self.class.to_s.underscore}_id": id }
    end
  end
end
