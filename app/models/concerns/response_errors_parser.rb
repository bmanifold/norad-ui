# frozen_string_literal: true

class ResponseErrorsParser
  def initialize(errors)
    @errors = errors
  end

  def to_human
    if @errors.is_a? Array
      @errors.map { |e| parse_response_errors([e]) }.map(&:presence).compact.join('. ')
    else
      parse_response_errors(@errors)
    end
  end

  private

  # raw_errors can take the following forms, depending on whether any child associations' errors are included
  # in the API response and whether multiple records' errors are being rendered at once.
  #
  # 1. [{:name => ['is too short', 'has already been taken']}, {:name => ['must be alphanumeric']}]
  # 2. [[':security_test_repositories.security_containers.name', ['is too short', 'has already been taken']]]
  # 3. [':name', ['is too short', 'has already been taken']]
  def parse_response_errors(raw_errors)
    raw_errors.to_a.collect do |error|
      if error.present?
        error_arr = error.is_a?(Array) ? error : error.to_a.flatten(1)
        key = error_arr[0]
        messages = error_arr[1]
        messages.collect { |message| "#{error_subject_for(key)} #{message}" }.join('. ')
      else
        ''
      end
    end.join('. ')
  end

  # turns :foo.bar.baz into 'Baz'
  def error_subject_for(key)
    return '' if key.to_s.starts_with? 'base' # don't print base key
    key.to_s.split('.').last.titleize
  end
end
