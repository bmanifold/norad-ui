const webpackConfig = require('./config/webpack/test.js')

// Set headless chrome command
process.env.CHROME_BIN = 'google-chrome'

module.exports = function(config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine"],
    plugins: [
      "karma-jquery",
      "karma-jasmine-jquery",
      "karma-jasmine",
      "karma-webpack",
      "karma-chrome-launcher",
      "karma-coverage-istanbul-reporter",
      "karma-spec-reporter"
    ],
    files: [
      "tmp/routes.js",
      "spec/javascript/**/*.js"
    ],
    exclude: [],
    webpack: webpackConfig,
    preprocessors: {
      "tmp/routes.js": "webpack",
      "spec/javascript/**/*.js": "webpack"
    },
    mime: { "text/x-javascript": ["js"] },
    reporters: ["progress", "coverage-istanbul"],
    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovonly', 'text-summary' ],
      fixWebpackSourcePaths: true
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["ChromeNoSandbox"],
    customLaunchers: {
      ChromeNoSandbox: {
        base: 'ChromeHeadless',
        flags: [
          // These are needed because:
          //  1. We're running inside a docker container
          //  2. We're effectively running as root
          '--no-sandbox',
          '--disable-setuid-sandbox'
        ]
      }
    },
    // Prevent useless webpack info from being logged to console
    webpackMiddleware: {
      noInfo: true
    },
    singleRun: true
  });
};
