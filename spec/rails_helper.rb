# frozen_string_literal: true

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
require 'capybara/rspec'
require 'capybara/rails'
require 'securerandom'
require 'byebug'

# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
Dir[Rails.root.join('spec', 'support', '**', '*.rb')].each { |f| require f }
# rubocop:disable Style/MixinUsage
include SessionsTestHelper
# rubocop:enable Style/MixinUsage

Capybara.configure do |config|
  config.run_server = true
end
Capybara.default_max_wait_time = 4
Capybara.app_host = 'http://172.17.0.1:4000'
Capybara.server_host = '172.17.0.1'
Capybara.server_port = '4000'

Capybara.register_driver :headless_chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :remote,
                                      url: 'http://0.0.0.0:4444/wd/hub',
                                      desired_capabilities: Selenium::WebDriver::Remote::Capabilities.chrome)
end

Capybara.javascript_driver = :headless_chrome

def reload_page
  page.evaluate_script('window.location.reload()')
end

def valid_login(user = 'testuser')
  logout
  visit login_path
  fill_in 'session[uid]', with: user
  fill_in 'session[password]', with: 'goodpass'
  click_on 'sign-in'
  visit root_path
end

# Returns the parameterized name of the created organization
def create_org_helper(name = "test organization #{SecureRandom.hex}")
  visit organizations_path
  wait_for_vue
  click_on 'New Organization'
  fill_in id: 'org-name-input', with: name
  click_on 'Create'
  wait_for_vue

  name.parameterize
end

FAKE_RSA_PRIVATE_KEY = '-----BEGIN RSA PRIVATE KEY-----
MIIBzAIBAAJhAO6O+guiF3YQWkdBjZpxyM5Ivn0/U78pFwIdK8xITldZEd8ERo9n
1ZW5GWgSwk5qfpxu/9XR4AxdIJPRWBmcWPyPn7/VU2Ahga9bcFZeZ7u7PPhYnW+a
eihlK5pqwKtyzwIDAQABAmEAsLA9Cri3X76k3vnyg12bOQ4v6FwrDv7EB9DLS7aS
L6fM0L099aEWVJqP8sFSIPpF9i9cZfv84tiGFxShgFzTxlkfDASEpTFjXQNgHQY8
zaPfsCOCGYvP1GY9vun9IUVhAjEA/WOVZDsucQLStb/AzgIETRjGyaQSuRP+Hcdx
AkJF+vVVGQ0NZqprm5BPulsmeYfVAjEA8QRFj5BkXAPecMGHUgZc2pnRPAsmUNKc
aDcrRPi+hJ/ycPehhJeoRoy9zq3iqeYTAjEAlTXPAvbWojXHXr9MCgOmBMBSTusP
G21qdp67LVlZXC6breTBv3Mbar/QMyz5uxcBAjBKHS8q05MY/JOVht2O6qf5DN3b
oe4ZYnp4Taa/oeIsNGdAN9XySRSVBUiE2tHdvwMCMQCfBXABop3zntzNun2Gc854
mmeZxJykaYJelEzT3FW8iy9V79Flx+OSU9bWmkmEmno=
-----END RSA PRIVATE KEY-----'

def add_new_ssh_key_pair(org_slug)
  visit settings_organization_path(org_slug)
  click_on 'add-new-ssh-key-pair'
  fill_in 'ssh_key_pair[name]', with: 'RSA Key'
  fill_in 'ssh_key_pair[description]', with: 'Valid RSA Key Pair'
  fill_in 'ssh_key_pair[username]', with: 'root'
  fill_in 'ssh_key_pair[key]', with: FAKE_RSA_PRIVATE_KEY
  click_on 'save-ssh-key-pair'
end

def create_machine_helper(org_slug, **opts)
  visit organization_machines_path(org_slug)
  click_on 'New Machine'
  wait_for_ajax
  machine_name = SecureRandom.hex[0, 20]
  enter_machine_data(machine_name, opts)
  click_on 'Create'
  expect(page).to have_content('Machine successfully created!')
  new_machine_from_url(machine_name)
end

def new_machine_from_url(name)
  id = URI.parse(current_url).path[/\d+/]
  Machine.new id: id, name: name
end

def enter_machine_data(machine_name, **opts)
  fill_in id: 'machine_fqdn', with: "#{machine_name}.test.invalid"
  fill_in id: 'machine_ip', with: opts[:ip_address]
  fill_in id: 'machine_name', with: machine_name
  fill_in id: 'machine_description', with: 'Random machine'
end

def create_relay_helper(org_slug)
  visit organization_path(org_slug)
  token = find('.callout > span').text
  conn = Faraday.new(url: 'http://localhost:3000')
  conn.post('/v1/docker_relays', organization_token: token,
                                 docker_relay: {
                                   public_key: Base64.encode64(OpenSSL::PKey::RSA.new(4096).public_key.to_pem)
                                 })
end

def create_requirement_group_helper(name = "test group #{SecureRandom.hex}")
  visit new_requirement_group_path
  fill_in 'requirement_group[name]', with: name
  fill_in 'requirement_group[description]', with: 'Random group'
  click_on 'Submit'
  name
end

def create_requirement_helper(name = "test requirement #{SecureRandom.hex}")
  fill_in 'requirement[name]', with: name
  fill_in 'requirement[description]', with: 'Random requirement'
  click_on 'Add Requirement'
  name
end

def create_navigating_requirement_helper(
  requirement_group,
  name = "test requirement #{SecureRandom.hex}"
)
  visit requirement_groups_path
  click_on requirement_group
  fill_in 'requirement[name]', with: name
  fill_in 'requirement[description]', with: 'Random requirement'
  click_on 'Add Requirement'
  name
end

def add_sec_test_to_requirement_helper(requirement_group, requirement_name, test_name = 'vuls')
  test_regexp = /#{test_name}\z/
  visit requirement_groups_path
  click_on requirement_group
  find('tr', text: requirement_name.to_s).find('.fa.fa-gear').click
  find('.security-test-list ~ span.select2').click
  find('.select2-results').find('li', text: test_regexp).click
  click_on 'Add Security Tests'
  wait_for_ajax
end

# This will take a picture of the entire page regardless of how
# vertically long the page is.
def take_screenshot(file_name = screenshot.png)
  # Check to make sure file_name has .png extension
  file_name << '.png' unless /\.png$/.match?(file_name)

  width, height = page_dimensions
  resize_window(width, height)

  page.save_screenshot(file_name, full: true)
end

# rubocop:disable Metrics/LineLength
def page_dimensions
  # Get the actual page dimensions using javascript
  width = Capybara.page.execute_script('return Math.max(document.body.scrollWidth, document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth);')
  height = Capybara.page.execute_script('return Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);')
  [width, height]
end
# rubocop:enable Metrics/LineLength

def resize_window
  window = Capybara.current_session.driver.browser.manage.window
  window.resize_to(width + 100, height + 100)
end

# Add additional requires below this line. Rails is not loaded until this point!
#
# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.

# Checks for pending migration and applies them before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  config.example_status_persistence_file_path = Rails.root.join('spec', '.rspec_persistence_file')

  config.before :suite do
    mock_auth_type :reverse_proxy
  end

  # Like use_transactional_fixtures but for the API
  config.around :each do |spec|
    ApiTestHelper.begin_transaction
    spec.run
    ApiTestHelper.rollback_transaction
  end

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")
end
