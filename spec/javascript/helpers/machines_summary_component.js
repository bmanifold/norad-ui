import Vue from 'vue'
import store from 'store'

export function MachinesSummaryComponent (Component) {
  const Constructor = Vue.extend(Component)

  let constructor = new Constructor({
    store,
    created () {
      this.$store.commit(
        'machines/setOrganization',
        {
          organization: {
            machine_count: 10,
            machine_assessment_summary: {
              passing: 1,
              erroring: 2,
              warning: 3,
              failing: 4
            },
            contains_enabled_tests: false,
            uid: 'mytest',
            id: 1,
            slug: 'mytest'
          }
        }
      )
    }
  })

  let mounted = constructor.$mount()

  return mounted
}
