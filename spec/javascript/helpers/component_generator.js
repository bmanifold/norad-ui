import Vue from 'vue'
import { storeGenerator } from 'store'
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate)

export function ComponentGenerator (Component, obj) {
  let Constructor = Vue.extend(Component)
  return new Constructor({ store: storeGenerator(), ...obj })
}
