import Vue from 'vue'
import store from 'store'

import VeeValidate from 'vee-validate'
Vue.use(VeeValidate)

import axios from 'axios'
Vue.prototype.$http = axios
import moxios from 'moxios'

export function MachinesTableComponent (Component) {
  moxios.install()

  const Constructor = Vue.extend(Component)
  let constructor

  moxios.withMock(function () {
    constructor = new Constructor({
      store,
      props: {
        machines: {
          default: () => [
            {
              id: 1,
              ip: "1.1.1.1",
              status: 'pass',
              fqdn: "f.first",
              use_fqdn_as_target: false,
              name: "firstmachine",
              description: "first machine"
            },
            {
              id: 2,
              ip: "2.2.2.2",
              status: 'pass',
              fqdn: "f.second",
              use_fqdn_as_target: false,
              name: "secondmachine",
              description: "second machine"
            },
            {
              id: 3,
              ip: "3.3.3.3",
              status: 'warn',
              fqdn: "f.third",
              use_fqdn_as_target: false,
              name: "thirdmachine",
              description: "third machine"
            }
          ]
        }
      },
      created () {
        this.$store.commit(
          'machines/setOrganization',
          {
            organization: {
              contains_enabled_tests: false,
              uid: 'mytest',
              id: 1,
              slug: 'mytest'
            }
          }
        )
      }
    })
  })

  let mounted = constructor.$mount()
  moxios.uninstall()

  return mounted
}
