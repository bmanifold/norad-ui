import { createLocalVue, shallow } from '@vue/test-utils'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

const wrapperFactory = function (component, opts = {}) {
  let store = new Vuex.Store({
    modules: {
      results_by_requirement: {
        namespaced: true,
        getters: {
          dataInitialized () { return true },
          ...opts.getters
        },
        actions: {
          initData () {},
          ...opts.actions
        },
        mocks: {
          ...opts.mocks
        }
      }
    }
  })
  let propsData = opts.propsData

  return shallow(component, { store, localVue, propsData })
}

export { wrapperFactory }
