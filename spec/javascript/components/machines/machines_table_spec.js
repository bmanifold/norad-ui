import Vue from 'vue'
import MachinesTable from 'components/machines/machines-table.vue'
import { MachinesTableComponent } from 'helpers/machines_table_component'

describe('MachinesTable', () => {
  it('can delete machines', () => {
    let component = MachinesTableComponent(MachinesTable)

    let rows = Object.values(component.$el.querySelectorAll('a.machine-name')).map(link => link.innerHTML)
    expect(rows).toEqual(['firstmachine', 'secondmachine', 'thirdmachine'])

    component.$el.querySelectorAll('.fa-remove')[0].click()

    component.$nextTick(_ => {
      component.$el.$alert.click()
      rows = Object.values(component.$el.querySelectorAll('a.machine-name')).map(link => link.innerHTML)
      expect(rows).toEqual(['secondmachine', 'thirdmachine'])
    })
  })

  it('uses "name" property as the default sort column', () => {
    let component = MachinesTableComponent(MachinesTable)
    let rowOrder

    component.$nextTick(_ => {
      rowOrder = Object.values(component.$el.querySelectorAll('a.machine-name')).map(link => link.innerHTML)
      expect(rowOrder).toEqual(['firstmachine', 'secondmachine', 'thirdmachine'])

      component.$el.querySelector('.machine-name-sort').click()

      component.$nextTick(_ => {
        rowOrder = Object.values(component.$el.querySelectorAll('.machine-name')).map(link => link.innerHTML)
        expect(rowOrder).toEqual(['thirdmachine', 'secondmachine', 'firstmachine'])
      })
    })
  })

  it('sorts and reverse sorts by "scan target" properly', () => {
    let component = MachinesTableComponent(MachinesTable)
    let rowOrder

    component.$el.querySelector('.machine-scan-target-sort').click()

    component.$nextTick(_ => {
      rowOrder = Object.values(component.$el.querySelectorAll('.machine-scan-target')).map(link => link.innerHTML)
      expect(rowOrder).toEqual(['1.1.1.1', '2.2.2.2', '3.3.3.3'])

      component.$el.querySelector('.machine-scan-target-sort').click()

      component.$nextTick(_ => {
        rowOrder = Object.values(component.$el.querySelectorAll('.machine-scan-target')).map(link => link.innerHTML)
        expect(rowOrder).toEqual(['3.3.3.3', '2.2.2.2', '1.1.1.1'])
      })
    })
  })

  it('sorts and reverse sorts by "status" properly', () => {
    let component = MachinesTableComponent(MachinesTable)
    let rowOrder

    component.$el.querySelector('.machine-status-sort').click()

    component.$nextTick(_ => {
      rowOrder = Object.values(component.$el.querySelectorAll('.machine-status')).map(link => link.innerHTML)

      expect(rowOrder).toEqual(['Pass', 'Pass', 'Warn'])

      component.$el.querySelector('.machine-status-sort').click()

      component.$nextTick(_ => {
        rowOrder = Object.values(component.$el.querySelectorAll('.machine-status')).map(link => link.innerHTML)
        expect(rowOrder).toEqual(['Warn', 'Pass', 'Pass'])
      })
    })
  })

  it('defaults to "name" for sorted column and its sort direction toggles properly', () => {
    let component = MachinesTableComponent(MachinesTable)

    expect(component.$el.querySelector('a.machine-name-sort i').classList.contains('fa-sort-alpha-asc')).toBe(true)

    component.$el.querySelector('.machine-name-sort').click()
    component.$nextTick(_ => {
      expect(component.$el.querySelector('a.machine-name-sort i').classList.contains('fa-sort-alpha-desc')).toBe(true)
      expect(component.$el.querySelector('a.machine-name-sort i').classList.contains('inactive-sort-button')).toBe(false)

      expect(component.$el.querySelector('a.machine-scan-target-sort i').classList.contains('inactive-sort-button')).toBe(true)
      expect(component.$el.querySelector('a.machine-status-sort i').classList.contains('inactive-sort-button')).toBe(true)
    })
  })

  it('toggles sort direction when sorting by "scan target"', () => {
    let component = MachinesTableComponent(MachinesTable)

    component.$el.querySelector('.machine-scan-target-sort').click()
    component.$nextTick(_ => {
      expect(component.$el.querySelector('a.machine-scan-target-sort i').classList.contains('fa-sort-alpha-asc')).toBe(true)
      expect(component.$el.querySelector('a.machine-scan-target-sort i').classList.contains('inactive-sort-button')).toBe(false)

      expect(component.$el.querySelector('a.machine-name-sort i').classList.contains('inactive-sort-button')).toBe(true)
      expect(component.$el.querySelector('a.machine-status-sort i').classList.contains('inactive-sort-button')).toBe(true)

      component.$el.querySelector('.machine-scan-target-sort').click()
      component.$nextTick(_ => {
        expect(component.$el.querySelector('a.machine-scan-target-sort i').classList.contains('fa-sort-alpha-desc')).toBe(true)
      })
    })
  })

  it('toggles sort direction when sorting by "status"', () => {
    let component = MachinesTableComponent(MachinesTable)

    component.$el.querySelector('.machine-status-sort').click()
    component.$nextTick(_ => {
      expect(component.$el.querySelector('a.machine-status-sort i').classList.contains('fa-sort-alpha-asc')).toBe(true)
      expect(component.$el.querySelector('a.machine-status-sort i').classList.contains('inactive-sort-button')).toBe(false)

      expect(component.$el.querySelector('a.machine-scan-target-sort i').classList.contains('inactive-sort-button')).toBe(true)
      expect(component.$el.querySelector('a.machine-name-sort i').classList.contains('inactive-sort-button')).toBe(true)

      component.$el.querySelector('.machine-status-sort').click()
      component.$nextTick(_ => {
        expect(component.$el.querySelector('a.machine-status-sort i').classList.contains('fa-sort-alpha-desc')).toBe(true)
      })
    })
  })
})
