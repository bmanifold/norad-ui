import Vue from 'vue'
import MachinesSummary from 'components/machines/machines-summary.vue'
import { MachinesSummaryComponent } from 'helpers/machines_summary_component'
const Component = MachinesSummaryComponent(MachinesSummary)

describe('MachinesSummary', () => {
  it('shows the machine count', () => {
    expect(Component.$el.querySelector('#machine-count').innerHTML).toEqual('10')
  })

  it('shows the machine assessment summary passing count', () => {
    expect(Component.$el.querySelector('#machine-passing-count').innerHTML).toEqual('1')
  })

  it('shows the machine assessment summary erroring and warning count', () => {
    expect(Component.$el.querySelector('#machine-erroring-and-warning-count').innerHTML).toEqual('5')
  })

  it('shows the machine assessment summary failing count', () => {
    expect(Component.$el.querySelector('#machine-failing').innerHTML).toEqual('4')
  })
})
