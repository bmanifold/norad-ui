import Vue from 'vue'
import MachineList from 'components/machines/machine-list.vue'
import { MachinesListComponent } from 'helpers/machines_list_component'
const Component = MachinesListComponent(MachineList)

describe('MachineList', () => {
  it('allows filtering by status', () => {
    Component.$el.querySelector(".v-select input[type=search]").value = 'Warn'
    Component.$nextTick(_ => {
      let rows = Object.values(component.$el.querySelectorAll('.machine-status')).map(link => link.innerHTML)
      expect(rows).toEqual(['Warn'])
    })
  })
})
