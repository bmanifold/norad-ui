import OrganizationIgnoreButton from 'components/results/by_requirement/organization_ignore_button.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('OrganizationIgnoreButton', () => {
  let wrapper
  const wrapperOpts = {
    getters: {
      org_id () { return 1 }
    }
  }

  beforeEach(() => {
    wrapper = wrapperFactory(OrganizationIgnoreButton, wrapperOpts)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('calls createIgnoreRule and incremementIgnoringCount when button is clicked', () => {
    spyOn(wrapper.vm, 'createIgnoreRule')
    spyOn(wrapper.vm, 'incrementIgnoringCount')
    wrapper.setProps({
      ignored: false,
      result_signature: 'test',
      requirement_id: 0
    })

    wrapper.trigger('click')

    expect(wrapper.vm.createIgnoreRule).toHaveBeenCalledWith({
      ignore_scope: 'organization',
      ignore_scope_id: 1,
      result_signature: 'test'
    })
    expect(wrapper.vm.incrementIgnoringCount).toHaveBeenCalledWith(0)
  })
})
