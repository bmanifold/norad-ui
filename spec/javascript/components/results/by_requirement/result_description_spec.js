import ResultDescription from 'components/results/by_requirement/result_description.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('ResultDescription', () => {
  let wrapper

  afterEach(() => {
    wrapper.destroy()
  })

  describe('when no description is provided', () => {
    beforeEach(() => {
      wrapper = wrapperFactory(ResultDescription)
    })

    it('returns "No Description Provided"', () => {
      expect(wrapper.vm.displayed_description).toBe('No Description Provided')
    })
  })

  describe('when description is provided', () => {
    beforeEach(() => {
      wrapper = wrapperFactory(ResultDescription, {
        propsData: {
          result_description: 'test description'
        }
      })
    })

    it('returns the result_description property', () => {
      expect(wrapper.vm.displayed_description).toBe('test description')
    })
  })
})
