import Requirements from 'components/results/by_requirement/requirements.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('Requirements', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(Requirements, {
      getters: {
        requirements () {
          return {}
        }
      }
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('has the requirements getter defined', () => {
    expect(wrapper.vm.requirements).toBeDefined()
  })
})
