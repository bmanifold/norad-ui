import MachineIgnoreButton from 'components/results/by_requirement/machine_ignore_button.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('MachineIgnoreButton', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(MachineIgnoreButton)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('calls createIgnoreRule and incremementIgnoringCount when button is clicked', () => {
    spyOn(wrapper.vm, 'createIgnoreRule')
    spyOn(wrapper.vm, 'incrementIgnoringCount')
    wrapper.setProps({
      ignored: false,
      machine_id: -1,
      result_signature: 'test',
      requirement_id: 0
    })

    wrapper.trigger('click')

    expect(wrapper.vm.createIgnoreRule).toHaveBeenCalledWith({
      ignore_scope: 'machine',
      ignore_scope_id: -1,
      result_signature: 'test'
    })
    expect(wrapper.vm.incrementIgnoringCount).toHaveBeenCalledWith(0)
  })
})
