import ResultsByRequirement from 'components/results/by_requirement/by_requirement.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('ResultsByRequirement', () => {
  let reqs, wrapper
  const wrapperOpts = {
    getters: {
      requirements () {
        return reqs
      }
    },
    actions: {}
  }

  const w = () => {
    wrapper = wrapperFactory(ResultsByRequirement, wrapperOpts)
    return wrapper
  }

  afterEach(() => {
    wrapper.destroy()
  })

  it('computes requirements_empty when reqs is null', () => {
    reqs = null
    expect(w().vm.requirements_empty).toBe(true)
  })

  it('computes requirements_empty when reqs is empty', () => {
    reqs = {}
    expect(w().vm.requirements_empty).toBe(true)
  })

  it('computes requirements_empty when reqs is provided', () => {
    reqs = { foo: 'bar' }
    expect(w().vm.requirements_empty).toBe(false)
  })

  it('has the mounted hook', () => {
    expect(typeof ResultsByRequirement.mounted).toBe('function')
  })

  it('has props defined', () => {
    let expected_keys = Object.keys(ResultsByRequirement.props)
    expect(expected_keys).toEqual(['docker_command_id', 'org_id', 'export_queues_present'])
  })

  it('calls initData when mounted', () => {
    wrapper = wrapperFactory(ResultsByRequirement, wrapperOpts)
    wrapper.setProps({ docker_command_id: 1, org_id: 2 })
    spyOn(wrapper.vm, 'initData')

    wrapper.vm.$mount()
    expect(wrapper.vm.initData).toHaveBeenCalledWith({
      docker_command_id: 1,
      org_id: 2,
      export_queues_present: false
    })
  })
})
