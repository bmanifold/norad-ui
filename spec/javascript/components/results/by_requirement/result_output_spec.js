import ResultOutput from 'components/results/by_requirement/result_output.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('ResultOutput', () => {
  let wrapper

  afterEach(() => {
    wrapper.destroy()
  })

  describe('when no output is provided', () => {
    beforeEach(() => {
      wrapper = wrapperFactory(ResultOutput)
    })

    it('returns "Output is empty."', () => {
      expect(wrapper.vm.displayed_output).toBe('Output is empty.')
    })
  })

  describe('when output is provided', () => {
    beforeEach(() => {
      wrapper = wrapperFactory(ResultOutput, {
        propsData: {
          result_output: 'test output'
        }
      })
    })

    it('returns the result_output', () => {
      expect(wrapper.vm.displayed_output).toBe('test output')
    })
  })
})
