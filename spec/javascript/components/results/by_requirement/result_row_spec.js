import ResultRow from 'components/results/by_requirement/result_row.vue'
import ResultOutput from 'components/results/by_requirement/result_output.vue'
import ResultDescription from 'components/results/by_requirement/result_output.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('ResultRow', () => {
  let wrapper
  let result = {
    signature: 'test signature',
    machine_id: 1
  }

  afterEach(() => {
    wrapper.destroy()
  })

  describe('when result is ignored', () => {
    describe('and show_ignored_results is true', () => {
      beforeEach(() => {
        wrapper = wrapperFactory(ResultRow, {
          getters: {
            ignored_result_signatures () { return ['test signature'] },
            show_ignored_results () { return true }
          },
          propsData: { result }
        })
      })

      it('show_result should be true', () => {
        expect(wrapper.vm.show_result).toBe(true)
      })
    })

    describe('and show_ignored_results is false', () => {
      beforeEach(() => {
        wrapper = wrapperFactory(ResultRow, {
          getters: {
            ignored_result_signatures () { return ['test signature'] },
            show_ignored_results () { return false }
          },
          propsData: { result }
        })
      })

      it('show_result should be false', () => {
        expect(wrapper.vm.show_result).toBe(false)
      })
    })
  })

  describe('when result is not ignored', () => {
    describe('and show_ignored_results is true', () => {
      beforeEach(() => {
        wrapper = wrapperFactory(ResultRow, {
          getters: {
            ignored_result_signatures () { return [] },
            show_ignored_results () { return true }
          },
          propsData: { result }
        })
      })

      it('show_result should be true', () => {
        expect(wrapper.vm.show_result).toBe(true)
      })
    })

    describe('and show_ignored_results is false', () => {
      beforeEach(() => {
        wrapper = wrapperFactory(ResultRow, {
          getters: {
            ignored_result_signatures () { return [] },
            show_ignored_results () { return false }
          },
          propsData: { result }
        })
      })

      it('show_result should be true', () => {
        expect(wrapper.vm.show_result).toBe(true)
      })
    })
  })

  describe('when clicking Result view buttons', () => {
    beforeEach(() => {
      wrapper = wrapperFactory(ResultRow, {
        getters: {
          ignored_result_signatures () { return [] },
          show_ignored_results () { return false }
        },
        propsData: { result }
      })
    })

    it('should toggle show_output', () => {
      // if a user clicks on the button text
      expect(wrapper.vm.show_output).toBe(false)
      wrapper.findAll('#toggle-output').at(0).trigger('click')
      expect(wrapper.vm.show_output).toBe(true)
      wrapper.findAll('#toggle-output').at(0).trigger('click')
      expect(wrapper.vm.show_output).toBe(false)

      // if a user clicks on the button icon
      wrapper.findAll('#toggle-output i').at(0).trigger('click')
      expect(wrapper.vm.show_output).toBe(true)
      wrapper.findAll('#toggle-output i').at(0).trigger('click')
      expect(wrapper.vm.show_output).toBe(false)
    })

    it('should toggle show_description', () => {
      // if a user clicks on the button text
      expect(wrapper.vm.show_description).toBe(false)
      wrapper.findAll('#toggle-description').at(0).trigger('click')
      expect(wrapper.vm.show_description).toBe(true)
      wrapper.findAll('#toggle-description').at(0).trigger('click')
      expect(wrapper.vm.show_description).toBe(false)

      // if a user clicks on the button icon
      wrapper.findAll('#toggle-description i').at(0).trigger('click')
      expect(wrapper.vm.show_description).toBe(true)
      wrapper.findAll('#toggle-description i').at(0).trigger('click')
      expect(wrapper.vm.show_description).toBe(false)
    })
  })

  describe('when computing machine_link', () => {
    beforeEach(() => {
      wrapper = wrapperFactory(ResultRow, {
        getters: {
          ignored_result_signatures () { return [] },
          show_ignored_results () { return false }
        },
        propsData: { result }
      })
    })

    it('should be the machine_path', () => {
      expect(wrapper.vm.machine_link).toBe(Routes.machine_path(1))
    })
  })
})
