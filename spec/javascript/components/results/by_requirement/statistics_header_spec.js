import StatisticsHeader from 'components/results/by_requirement/statistics_header.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('StatisticsHeader', () => {
  let wrapper
  const requirement = {
    ignoring_count: 1,
    passing_count: 2,
    failing_count: 3,
    warning_count: 4,
    erroring_count: 5,
    informing_count: 6,
    total_assessments: 7,
    completed_assessments: 8
  }

  beforeEach(() => {
    wrapper = wrapperFactory(StatisticsHeader, { propsData: { requirement }})
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('should compute ignored', () => {
    expect(wrapper.vm.ignored).toBe(1)
  })

  it('should compute passed', () => {
    expect(wrapper.vm.passed).toBe(2)
  })

  it('should compute failed', () => {
    expect(wrapper.vm.failed).toBe(3)
  })

  it('should compute warned', () => {
    expect(wrapper.vm.warned).toBe(4)
  })

  it('should compute errored', () => {
    expect(wrapper.vm.errored).toBe(5)
  })

  it('should compute info', () => {
    expect(wrapper.vm.info).toBe(6)
  })

  it('should compute total', () => {
    expect(wrapper.vm.total).toBe(7)
  })

  it('should compute completed', () => {
    expect(wrapper.vm.completed).toBe(8)
  })
})
