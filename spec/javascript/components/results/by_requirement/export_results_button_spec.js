import ExportResultsButton from 'components/results/by_requirement/export_results_button.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('ExportResultButton', () => {
  let wrapper
  const wrapperOpts = {
    getters: {
      export_queues_present () {
        return true
      },
      exported_result_ids () {
        return [-1]
      }
    }
  }

  beforeEach(() => {
    wrapper = wrapperFactory(ExportResultsButton, wrapperOpts)
  })

  afterEach(() => {
    wrapper.destroy()
  })


  it('computes disabled_class', () => {
    expect(wrapper.vm.disabled_class).toBe('')
    wrapper.setProps({ result_id: -1 })
    expect(wrapper.vm.disabled_class).toBe('disabled')
  })

  it('computes result_exported', () => {
    expect(wrapper.vm.result_exported).toBe(false)
    wrapper.setProps({ result_id: -1 })
    expect(wrapper.vm.result_exported).toBe(true)
  })

  it('computes button_title', () => {
    expect(wrapper.vm.button_title).toBe('Export Results to All Export Locations')
    wrapper.setProps({ result_id: -1 })
    expect(wrapper.vm.button_title).toBe('Result Exported Successfully')
  })

  it('calls exportResults when clicked', () => {
    spyOn(wrapper.vm, 'exportResults')
    wrapper.setProps({ result_id: 0 })
    wrapper.trigger('click')
    expect(wrapper.vm.exportResults).toHaveBeenCalledWith(0)
  })
})
