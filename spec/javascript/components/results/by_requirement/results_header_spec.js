import ResultsHeader from 'components/results/by_requirement/results_header.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('ResultsHeader', () => {
  let wrapper

  afterEach(() => {
    wrapper.destroy()
  })

  describe('when computing caret_class', () => {
    const getters = {
      ignored_result_signatures () { return [] },
      show_ignored_results () {}
    }

    describe('when expanded', () => {
      beforeEach(() => {
        const propsData = {
          req: { test_results: ['dummy'] },
          expanded: true,
          scope: 'test'
        }
        wrapper = wrapperFactory(ResultsHeader, { getters, propsData })
      })

      it('should be fa-caret-down', () => {
        expect(wrapper.vm.caret_class).toBe('fa-caret-down')
      })
    })

    describe('when not expanded', () => {
      beforeEach(() => {
        const propsData = {
          req: { test_results: ['dummy'] },
          expanded: false,
          scope: 'test'
        }
        wrapper = wrapperFactory(ResultsHeader, { getters, propsData })
      })

      it('should be fa-caret-right', () => {
        expect(wrapper.vm.caret_class).toBe('fa-caret-right')
      })
    })
  })

  describe('when computing ignored_result_count', () => {
    let propsData, getters

    describe('when ignored results are present', () => {
      beforeEach(() => {
        getters = {
          ignored_result_signatures () {
            return ['sig1', 'sig2']
          },
          show_ignored_results () {}
        }
        propsData = {
          req: {
            test_results: [{ signature: 'sig1' }, { signature: 'missing' }]
          },
          scope: 'test'
        }
        wrapper = wrapperFactory(ResultsHeader, { getters, propsData })
      })

      it('should only include the ignored results', () => {
        expect(wrapper.vm.ignored_result_count).toBe(1)
      })
    })

    describe('when no results are present', () => {
      beforeEach(() => {
        getters = {
          ignored_result_signatures () {
            return ['sig1', 'sig2']
          },
          show_ignored_results () {}
        }
        propsData = {
          req: {
            test_results: []
          },
          scope: 'test'
        }
        wrapper = wrapperFactory(ResultsHeader, { getters, propsData })
      })

      it('should return 0', () => {
        expect(wrapper.vm.ignored_result_count).toBe(0)
      })
    })
  })
})
