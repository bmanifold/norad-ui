import Requirement from 'components/results/by_requirement/requirement.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('Requirement', () => {
  let wrapper

  // Utility function to mock a specific category of result count to 1 to
  // test all of view's logic
  const oneHot = (hot_key) => {
    let requirement = {}
    const keys = ['failing_count', 'erroring_count', 'warning_count', 'passing_count', 'informing_count']
    for (let key of keys) {
      if (key === hot_key) requirement[key] = 1
      else requirement[key] = 0
    }
    wrapper.setProps({ requirement })
  }

  beforeEach(() => {
    wrapper = wrapperFactory(Requirement, {
      propsData: {
        requirement: {
          failing_count: 0,
          erroring_count: 0,
          warning_count: 0,
          passing_count: 0,
          informing_count: 0
        }
      }
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('computes box_class to box-danger when failing_count > 0', () => {
    oneHot('failing_count')
    expect(wrapper.vm.box_class).toBe('box-danger')
  })

  it('computes box_class to box-warning when erroring_count > 0', () => {
    oneHot('erroring_count')
    expect(wrapper.vm.box_class).toBe('box-warning')
  })

  it('computes box_class to box-warning when warning_count > 0', () => {
    oneHot('warning_count')
    expect(wrapper.vm.box_class).toBe('box-warning')
  })

  it('computes box_class to box-success when passing_count > 0', () => {
    oneHot('passing_count')
    expect(wrapper.vm.box_class).toBe('box-success')
  })

  it('computes box_class to box-info when informing_count > 0', () => {
    oneHot('informing_count')
    expect(wrapper.vm.box_class).toBe('box-info')
  })

  it('computes box_class to empty string when no results', () => {
    oneHot('')
    expect(wrapper.vm.box_class).toBe('')
  })

  it('collapses successful results', () => {
    oneHot('passing_count')
    expect(wrapper.vm.collapsed).toBe(true)
  })

  it('collapses informing results', () => {
    oneHot('informing_count')
    expect(wrapper.vm.collapsed).toBe(true)
  })

  it('collapses empty results', () => {
    oneHot('')
    expect(wrapper.vm.collapsed).toBe(true)
  })

  it('does not collapse failing results', () => {
    oneHot('failing_count')
    expect(wrapper.vm.collapsed).toBe(false)
  })

  it('does not collapse erroring results', () => {
    oneHot('erroring_count')
    expect(wrapper.vm.collapsed).toBe(false)
  })

  it('does not collapse warning results', () => {
    oneHot('warning_count')
    expect(wrapper.vm.collapsed).toBe(false)
  })

  it('computes icon_class to be fa-plus when collapsed', () => {
    // There's no straightforward to mock computed properties, so set the appropriate props for 'collapsed'
    oneHot('')
    expect(wrapper.vm.icon_class).toBe('fa-plus')
  })

  it('computes icon_class to be fa-minus when not collapsed', () => {
    // There's no straightforward to mock computed properties, so set the appropriate props for 'collapsed'
    oneHot('failing_count')
    expect(wrapper.vm.icon_class).toBe('fa-minus')
  })
})
