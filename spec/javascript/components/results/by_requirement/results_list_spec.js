import ResultsList from 'components/results/by_requirement/results_list.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('ResultsList', () => {
  let wrapper

  beforeEach(() => {
    wrapper = wrapperFactory(ResultsList, {
      propsData: {
        scope: 'test',
        req: {}
      }
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  describe('when computing el_class', () => {
    describe('when expanded', () => {
      beforeEach(() => {
        wrapper.setProps({ expanded: true })
      })

      it('should be "in"', () => {
        expect(wrapper.vm.el_class).toBe('in')
      })
    })

    describe('when not expanded', () => {
      beforeEach(() => {
        wrapper.setProps({ expanded: false })
      })

      it('should be empty', () => {
        expect(wrapper.vm.el_class).toBe('')
      })
    })
  })

  describe('when computing el_id', () => {
    beforeEach(() => {
      wrapper.setProps({ req: { id: -1 } })
    })

    it('should be made up of the scope and req_id', () => {
      expect(wrapper.vm.el_id).toBe('test-results--1')
    })
  })
})
