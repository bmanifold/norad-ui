import StopIgnoringButton from 'components/results/by_requirement/stop_ignoring_button.vue'
import { wrapperFactory } from 'helpers/results_by_requirement_helper'

describe('StopIgnoringButton', () => {
  let wrapper
  const propsData = {
    ignored: false,
    machine_id: -1,
    result_signature: 'test signature',
    requirement_id: 1
  }

  beforeEach(() => {
    wrapper = wrapperFactory(StopIgnoringButton, { propsData })
    spyOn(wrapper.vm, 'stopIgnoring')
    spyOn(wrapper.vm, 'decrementIgnoringCount')
  })

  afterEach(() => {
    wrapper.destroy()
  })

  describe('when clicked', () => {
    it('should call stopIgnoring with proper arguments', () => {
      wrapper.trigger('click')
      expect(wrapper.vm.stopIgnoring).toHaveBeenCalledWith({
        result_signature: 'test signature',
        org_id: undefined,
        machine_id: -1
      })
    })

    it('should call decrementIgnoringCount with proper arguments', () => {
      wrapper.trigger('click')
      expect(wrapper.vm.decrementIgnoringCount).toHaveBeenCalledWith(1)
    })
  })
})
