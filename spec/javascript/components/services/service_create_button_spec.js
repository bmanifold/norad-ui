import ServiceCreateButton from 'components/services/service_create_button.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceCreateButton', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceCreateButton)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('has the create method', () => {
    expect(typeof Component.create).toBe('function')
  })

  it('has the props defined', () => {
    expect(Object.keys(ServiceCreateButton.props).sort()).toEqual(['hidden', 'machine_id'])
  })
})
