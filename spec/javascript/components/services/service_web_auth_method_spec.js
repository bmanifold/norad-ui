import ServiceWebAuthMethod from 'components/services/service_web_auth_method.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceWebAuthMethod', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceWebAuthMethod)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('next_view is service_creds by default', () => {
    expect(Component.next_view).toEqual('service_creds')
  })
})
