import ServiceCreds from 'components/services/service_creds.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceCreds', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceCreds)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('next_view is service_encryption_protocol', () => {
    expect(Component.next_view).toEqual('service_encryption_protocol')
  })
})
