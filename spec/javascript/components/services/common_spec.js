import Common from 'components/services/common.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('Common', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(Common)
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('has the mounted hook', () => {
    expect(typeof Common.mounted).toBe('function')
  })

  it('has the setServiceProp method defined', () => {
    expect(typeof Component.setServiceProp).toBe('function')
  })

  it('has the setPortDefaults method defined', () => {
    expect(typeof Component.setPortDefaults).toBe('function')
  })
})
