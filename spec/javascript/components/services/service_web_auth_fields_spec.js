import ServiceWebAuthFields from 'components/services/service_web_auth_fields.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceWebAuthFields', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceWebAuthFields)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('next_view is service_creds', () => {
    expect(Component.next_view).toEqual('service_creds')
  })
})
