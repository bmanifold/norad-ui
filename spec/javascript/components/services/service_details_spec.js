import ServiceDetails from 'components/services/service_details.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceDetails', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceDetails)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('has the mounted hook', () => {
    expect(typeof ServiceDetails.mounted).toBe('function')
  })

  it('next_view is service_create by default', () => {
    Component.$nextTick(() => {
      expect(Component.next_view).toBe('service_create')
    })
  })

  it('show_transport_protocol is false by default', () => {
    Component.$nextTick(() => {
      expect(Component.show_transport_protocol).toBe(false)
    })
  })

  it('has the setPortNumberChanged function', () => {
    Component.$nextTick(() => {
      expect(typeof Component.setPortNumberChanged).toBe('function')
    })
  })
})
