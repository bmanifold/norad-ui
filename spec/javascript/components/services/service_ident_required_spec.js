import ServiceIdentRequired from 'components/services/service_ident_required.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceIdentRequired', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceIdentRequired)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('next_view is service_encryption_protocol by default', () => {
    expect(Component.next_view).toEqual('service_encryption_protocol')
  })
})
