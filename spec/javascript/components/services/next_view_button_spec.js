import NextViewButton from 'components/services/next_view_button.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('NextViewButton', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(NextViewButton)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('has the nextView method', () => {
    expect(typeof Component.nextView).toBe('function')
  })

  it('has the props defined', () => {
    expect(Object.keys(NextViewButton.props).sort()).toEqual(['disabled', 'hidden'])
  })
})
