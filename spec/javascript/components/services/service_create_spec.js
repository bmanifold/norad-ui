import ServiceCreate from 'components/services/service_create.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceCreate', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceCreate, {
      propsData: { application_type_options: [] }
    })
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('has the mounted hook', () => {
    expect(typeof ServiceCreate.mounted).toBe('function')
  })

  it('application_protocol is null by default', (done) => {
    Component.$nextTick(() => {
      expect(Component.application_protocol).toBe(null)
      done()
    })
  })

  it('has the service computed property', () => {
    Component.$nextTick(() => {
      expect(Component.service.name).toBe(null)
    })
  })

  it('has the service_identity computed property', () => {
    Component.$nextTick(() => {
      expect(Component.service_identity.username).toBe(null)
    })
  })

  it('has the web_application_config computed property', () => {
    Component.$nextTick(() => {
      expect(Component.web_application_config.url_blacklist).toBe(null)
    })
  })
})
