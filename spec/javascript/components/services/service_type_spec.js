import ServiceType from 'components/services/service_type.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceType', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceType)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('next_view is service_ident_required by default', () => {
    expect(Component.next_view).toBe('service_ident_required')
  })
})
