import ServiceEncryptionProtocol from 'components/services/service_encryption_protocol.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceEncryptionProtocol', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceEncryptionProtocol)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('next_view is service_details', () => {
    expect(Component.next_view).toEqual('service_details')
  })
})
