import AddServiceWizard from 'components/services/add_service_wizard.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('AddServiceWizard', () => {
  let Component
  const currentView = () => { return Component.$store.state.services.view.current_view }

  beforeEach(() => {
    Component = ComponentGenerator(AddServiceWizard)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('has the props defined', () => {
    expect(Object.keys(AddServiceWizard.props).sort()).toEqual(['application_type_options', 'machine_id'])
  })

  it('has the nextView method defined', () => {
    expect(typeof Component.nextView).toBe('function')
  })

  it('has the setValidator function defined', () => {
    expect(typeof Component.setValidator).toBe('function')
  })

  it('has the validate function defined', () => {
    expect(typeof Component.validate).toBe('function')
  })

  it('starts with the service_type view', () => {
    expect(currentView()).toBe('service_type')
  })

  it('progresses to next view', () => {
    Component.nextView()

    // View changes take two ticks
    Component.$nextTick().then(Component.$nextTick()).then(() => {
      expect(currentView()).toBe('service_ident_required')
    })
  })

  describe('view cycling', () => {
    const forward_views = [
      'service_type',
      'service_web_auth_method',
      'service_web_auth_fields',
      'service_web_details',
      'service_ident_required',
      'service_encryption',
      'service_details',
      'service_creds',
      'service_create',
      'service_common_name'
    ]

    it('cycles through all views', () => {
      let i

      for (i = 1; i < forward_views.length; i++) {
        Component.$store.commit('services/view/setNextView', forward_views[i])
        Component.$store.commit('services/view/nextView')
        expect(currentView()).toBe(forward_views[i])
      }

      for (i = forward_views.length - 1; i >= 0; i--) {
        expect(currentView()).toBe(forward_views[i])
        Component.$store.commit('services/view/prevView')
      }
    })
  })
})
