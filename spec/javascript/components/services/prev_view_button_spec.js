import PrevViewButton from 'components/services/prev_view_button.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('PrevViewButton', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(PrevViewButton)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('has the prevView method', () => {
    expect(typeof Component.prevView).toBe('function')
  })

  it('has the props defined', () => {
    expect(Object.keys(PrevViewButton.props)).toEqual(['hidden'])
  })
})
