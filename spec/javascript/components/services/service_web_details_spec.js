import ServiceWebDetails from 'components/services/service_web_details.vue'
import { ComponentGenerator } from 'helpers/component_generator'


describe('ServiceWebDetails', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceWebDetails)
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('next_view is service_web_auth_method', () => {
    expect(Component.next_view).toEqual('service_web_auth_method')
  })
})
