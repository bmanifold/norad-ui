import ServiceApplicationType from 'components/services/service_application_type.vue'
import { ComponentGenerator } from 'helpers/component_generator'

describe('ServiceApplicationType', () => {
  let Component

  beforeEach(() => {
    Component = ComponentGenerator(ServiceApplicationType, {
      propsData: { application_type_options: [] }
    })
    Component.$mount()
  })

  afterEach(() => {
    Component.$destroy()
  })

  it('has the setApplicationTypeChanged method', () => {
    expect(typeof Component.setApplicationTypeChanged).toBe('function')
  })

  it('has the application_type_options props', () => {
    expect(Object.keys(ServiceApplicationType.props)).toEqual(['application_type_options'])
  })

  it('next_view is service_create', () => {
    Component.$nextTick(() => {
      expect(Component.next_view).toBe('service_create')
    })
  })
})
