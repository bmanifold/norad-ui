# frozen_string_literal: true

module OrganizationTestHelper
  def add_member_to_org(org_slug, username, role)
    visit settings_organization_path(org_slug)
    fill_in 'membership[uid]', with: username
    select(role.capitalize, from: 'membership[admin]')
    click_button('Add Member')
  end
end
