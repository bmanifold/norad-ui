# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Assessment, type: :model do
  def assessment_collection(count_hash)
    assessments_array = count_hash.map do |key, value|
      Array.new(value) { build_stubbed(:assessment, "with_#{key}_result".to_sym) }
    end.flatten
    Her::Collection.new(assessments_array)
  end

  def random_count_hash
    rg = Random.new
    {
      failed: rg.rand(0..10),
      errored: rg.rand(0..10),
      passed: rg.rand(0..10),
      ignored: rg.rand(0..10),
      incomplete: rg.rand(0..10)
    }
  end

  context 'when calling statistics' do
    it 'reports correct statistics for one hash' do
      count_hash =
        { failed: 2, errored: 3, passed: 4, ignored: 5, incomplete: 6 }
      assessments = assessment_collection(count_hash)
      result_hash = count_hash.merge(total: assessments.count)
      expect(Assessment.statistics(assessments)).to eq result_hash
    end

    it 'reports correct statistics for randomly generated count hashes' do
      20.times do
        count_hash = random_count_hash
        assessments = assessment_collection(count_hash)
        result_hash = count_hash.merge(total: assessments.count)
        expect(Assessment.statistics(assessments)).to eq result_hash
      end
    end
  end
end
