# frozen_string_literal: true

require 'rails_helper'

describe AssessmentsHelper do
  describe '#reduce_by_result_status' do
    def build_assessments(construction_hash_array)
      construction_hash_array.map do |construction_hash|
        build_assessment(construction_hash)
      end
    end

    def build_assessment(construction_hash)
      results = build_results(construction_hash[:failed],
                              construction_hash[:errored],
                              construction_hash[:passed],
                              construction_hash[:ignored])
      build_stubbed(:assessment,
                    results: results,
                    service_name: construction_hash[:service_name])
    end

    def build_results(failed, errored, passed, ignored)
      [build_stubbed_list(:result, failed, :failed),
       build_stubbed_list(:result, errored, :errored),
       build_stubbed_list(:result, passed, :passed),
       build_stubbed_list(:result, ignored, :ignored, :passed)].flatten
    end

    before(:each) do
      construction_hash = [
        { service_name: 'web service', failed: 1, errored: 2, passed: 3, ignored: 4 },
        { service_name: 'encrypted web service', failed: 2, errored: 3, passed: 4, ignored: 5 }
      ]
      assessments = build_assessments(construction_hash)
      @status_service_assessment_results_hash = helper.reduce_by_result_status(assessments)
    end

    it 'properly counts failed results' do
      expect(@status_service_assessment_results_hash[:fail][:count]).to eq 3
    end

    it 'properly counts errored results' do
      expect(@status_service_assessment_results_hash[:error][:count]).to eq 5
    end

    it 'properly counts passed results' do
      # Note: The ignored results are passed and ignored. Thus, we have a total
      # of 14 (= 3 + 4 + 4 + 5) passed results.
      expect(@status_service_assessment_results_hash[:pass][:count]).to eq 16
    end

    it 'properly counts passed and ignored results' do
      expect(@status_service_assessment_results_hash[:pass][:ignored]).to eq 9
    end

    it 'properly groups results by service' do
      failed_results = @status_service_assessment_results_hash[:fail][:data]['web service'].values.flatten
      errored_results = @status_service_assessment_results_hash[:error][:data]['web service'].values.flatten
      passed_results = @status_service_assessment_results_hash[:pass][:data]['web service'].values.flatten
      expect(failed_results.count).to eq 1
      expect(errored_results.count).to eq 2
      # Note: The ignored results are passed and ignored. Thus, we have a total
      # of 7 (= 3 + 4) passed results.
      expect(passed_results.count).to eq 7
    end
  end
end
