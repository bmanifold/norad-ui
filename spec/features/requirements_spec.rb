# frozen_string_literal: true

require 'rails_helper'

feature 'Requirements functionality' do
  before :each do
    valid_login
    @req_group = create_requirement_group_helper
    td = find('td', text: @req_group)
    td.find('a').click
  end

  scenario 'add a requirement', js: true do
    name = create_requirement_helper

    td = find('td', text: name)
    expect(td).to be_present
  end

  scenario 'modify a requirement', js: true do
    name = create_requirement_helper
    new_name = "#{name} UPDATED"
    new_description = 'Updated Description'

    td = find('td', text: name)
    td.find(:xpath, '..').find('a[title="Edit Requirement"]').click
    form = find('form.requirement-edit-form')
    form.fill_in 'requirement[name]', with: new_name
    form.fill_in 'requirement[description]', with: new_description
    click_on 'Save'

    td_name = find('td', text: new_name)
    expect(td_name).to be_present
    td_description = td_name.find(:xpath, '..').find('td', text: new_description)
    expect(td_description).to be_present
  end

  scenario 'delete a requirement', js: true do
    name = create_requirement_helper

    td = find('td', text: name)
    expect(td).to be_present
    td.find(:xpath, '..').find('a[data-method="delete"]').click
    expect(page).not_to have_selector('td', text: name)
  end
end
