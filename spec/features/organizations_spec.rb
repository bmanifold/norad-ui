# frozen_string_literal: true

require 'rails_helper'

feature 'Organization functionality', js: true do
  include OrganizationTestHelper

  before :each do
    valid_login
    @org_slug = create_org_helper
    visit organization_path(@org_slug)
  end

  scenario 'list orgs' do
    visit organizations_path

    expect(page).to have_selector("a[href=\"#{organization_path(@org_slug)}\"]")
  end

  scenario 'add an organization' do
    visit organizations_path
    click_on 'New Organization'
    fill_in id: 'org-name-input', with: 'new-test-org'
    click_on 'Create'
    wait_for_ajax

    expect(page).to have_selector('a[href="/organizations/new-test-org"]')
  end

  scenario 'add an existing organization' do
    visit organizations_path
    org_name = @org_slug.tr('-', ' ')
    click_on 'New Organization'
    fill_in id: 'org-name-input', with: org_name
    click_on 'Create'
    wait_for_ajax

    alert_msg = accept_alert
    expect(alert_msg).to eql('Name already exists in the database.')
  end

  scenario 'display organization token for given org' do
    token = find('.callout > span').text

    expect(page).to have_content('Organization Registration Token')
    expect(token).to match(/[a-f0-9]{64}/)
  end

  scenario 'recreate organization token' do
    old_token = find('.callout > span').text
    expect(old_token).to match(/[a-f0-9]{64}/)

    find('#recreate-org-token').click
    new_token = find('.callout > span').text
    expect(new_token).to match(/[a-f0-9]{64}/)
    expect(new_token).not_to eq(old_token)
  end

  scenario 'display machine count for given org' do
    create_machine_helper(@org_slug)
    visit organization_path(@org_slug)
    machine_count = find('#machine-count').text.to_i
    expect(machine_count).to be > 0
  end

  skip 'display machine stats for given org' do
    skip 'cannot be tested due to inability to create mock assessment results from API'
  end

  scenario 'do not display organization without permission' do
    valid_login('anotheruser')
    valid_login('testuser')
    visit organization_path('anotheruser-org-default')

    expect(page).to have_content('You are not authorized to view this organization')
  end

  scenario 'display info when trying to access an organization that does not exist' do
    visit organization_path('blah')
    expect { visit organization_path('blah') }.to raise_error(ActionController::RoutingError)
  end

  skip 'start organization wide scan' do
    skip 'Waiting on best way to spoof actual scan from starting'
  end

  skip 'display latest assessments for an organization' do
    skip 'Waiting on seeds file'
  end

  feature 'scan history table columns' do
    let(:summary) do
      {
        1 => {
          state_details: 'Started successfully',
          created_at: 1.hour.ago,
          state: 'in_progress',
          machines_scanned: 682,
          results_summary: {
            failing: 671,
            passing: 11,
            erroring: 0,
            informing: 0,
            warning: 0
          }
        }
      }
    end

    before(:each) do
      allow_any_instance_of(Organization).to receive(:assessments_summary).and_return(summary)
    end

    scenario 'for a scan in progress' do
      visit history_organization_path(@org_slug)

      expect(page).to have_content('Started successfully')
      expect(page).to have_content('682') # machines_scanned
      expect(page).to have_content('11')  # passing
      expect(page).to have_content('671') # failing
      expect(page).to have_content('0')   # erroring
    end
  end

  skip 'display scan history for an organization' do
    skip 'Waiting on seeds file'
  end

  skip 'remove scan from scan history page' do
    skip 'Waiting on best way to mock a scan'
  end

  scenario 'display organization errors table on the dashboard' do
    expect(page).to_not have_content('Organization Errors')
    # Create a machine with an RFC 1918 IP Address
    create_machine_helper(@org_slug, ip_address: '192.168.1.100')
    visit organization_path(@org_slug)
    expect(page).to have_content('There are machines in your Organization in a private network, '\
                                 'but no verified Relay is available to scan them.')
  end

  scenario 'display organization error warning on organization-centric pages' do
    expect(page).to_not have_content('This organization may not be performing scans correctly')
    # Create a machine with an RFC 1918 IP Address
    create_machine_helper(@org_slug, ip_address: '192.168.1.100')
    # Stay on the machines index after creating the machine
    expect(page).to have_content('This organization may not be performing scans correctly')
  end
end

feature 'Organization delete functionality', js: true do
  include OrganizationTestHelper

  before :each do
    valid_login
    @org_slug = create_org_helper
    visit settings_organization_path(@org_slug)
  end

  scenario 'delete an organization from index' do
    visit organizations_path

    accept_confirm do
      find("a[href=\"#{organization_path(@org_slug)}\"] > i.fa-remove").click
    end

    expect(page).not_to have_selector("a[href=\"#{organization_path(@org_slug)}\"] > i.fa-remove")
  end

  scenario 'delete an organization from settings' do
    accept_confirm do
      find('.box-footer > a.btn.btn-danger').click
    end

    expect(current_path).to eq(organizations_path)
    expect(page).not_to have_selector("a[href=\"#{organization_path(@org_slug)}\"] > i.fa-remove")
  end
end

feature 'Organization settings', js: true do
  include OrganizationTestHelper

  before :each do
    valid_login
    @org_slug = create_org_helper
    visit settings_organization_path(@org_slug)
  end

  def make_default_org(user_name, org_slug)
    valid_login(user_name)
    visit organizations_path

    # First find the org name link
    org_link = find("a.org-name[href=\"#{organization_path(org_slug)}\"]")
    # then traverse up to find row and back down to find default button icon
    org_link.find(:xpath, '../../td[1]/i').click
    wait_for_vue
  end

  scenario 'add a valid user as admin to organization' do
    new_user = 'anotheruser'
    valid_login(new_user)
    valid_login
    add_member_to_org(@org_slug, new_user, 'Admin')

    expect(page).to have_content('Membership created!')
    role_cell = find('#membership-table td', text: new_user).find(:xpath, '..').find('td', text: 'Admin')
    expect(role_cell).to have_text('Admin')
  end

  scenario 'remove an admin user from organization' do
    new_user = 'anotheruser'
    valid_login(new_user)
    valid_login
    add_member_to_org(@org_slug, new_user, 'Admin')
    expect(page).to have_content('Membership created!')

    accept_confirm do
      find('td', text: new_user).find(:xpath, '..').find('a.delete-membership').click
    end

    expect(page).to have_content('Membership successfully deleted!')
  end

  scenario 'add a valid user as reader to organization and prevent duplicates' do
    new_user = 'anotheruser'
    valid_login(new_user)
    valid_login

    add_member_to_org(@org_slug, new_user, 'Reader')
    expect(page).to have_content('Membership created!')

    role_cell = find('#membership-table td', text: new_user).find(:xpath, '..').find('td', text: 'Reader')
    expect(role_cell).to have_text('Reader')

    add_member_to_org(@org_slug, new_user, 'Reader')
    expect(page).to have_content('User has already been taken')

    accept_confirm do
      find('td', text: new_user).find(:xpath, '..').find('td:last-child a').click
    end
    expect(page).not_to have_selector('#membership-table td', text: new_user)
  end

  scenario 'attempt to add unknown user as admin to organization' do
    fill_in 'membership[uid]', with: 'unknownuser'
    select('Admin', from: 'membership[admin]')
    click_button('Add Member')

    expect(page).to have_content('Could not create membership!')
  end

  scenario "change an organization's name" do
    visit settings_organization_path(@org_slug)
    new_organization_name = "New Name #{rand(10_000)}"
    fill_in 'organization[uid]', with: new_organization_name
    find('#edit-organization .btn.btn-success').click
    expect(page).to have_content("Organization Settings ( for #{new_organization_name} )")
  end

  scenario 'a pre-existing name cannot be chosen' do
    create_org_helper('reserved-name')
    visit settings_organization_path(@org_slug)
    fill_in 'organization[uid]', with: 'reserved-name'
    find('#edit-organization .btn.btn-success').click
    expect(page).to have_content('Name already exists in the database.')
  end

  scenario 'an invalid name cannot be chosen' do
    visit settings_organization_path(@org_slug)
    new_invalid_organization_name = "New Invalid Name * #{rand(10_000)}"
    fill_in 'organization[uid]', with: new_invalid_organization_name
    find('#edit-organization .btn.btn-success').click
    error_string = 'Uid must be alphanumeric (dashes and spaces are ok).'
    expect(page).to have_content(error_string)
  end

  scenario 'ensure that all default organization names are updated' do
    new_user_name = "anotheruser#{rand(10_000)}"
    valid_login(new_user_name)
    valid_login
    add_member_to_org(@org_slug, new_user_name, 'Reader')
    make_default_org(new_user_name, @org_slug)
    valid_login
    visit settings_organization_path(@org_slug)
    new_organization_name = "New Name #{rand(10_000)}"
    fill_in 'organization[uid]', with: new_organization_name
    find('#edit-organization .btn.btn-success').click
    new_user_profile = UserProfile.where(user_uid: new_user_name).first
    new_organization_slug = new_organization_name.downcase.tr(' ', '-')
    expect(new_user_profile.default_org).to eq(new_organization_slug)
  end
end
