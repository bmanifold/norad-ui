# frozen_string_literal: true

require 'rails_helper'

feature 'Organization Scan Schedule functionality' do
  before :each do
    valid_login
    @org_slug = create_org_helper
    visit organization_scan_schedules_path(@org_slug)
  end

  let(:create_schedule) do
    click_on 'add-new-schedule'
    select 'PM', from: 'organization_scan_schedule_am_pm'
    click_on 'schedule-form-save-button'
  end

  scenario 'add a schedule', js: true do
    expect(page).to have_content('You have not set up a recurring scan for the machines in this organization.')
    create_schedule
    expect(page).to have_content('13:00 UTC')
  end

  scenario 'view a schedule', js: true do
    create_schedule
    expect(page).to have_content('13:00 UTC')
  end

  scenario 'update a schedule', js: true do
    create_schedule
    expect(page).to have_content('13:00 UTC')
    click_on 'edit-schedule'
    select 'AM', from: 'organization_scan_schedule_am_pm'
    click_on 'schedule-form-save-button'
    expect(page).to have_content('1:00 UTC')
  end

  scenario 'remove a schedule', js: true do
    create_schedule
    expect(page).to have_content('13:00 UTC')
    accept_confirm do
      click_on 'remove-schedule'
    end
    expect(page).to have_content('You have not set up a recurring scan for the machines in this organization.')
  end
end
