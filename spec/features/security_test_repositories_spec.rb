# frozen_string_literal: true

require 'rails_helper'

feature 'Security Test Repositories', js: true do
  let(:name) { 'new-test-repository' }
  let(:host) { 'foobar:4000' }
  let(:username) { 'foobar' }
  let(:password) { 'password' }
  let(:other_uid) { 'other_user' }
  let(:valid_yaml_path) { Rails.root + 'spec/support/uploads/valid_repository_security_container.yml' }
  let(:invalid_yaml_path) { Rails.root + 'spec/support/uploads/invalid_repository_security_container.yml' }
  let(:valid_yaml_array_path) { Rails.root + 'spec/support/uploads/valid_repository_security_containers.yml' }
  let(:invalid_yaml_array_path) { Rails.root + 'spec/support/uploads/invalid_repository_security_containers.yml' }

  # Capybara has issues attaching files to hidden file fields
  def make_file_input_visible
    execute_script <<~SCRIPT
      $('input[type=file]').removeAttr('hidden');
      $('.btn-file').removeClass('btn-file');
    SCRIPT
  end

  def set_file_detector
    page.driver.browser.file_detector = lambda do |args|
      args.first.to_s
    end
  end

  def remove_file_detector
    page.driver.browser.file_detector = nil
  end

  def create_container(yaml_path)
    set_file_detector
    click_on 'New Security Test'
    expect(page).to have_content('Example YAML configuration')
    make_file_input_visible
    page.attach_file('yaml-upload', yaml_path)
    click_on 'Create'
    # Need to remove the file_detector because it will always try to
    # use it if not.  There may be a better way to do this, but for now
    # this works
    remove_file_detector
  end

  def create_repository(new_name: name, new_host: host, new_scope: 'Private')
    visit security_test_repositories_path
    click_on 'New Test Repository'
    fill_in 'security_test_repository[name]', with: new_name
    choose new_scope
    fill_in 'security_test_repository[host]', with: new_host
    fill_in 'security_test_repository[username]', with: username
    fill_in 'security_test_repository[password]', with: password
    click_on 'submit-repository'
  end

  before :each do
    valid_login
    create_repository
  end

  after :each do
    remove_file_detector
  end

  scenario 'repository is in list' do
    expect(page).to have_content(name)
    expect(page).to have_content('Private')
  end

  context 'repository admins' do
    before :each do
      wait_for_ajax
      valid_login other_uid
      valid_login
      visit security_test_repositories_path
      click_on name
    end

    scenario 'add and remove' do
      fill_in 'repository_member[uid]', with: other_uid
      click_on 'Add Repository Member'
      expect(page).to have_content other_uid

      accept_confirm do
        find('.remove-member:not(.disabled)').click
      end

      expect(page).not_to have_content other_uid
    end
  end

  context 'viewing private repositories' do
    before :each do
      visit security_test_repositories_path
      click_on name
      create_container valid_yaml_path
      valid_login other_uid
    end

    it 'should show private repository with details redacted' do
      visit security_test_repositories_path
      click_on name
      expect(page).to have_content name
      expect(page).to have_content 'Private'
      expect(page).to have_content 'container_name'
      expect(page).to have_content '0.0.1'
      expect(page).not_to have_content 'blackbox'
      expect(page).not_to have_content 'prog_args'
      expect(page).not_to have_content 'default_config'
      expect(page).not_to have_content 'test_types'
      expect(page).not_to have_content 'help_url'
      expect(page).not_to have_content 'multihost'
    end
  end

  scenario 'update a repository' do
    click_on name
    name_field = find_field('security_test_repository[name]')
    host_field = find_field('security_test_repository[host]')
    username_field = find_field('security_test_repository[username]')

    expect(name_field.value).to eq name
    expect(host_field.value).to eq host
    expect(username_field.value).to eq username

    fill_in 'security_test_repository[name]', with: "#{name} (updated)"
    fill_in 'security_test_repository[host]', with: "#{host} (updated)"
    fill_in 'security_test_repository[username]', with: "#{username} (updated)"
    click_on 'submit-repository'

    expect(name_field.value).to eq("#{name} (updated)")
    expect(host_field.value).to eq("#{host} (updated)")
    expect(username_field.value).to eq("#{username} (updated)")
  end

  context 'viewing public repositories' do
    let(:public_name) { 'public-repo' }
    let(:public_host) { 'public-host:5000' }

    before :each do
      valid_login
      create_repository(new_scope: 'Public', new_name: public_name, new_host: public_host)
    end

    it 'allows whitelisting other public repositories' do
      valid_login other_uid
      visit security_test_repositories_path
      click_on public_name

      check_box_field = find_field('repository_whitelist_entry[organization_id]')
      check_box_field.click
      expect(page).to have_content('Update Whitelist: Success')
      reload_page
      expect(check_box_field).to be_checked

      check_box_field.click
      expect(page).to have_content('Update Whitelist: Success')
      reload_page
      expect(check_box_field).not_to be_checked
    end
  end

  context 'viewing private repositories as reader', js: true do
    let(:private_name) { 'private-repo' }
    let(:private_host) { 'private-host:5000' }

    before :each do
      valid_login other_uid
      valid_login
      create_repository(new_scope: 'Private', new_name: private_name, new_host: private_host)
      visit security_test_repositories_path
      click_on private_name
      fill_in 'repository_member[uid]', with: other_uid
      click_on 'Add Repository Member'
      expect(page).to have_content other_uid
    end

    it 'allows whitelisting private repositories as reader' do
      valid_login other_uid
      visit security_test_repositories_path
      click_on private_name

      check_box_field = find_field('repository_whitelist_entry[organization_id]')
      check_box_field.click
      expect(page).to have_content('Update Whitelist: Success')
      reload_page
      expect(check_box_field).to be_checked

      check_box_field.click
      expect(page).to have_content('Update Whitelist: Success')
      reload_page
      expect(check_box_field).not_to be_checked
    end
  end

  scenario 'updates the whitelist' do
    click_on name
    check_box_field = find_field('repository_whitelist_entry[organization_id]')
    check_box_field.click
    expect(page).to have_content('Update Whitelist: Success')
    reload_page
    expect(check_box_field).to be_checked

    check_box_field.click
    expect(page).to have_content('Update Whitelist: Success')
    reload_page
    expect(check_box_field).not_to be_checked
  end

  context 'security containers' do
    before :each do
      click_on name
    end

    scenario 'add and remove a container' do
      create_container valid_yaml_path
      expect(page).to have_content('container_name')

      accept_confirm { find('a.remove').click }
      expect(page).not_to have_content('container_name')
    end

    scenario 'add an invalid container' do
      create_container invalid_yaml_path
      wait_for_ajax
      expect(page).to have_content('Name can only contain @, alphanumeric, colons, periods, underscores and dashes.')
      expect(page).not_to have_content('invalid')
    end

    scenario 'add multiple containers' do
      create_container valid_yaml_array_path
      expect(page).to have_content('container_name_1')
      expect(page).to have_content('container_name_2')
    end

    scenario 'add multiple invalid containers' do
      create_container invalid_yaml_array_path
      wait_for_ajax
      expect(page).to have_content('Name can only contain @, alphanumeric, colons, periods, underscores and dashes.')
      expect(page).not_to have_content('invalid')
      expect(page).not_to have_content('container_name')
    end
  end
end
