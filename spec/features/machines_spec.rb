# frozen_string_literal: true

require 'rails_helper'

feature 'Machines functionality', js: true do
  before :each do
    valid_login
    @org_slug = create_org_helper
    @machine = create_machine_helper(@org_slug)
    visit organization_path(@org_slug)
  end

  scenario 'add machine to an organization' do
    visit organization_machines_path(@org_slug)
    click_on 'New Machine'
    fill_in id: 'machine_ip', with: '1.1.1.1'
    fill_in id: 'machine_fqdn', with: 'test.invalid'
    fill_in id: 'machine_description', with: 'Test machine created by capybara/rspec'
    click_on 'Create'

    expect(page).to have_content('1.1.1.1')
    expect(page).to have_content('Machine successfully created!')
    expect(current_url).to match(%r{machines/\d+/services})
  end

  scenario 'add machine to an organization with FQDN as the Target' do
    visit organization_machines_path(@org_slug)
    click_on 'New Machine'
    fill_in id: 'machine_ip', with: '1.1.1.1'
    fill_in id: 'machine_fqdn', with: 'fqdn.cisco.com'
    fill_in id: 'machine_description', with: 'Test machine created by capybara/rspec'
    check 'machine_use_fqdn_as_target'
    click_on 'Create'

    expect(page).to have_content('fqdn.cisco.com')
    expect(current_url).to match(%r{machines/\d+/services})
  end

  scenario 'create machine with invalid FQDN' do
    visit organization_machines_path(@org_slug)
    click_on 'New Machine'
    fill_in id: 'machine_ip', with: '1.1.1.1'
    fill_in id: 'machine_fqdn', with: '1.1.1.1'
    find('#machine_name').native.send_keys('a', :down, :up)
    wait_for_vue

    expect(page).to have_content('Must be a valid FQDN')
  end

  scenario 'update machine with invalid FQDN' do
    visit organization_machines_path(@org_slug)
    find('.fa-edit').click
    fill_in id: 'machine_fqdn', with: '1.1.1.1'
    find('#machine_name').native.send_keys('a', :down, :up)
    wait_for_vue

    expect(page).to have_content('Must be a valid FQDN')
  end

  scenario 'get error message when name exceeds 36 characters' do
    visit organization_machines_path(@org_slug)
    click_on 'New Machine'
    fill_in id: 'machine_name', with: 'A' * 36
    find('#machine_name').native.send_keys('a', :down, :up)
    wait_for_vue

    expect(page).to have_content('Machine name must not exceed 36 characters')
  end

  scenario 'list machines in an organization' do
    visit organization_machines_path(@org_slug)
    expect(page).to have_selector('a', text: @machine.name)
  end

  scenario 'display alert when trying to access machine that does not exist' do
    visit machine_path('1000')
    expect { visit machine_path('1000') }.to raise_error(ActionController::RoutingError)
  end

  scenario 'edit machine' do
    visit organization_machines_path(@org_slug)
    find('.fa-edit').click
    fill_in id: 'machine_name', with: 'capybara-edit'
    click_on 'Save'
    wait_for_vue

    expect(page).to have_content 'capybara-edit'
  end

  # The purpose of this test is to ensure that the null case functions properly
  # even as new functionality is added. This test prevents code with improper
  # null checks from slipping through the cracks.
  scenario 'machines without scans should display results page.' do
    visit results_machine_path(@machine.id)
    expect(page).to have_content('No scans have been run')
  end

  scenario 'machines without scans should display history page.' do
    visit history_machine_path(@machine.id)
    expect(page).to have_content('No scans have been run')
  end

  skip 'display latest assessments for a machine' do
    skip 'Waiting on seeds file'
  end

  feature 'Ensure the scan history table has the State details column' do
    let(:summary) do
      {
        5 => {
          state_details: 'Started successfully',
          created_at: 1.hour.ago,
          state: 'in_progress',
          machines_scanned: 1,
          results_summary: []
        }
      }
    end

    before(:each) do
      allow_any_instance_of(Machine).to receive(:assessments_summary).and_return(summary)
    end

    scenario 'for a scan in progress' do
      visit history_machine_path(@machine.id)
      expect(page).to have_content('Started successfully')
    end
  end

  skip 'display scan history for a machine' do
    skip 'Waiting on seeds file'
  end

  skip 'start machine scan' do
    skip 'Waiting on best way to spoof actual scan from starting'
  end

  skip 'remove scan from scan history page' do
    skip 'Waiting on best way to mock a scan'
  end

  skip 'remove scan from scan results page' do
    skip 'Waiting on best way to mock a scan'
  end

  skip 'results for a machine displays timestamps' do
    # Need to check for timestamps for scan starts and assessment completion
    # Timestamps on assessments should only be present on completion.
    # These two types of timestamps need to be present on both the
    # security tests tab and requirements tab.
    skip 'Waiting for a way to have assessment data sent ' \
         'from api in test environmnet'
  end

  skip 'history for a machine needs timestamps for each scan' do
    # Need a timestamp indicating the start of each scan.
    skip 'Waiting for a way to have assessment data sent ' \
         'from api in test environment'
  end
end
