# frozen_string_literal: true

require 'rails_helper'

feature 'Connectivity Checks', js: true do
  include OrganizationTestHelper

  before :each do
    valid_login
    @org_slug = create_org_helper
    @machine = create_machine_helper(@org_slug)
    visit settings_machine_path(@machine)
  end

  scenario 'it displays connectivity checks box' do
    expect(page).to have_content('Connectivity Checks')
  end
end
