# frozen_string_literal: true

require 'rails_helper'

feature 'Machine Security Tests functionality' do
  let(:configurable_testname) { 'configurable-test' }

  before :each do
    valid_login
    @org_slug = create_org_helper
    @machine = create_machine_helper(@org_slug)
    visit machine_security_tests_path(@machine)
  end

  def add_security_test(machine_id, test_name = 'vuls')
    test_regexp = /#{test_name}\z/
    visit machine_security_tests_path(machine_id)
    find('span.select2').click
    find('.select2-results').find('li', text: test_regexp).click
    click_on 'Add Security Tests'
    wait_for_ajax
  end

  scenario 'enable a configurable security test for a machine', js: true do
    add_security_test(@machine.id, configurable_testname)
    expect(page).to have_selector('td', text: configurable_testname)
  end

  scenario 'enable a non-configurable security test for a machine', js: true do
    test_name = 'vuls'
    add_security_test(@machine.id, test_name)
    expect(page).to have_selector('td', text: test_name)
  end

  scenario 'ensure added security tests have documentation links', js: true do
    test_name = 'vuls'
    add_security_test(@machine.id, test_name)
    expect(page).to have_selector(:css, 'a[title="Vuls Docs"]')
  end

  scenario 'ensure added security tests have Service Requirements', js: true do
    add_security_test(@machine.id, 'zap-passive')
    expect(page).to have_text('Web application service')
  end

  scenario 'configure a security test for a machine', js: true do
    add_security_test(@machine.id, configurable_testname)
    expect(page).to have_selector('td', text: configurable_testname)
    find('td', text: configurable_testname).find(:xpath, '..').find('a.edit-config').click
    fill_in 'values[configurable_option]', with: 'capybara-config-value'
    click_on 'Update Config'

    expect(page).to have_content('Saved Successfully')
  end

  scenario 'delete a configurable security test config for a machine', js: true do
    add_security_test(@machine.id, configurable_testname)
    expect(page).to have_selector('td', text: configurable_testname)
    find('td', text: configurable_testname).find(:xpath, '..').find('a.delete-config').click

    expect(page).not_to have_selector('a.delete-config')
  end

  scenario 'delete a non-configurable security test config for a machine', js: true do
    test_name = 'vuls'
    add_security_test(@machine.id, test_name)
    expect(page).to have_selector('td', text: test_name)
    first('a.delete-config').click

    expect(page).not_to have_selector('a.delete-config')
  end

  skip 'ensure scan result ignore rules can be deleted', js: true do
    skip 'Skipping until test environment provides test results'
  end

  skip 'ensure scan result ignore rules can be edited', js: true do
    skip 'Skipping until test environment provides test results'
  end

  scenario 'Scan button disabled when machine has no assessments configured', js: true do
    visit results_machine_path(@machine)
    expect(find('#machine-info')).to have_css('a.scan-machine.disabled')
  end

  scenario 'Scan button enabled when machine has assessments configured', js: true do
    add_security_test(@machine.id, 'zap-passive')
    wait_for_ajax
    visit results_machine_path(@machine)
    expect(find('#machine-info')).not_to have_css('a.scan-machine.disabled')
  end

  scenario 'Scan button dynamically disabled by JavaScript when only machine assessment is removed', js: true do
    add_security_test(@machine.id, 'zap-passive')
    wait_for_ajax
    visit machine_security_tests_path(@machine.id)
    find('.delete-config .fa.fa-close.action-icon').click
    expect(find('#machine-info')).to have_css('a.scan-machine.disabled')
  end

  scenario 'Scan button dynamically enabled by JavaScript when first machine assessment is added', js: true do
    add_security_test(@machine.id, 'zap-passive')
    expect(find('#machine-info')).not_to have_css('a.scan-machine.disabled')
  end
end
