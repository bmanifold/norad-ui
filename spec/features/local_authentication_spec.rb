# frozen_string_literal: true

require 'rails_helper'

feature 'Local authentication', js: true do
  before(:each) { mock_auth_type :local }
  after(:each) { mock_auth_type :reverse_proxy }

  scenario 'sign up with existing email fails' do
    same_email = timestamped_email
    local_signup(email: same_email)
    local_signup(email: same_email)
    expect(page).to have_content('There was an error signing up')
  end

  scenario 'sign up with invalid email fails' do
    local_signup(email: 'invalid')
    expect(page).to have_content('Email is invalid')
  end

  scenario 'sign up with missing password fails' do
    local_signup(password: '', password_confirmation: '')
    expect(page).to have_content('Password can\'t be blank')
  end

  scenario 'sign up with invalid password fails' do
    local_signup(password: 'badpass', password_confirmation: 'badpass')
    expect(page).to have_content('Password is too short')
  end

  scenario 'sign up with mismatched password fails' do
    local_signup(password: 'goodpass', password_confirmation: 'goodpass1')
    expect(page).to have_content("Password Confirmation doesn't match Password")
  end

  context 'with valid user', js: true do
    let(:memoized_email) { timestamped_email }

    before :each do
      local_signup(email: memoized_email)
    end

    scenario "can't sign in until confirmed" do
      expect(page).to have_content('Sign up successful')
      visit login_path
      local_signin(email: memoized_email)

      expect(page).to have_content('Invalid login attempt')
    end

    context 'password reset' do
      before :each do
        confirm_email ApiTestHelper.last_email_confirmation_token
        local_signin email: memoized_email
        visit users_path
        click_on 'Click here to reset your password'
      end

      scenario 'with invalid email shows success' do
        fill_in 'password_reset[email]', with: 'invaliduser@cisco.com'
        click_on 'Send'

        # To prevent email enumeration, password reset should return success even for invalid emails
        expect(page).to have_content('If that email exists in the system it will receive a password reset link.')
      end

      scenario 'with valid email shows success' do
        fill_in 'password_reset[email]', with: 'testuser@cisco.com'
        click_on 'Send'
        expect(page).to have_content('If that email exists in the system it will receive a password reset link.')
      end

      context 'with existing user' do
        scenario 'able to change password with valid token' do
          fill_in 'password_reset[email]', with: memoized_email
          click_on 'Send'
          expect(page).to have_content('If that email exists in the system it will receive a password reset link.')

          visit edit_password_reset_path(ApiTestHelper.last_password_reset_token)
          fill_in 'password_reset[password]', with: 'new_password'
          fill_in 'password_reset[password_confirmation]', with: 'new_password'
          click_on 'Reset Password'
          expect(page).to have_content('Password updated successfully. You may now sign in.')

          local_signin email: memoized_email, password: 'new_password'
          expect(page).to have_content('Organization Registration Token')
        end
      end
    end
  end

  scenario 'redirect to login when not signed in' do
    logout
    visit organizations_path
    expect(page).to have_content('Sign in to start your session')
  end
end
