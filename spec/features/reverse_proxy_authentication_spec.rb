# frozen_string_literal: true

require 'rails_helper'

feature 'Reverse proxy authentication', js: true do
  skip 'with invalid credentials' do
    skip 'With SSO changes, logins are no longer a thing.'
    logout
    visit login_path
    fill_in 'session[uid]', with: 'testuser'
    fill_in 'session[password]', with: 'badpass'
    click_on 'sign-in'

    expect(page).to have_content('Invalid login attempt')
  end

  scenario 'with valid credentails' do
    logout
    visit login_path
    fill_in 'session[uid]', with: 'testuser'
    fill_in 'session[password]', with: 'password'
    click_on 'sign-in'

    expect(page).to have_content('Organization Registration Token')
  end

  scenario 'redirect to login when not signed in' do
    logout
    visit organizations_path

    expect(page).to have_content('Sign in to start your session')
  end
end
