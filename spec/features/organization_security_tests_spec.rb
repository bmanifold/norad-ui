# frozen_string_literal: true

require 'rails_helper'

feature 'Organization Security Tests functionality' do
  let(:configurable_testname) { 'configurable-test' }

  before :each do
    valid_login
    @org_slug = create_org_helper
    visit organization_security_tests_path(@org_slug)
  end

  def add_security_test(org_slug, test_name = 'vuls')
    test_regexp = /#{test_name}\z/
    visit organization_security_tests_path(org_slug)
    find('#security-test-list ~ span.select2').click
    find('.select2-results').find('li', text: test_regexp).click
    click_on 'Add Security Tests'
    wait_for_ajax
  end

  scenario 'enable a security test for an organization', js: true do
    add_security_test(@org_slug)

    td = find('td', text: 'vuls')
    expect(td).to be_present
  end

  scenario 'configure a security test for an organization', js: true do
    add_security_test(@org_slug, configurable_testname)
    find('td', text: configurable_testname).find(:xpath, '..').find('a.edit-config').click
    fill_in 'values[configurable_option]', with: 'capybara-config-value'
    click_on 'Update Config'

    expect(page).to have_content('Saved Successfully')
  end

  scenario 'ensure security tests have documentation link', js: true do
    add_security_test(@org_slug)
    expect(page).to have_selector(:css, 'a[title="Vuls Docs"]')
  end

  scenario 'ensure security tests have Service Requirements listed', js: true do
    add_security_test(@org_slug, 'zap-passive')
    expect(page).to have_text('Web application service')
  end

  scenario 'ensure adding organization security test creates an ungrouped machine security test', js: true do
    add_security_test(@org_slug, 'zap-passive')
    @machine = create_machine_helper(@org_slug)
    visit machine_security_tests_path(@machine)
    expect(page.body).to include 'Organization Wide Security Tests'
    find('.req-group-name', text: 'Organization Tests').find('.req-list-toggle').click
    expect(page).to have_selector('td', text: 'zap-passive')
  end

  scenario 'delete security test config for an organization', js: true do
    add_security_test(@org_slug)
    find('td', text: 'vuls').find(:xpath, '..').find('a.delete-config').click

    expect(page).not_to have_selector('td', text: 'vuls')
  end

  skip 'ensure scan result ignore rules can be deleted', js: true do
    skip 'Skipping until test environment provides test results'
  end

  skip 'ensure scan result ignore rules can be edited', js: true do
    skip 'Skipping until test environment provides test results'
  end

  scenario 'Scan button enabled when machine\'s organization has assessments configured', js: true do
    @machine = create_machine_helper(@org_slug)
    visit results_machine_path(@machine)
    expect(find('#machine-info')).to have_css('a.scan-machine.disabled')
    add_security_test(@org_slug, 'zap-passive')
    visit results_machine_path(@machine)
    expect(find('#machine-info')).not_to have_css('a.scan-machine.disabled')
  end
end
