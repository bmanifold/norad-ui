# frozen_string_literal: true

require 'rails_helper'

feature 'Machine Scan Schedule functionality' do
  before :each do
    valid_login
    @org_slug = create_org_helper
    @machine = create_machine_helper(@org_slug)
    visit settings_machine_path(@machine.id)
  end

  let(:create_schedule) do
    click_on 'add-new-schedule'
    select 'PM', from: 'machine_scan_schedule_am_pm'
    click_on 'schedule-form-save-button'
  end

  it 'add a schedule', js: true do
    expect(page).to have_content('You have not set up a recurring scan for this machine.')
    create_schedule
    expect(page).to have_content('13:00 UTC')
  end

  it 'view a schedule', js: true do
    create_schedule
    expect(page).to have_content('13:00 UTC')
  end

  it 'update a schedule', js: true do
    create_schedule
    expect(page).to have_content('13:00 UTC')
    click_on 'edit-schedule'
    select 'AM', from: 'machine_scan_schedule_am_pm'
    click_on 'schedule-form-save-button'
    expect(page).to have_content('1:00 UTC')
  end

  it 'remove a schedule', js: true do
    create_schedule
    expect(page).to have_content('13:00 UTC')
    accept_confirm do
      click_on 'remove-schedule'
    end
    expect(page).to have_content('You have not set up a recurring scan for this machine.')
  end
end
