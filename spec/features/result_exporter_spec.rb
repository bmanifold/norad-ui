# frozen_string_literal: true

require 'rails_helper'

feature 'Result Exporter functionality', skip: true do
  scenario 'Export a failing result', js: true do
    skip 'Waiting on method to mock failed results and Jira issue creation in client_test environment.'
  end

  scenario 'Export button is enabled on page load if a result export queue exists', js: true do
    skip 'Waiting on method to mock failed results and Jira issue creation in client_test environment.'
  end

  scenario 'Export button is disabled on page load if a result export queue does not exist', js: true do
    skip 'Waiting on method to mock failed results and Jira issue creation in client_test environment.'
  end
end
