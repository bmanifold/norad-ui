# frozen_string_literal: true

require 'rails_helper'

feature 'Requirement Groups functionality' do
  before :each do
    valid_login
  end

  scenario 'adding and removing a requirement group', js: true do
    name = create_requirement_group_helper

    expect(page).to have_selector('td', text: name)

    click_on 'Remove'
    accept_alert

    expect(page).to have_no_selector('td', text: name)
  end

  scenario 'error displayed when requirement group creation failed', js: true do
    create_requirement_group_helper('')
    expect(page).to have_css('div.alert.alert-danger', text: "Name can't be blank.")
  end

  scenario 'modify a requirement group', js: true do
    name = create_requirement_group_helper
    new_name = "#{name} UPDATED"
    new_description = 'Updated Description'

    td = find('td', text: name)
    td.find('a').click
    fill_in 'requirement_group[name]', with: new_name
    fill_in 'requirement_group[description]', with: new_description
    click_on 'Update'

    visit requirement_groups_path
    td_name = find('td', text: new_name)
    expect(td_name).to be_present
    td_description = td_name.find(:xpath, '..').find('td', text: new_description)
    expect(td_description).to be_present
  end

  scenario 'ensure security tests in requirement groups have docs and Service Requirements', js: true do
    group_name = create_requirement_group_helper
    requirement_name = create_navigating_requirement_helper(group_name)
    add_sec_test_to_requirement_helper(group_name, requirement_name, 'vuls')
    visit requirement_groups_path
    click_on group_name
    find('tr', text: requirement_name.to_s).find('.fa.fa-gear').click
    expect(page).to have_selector(:css, 'a[title="Vuls Docs"]')
    expect(page).to have_text('SSH service')
  end

  scenario 'ensure security tests in organization requirement groups have docs and Service Requirements', js: true do
    org_slug = create_org_helper
    group_name = create_requirement_group_helper
    requirement_name = create_navigating_requirement_helper(group_name)
    add_sec_test_to_requirement_helper(group_name, requirement_name, 'vuls')
    group_regexp = /\A#{group_name}\z/
    visit organization_security_tests_path(org_slug)
    find('.req-group-list ~ span.select2').click
    find('.select2-results').find('li', text: group_regexp).click
    click_on 'Add Requirements'
    find('div', text: group_name, class: 'req-group-name').find('.fa.fa-plus-circle').click
    expect(page).to have_selector(:css, 'a[title="Vuls Docs"]')
    expect(page).to have_text('SSH service')
  end

  scenario 'ensure security tests in organization requirement groups have docs and Service Requirements', js: true do
    org_slug = create_org_helper
    group_name = create_requirement_group_helper
    requirement_name = create_navigating_requirement_helper(group_name)
    add_sec_test_to_requirement_helper(group_name, requirement_name, 'vuls')
    group_regexp = /\A#{group_name}\z/
    visit organization_security_tests_path(org_slug)
    find('.req-group-list ~ span.select2').click
    find('.select2-results').find('li', text: group_regexp).click
    click_on 'Add Requirements'
    find('div', text: group_name, class: 'req-group-name').find('.fa.fa-plus-circle').click
    expect(page).to have_selector(:css, 'a[title="Vuls Docs"]')
    expect(page).to have_text('Authenticated SSH service')
    machine = create_machine_helper(org_slug)
    visit machine_security_tests_path(machine)
    find('div', text: group_name, class: 'req-group-name').find('.fa.fa-plus-circle').click
    expect(page).to have_selector(:css, 'a[title="Vuls Docs"]')
    expect(page).to have_text('Authenticated SSH service')
  end
end
