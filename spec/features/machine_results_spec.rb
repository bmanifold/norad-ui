# frozen_string_literal: true

require 'rails_helper'

feature 'Machine Results functionality', skip: true do
  scenario 'default to results bye status view', js: true do
    skip 'Skipping until test environment provides test results'
  end

  scenario 'sort results by status', js: true do
    skip 'Skipping until test environment provides test results'
  end

  scenario 'sort results by security test', js: true do
    skip 'Skipping until test environment provides test results'
  end

  scenario 'sort results by requirements', js: true do
    skip 'Skipping until test environment provides test results'
  end

  scenario 'ignoring a result', js: true do
    skip 'Skipping until test environment provides test results'
  end

  scenario 'showing / hiding ignored results', js: true do
    skip 'Skipping until test environment provides test results'
  end
end
