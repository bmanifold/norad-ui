# frozen_string_literal: true

require 'rails_helper'

def valid_login(user = 'testuser')
  logout
  visit login_path
  fill_in 'session[uid]', with: user
  fill_in 'session[password]', with: 'goodpass'
  click_on 'sign-in'
end

def create_org(name)
  visit new_organization_path
  fill_in 'organization[uid]', with: name
  click_on 'submit-org'
end

def delete_org(org_slug)
  visit settings_organization_path(org_slug)
  accept_confirm do
    find('a', text: 'Delete Organization').click
  end
end

def make_default_org(org_slug)
  visit organizations_path

  # First find the org name link
  org_link = find("a.org-name[href=\"#{organization_path(org_slug)}\"]")
  # then traverse up to find row and back down to find default button icon
  org_link.find(:xpath, '../../td[1]/i').click
end

def find_org_row(org_slug)
  org_link = find("a.org-name[href=\"#{organization_path(org_slug)}\"]")
  org_link.find(:xpath, '../..')
end

feature 'User Profile Default Org Setting' do
  before :each do
    valid_login
    @org_slug = create_org_helper
    visit organizations_path
  end

  scenario 'set default organization for user', js: true do
    org_row = find_org_row(@org_slug)
    expect(org_row).to have_selector('i.fa.fa-circle-o')

    make_default_org(@org_slug)
    expect(org_row).to have_selector('i.fa.fa-circle')
  end

  scenario 'remove default organization for user', js: true do
    make_default_org(@org_slug)
    org_row = find_org_row(@org_slug)
    expect(org_row).to have_selector('i.fa.fa-circle')

    org_row.find('i.fa.fa-circle').click
    expect(org_row).to have_selector('i.fa.fa-circle-o')
  end

  scenario 'remove default org after deleting organization', js: true do
    make_default_org(@org_slug)
    visit root_path
    expect(current_path).to eq organization_path(@org_slug)

    delete_org(@org_slug)
    visit root_path
    expect(current_path).to eq organizations_path
  end
end
