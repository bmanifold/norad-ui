# frozen_string_literal: true

require 'rails_helper'

feature 'Machine Enforcement functionality' do
  before :each do
    valid_login
    @org_slug = create_org_helper
    @machine = create_machine_helper(@org_slug)
    @req_group = create_configurable_requirement_group
    visit organization_security_tests_path(@org_slug)
    add_requirement_group(@req_group)
    visit machine_security_tests_path(@machine.id)
  end

  define_method :add_requirement_group do |name = 'Cisco CSDL'|
    find('select.req-group-list ~ span.select2').click
    find('.select2-results').find('li', text: name).click
    click_on 'add-requirements'
  end

  define_method :add_relevant_security_tests do
    find('select.security-test-list ~ span.select2').click
    find('.select2-results').find('li', text: /\Avuls\Z/).click
    click_on 'Add Security Tests'
  end

  define_method :create_configurable_requirement_group do
    req_group = create_requirement_group_helper
    td = find('td', text: req_group)
    td.find('a').click

    req = create_requirement_helper
    td = find('td', text: req)
    td.find(:xpath, '..').find('a[title="Requirement Settings"]').click

    add_relevant_security_tests

    visit organization_security_tests_path(@org_slug)
    req_group
  end

  skip 'create config from requirement', js: true do
    skip 'The test environment does not currently have any configurable security tests'
    find('.req-group > .req-group-name > a.req-list-toggle').click

    row = first('.req-list table').first('tbody tr')
    container_name = row.first('td').text
    row.find('a.create-config').click

    expect(page).to have_selector('#enabled-containers td', text: container_name)
    expect(row).to have_selector('.create-config', text: 'Config Created')
  end
end
