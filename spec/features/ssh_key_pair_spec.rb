# frozen_string_literal: true

require 'rails_helper'

feature 'SSH Key Pair functionality' do
  before :each do
    valid_login
    @org_slug = create_org_helper
    visit settings_organization_path(@org_slug)
  end

  let(:valid_ssh_private_key) do
    '-----BEGIN RSA PRIVATE KEY-----
MIIBzAIBAAJhAO6O+guiF3YQWkdBjZpxyM5Ivn0/U78pFwIdK8xITldZEd8ERo9n
1ZW5GWgSwk5qfpxu/9XR4AxdIJPRWBmcWPyPn7/VU2Ahga9bcFZeZ7u7PPhYnW+a
eihlK5pqwKtyzwIDAQABAmEAsLA9Cri3X76k3vnyg12bOQ4v6FwrDv7EB9DLS7aS
L6fM0L099aEWVJqP8sFSIPpF9i9cZfv84tiGFxShgFzTxlkfDASEpTFjXQNgHQY8
zaPfsCOCGYvP1GY9vun9IUVhAjEA/WOVZDsucQLStb/AzgIETRjGyaQSuRP+Hcdx
AkJF+vVVGQ0NZqprm5BPulsmeYfVAjEA8QRFj5BkXAPecMGHUgZc2pnRPAsmUNKc
aDcrRPi+hJ/ycPehhJeoRoy9zq3iqeYTAjEAlTXPAvbWojXHXr9MCgOmBMBSTusP
G21qdp67LVlZXC6breTBv3Mbar/QMyz5uxcBAjBKHS8q05MY/JOVht2O6qf5DN3b
oe4ZYnp4Taa/oeIsNGdAN9XySRSVBUiE2tHdvwMCMQCfBXABop3zntzNun2Gc854
mmeZxJykaYJelEzT3FW8iy9V79Flx+OSU9bWmkmEmno=
-----END RSA PRIVATE KEY-----'
  end

  let(:ssh_private_key_without_header) do
    'MIIBzAIBAAJhAO6O+guiF3YQWkdBjZpxyM5Ivn0/U78pFwIdK8xITldZEd8ERo9n
1ZW5GWgSwk5qfpxu/9XR4AxdIJPRWBmcWPyPn7/VU2Ahga9bcFZeZ7u7PPhYnW+a
eihlK5pqwKtyzwIDAQABAmEAsLA9Cri3X76k3vnyg12bOQ4v6FwrDv7EB9DLS7aS
L6fM0L099aEWVJqP8sFSIPpF9i9cZfv84tiGFxShgFzTxlkfDASEpTFjXQNgHQY8
zaPfsCOCGYvP1GY9vun9IUVhAjEA/WOVZDsucQLStb/AzgIETRjGyaQSuRP+Hcdx
AkJF+vVVGQ0NZqprm5BPulsmeYfVAjEA8QRFj5BkXAPecMGHUgZc2pnRPAsmUNKc
aDcrRPi+hJ/ycPehhJeoRoy9zq3iqeYTAjEAlTXPAvbWojXHXr9MCgOmBMBSTusP
G21qdp67LVlZXC6breTBv3Mbar/QMyz5uxcBAjBKHS8q05MY/JOVht2O6qf5DN3b
oe4ZYnp4Taa/oeIsNGdAN9XySRSVBUiE2tHdvwMCMQCfBXABop3zntzNun2Gc854
mmeZxJykaYJelEzT3FW8iy9V79Flx+OSU9bWmkmEmno=
-----END RSA PRIVATE KEY-----'
  end

  let(:ssh_private_key_without_footer) do
    '-----BEGIN RSA PRIVATE KEY-----
MIIBzAIBAAJhAO6O+guiF3YQWkdBjZpxyM5Ivn0/U78pFwIdK8xITldZEd8ERo9n
1ZW5GWgSwk5qfpxu/9XR4AxdIJPRWBmcWPyPn7/VU2Ahga9bcFZeZ7u7PPhYnW+a
eihlK5pqwKtyzwIDAQABAmEAsLA9Cri3X76k3vnyg12bOQ4v6FwrDv7EB9DLS7aS
L6fM0L099aEWVJqP8sFSIPpF9i9cZfv84tiGFxShgFzTxlkfDASEpTFjXQNgHQY8
zaPfsCOCGYvP1GY9vun9IUVhAjEA/WOVZDsucQLStb/AzgIETRjGyaQSuRP+Hcdx
AkJF+vVVGQ0NZqprm5BPulsmeYfVAjEA8QRFj5BkXAPecMGHUgZc2pnRPAsmUNKc
aDcrRPi+hJ/ycPehhJeoRoy9zq3iqeYTAjEAlTXPAvbWojXHXr9MCgOmBMBSTusP
G21qdp67LVlZXC6breTBv3Mbar/QMyz5uxcBAjBKHS8q05MY/JOVht2O6qf5DN3b
oe4ZYnp4Taa/oeIsNGdAN9XySRSVBUiE2tHdvwMCMQCfBXABop3zntzNun2Gc854
mmeZxJykaYJelEzT3FW8iy9V79Flx+OSU9bWmkmEmno='
  end

  let(:ssh_private_key_with_invalid_characters) do
    # The key below has a '!' on the 4th line around character 20
    '-----BEGIN RSA PRIVATE KEY-----
MIIBzAIBAAJhAO6O+guiF3YQWkdBjZpxyM5Ivn0/U78pFwIdK8xITldZEd8ERo9n
1ZW5GWgSwk5qfpxu/9XR4AxdIJPRWBmcWPyPn7/VU2Ahga9bcFZeZ7u7PPhYnW+a
eihlK5pqwKtyzwIDAQABAmEAsLA9Cri3X76k3vnyg12bOQ4v6FwrDv7EB9DLS7aS
L6fM0L099aEWVJqP8sF!IPpF9i9cZfv84tiGFxShgFzTxlkfDASEpTFjXQNgHQY8
zaPfsCOCGYvP1GY9vun9IUVhAjEA/WOVZDsucQLStb/AzgIETRjGyaQSuRP+Hcdx
AkJF+vVVGQ0NZqprm5BPulsmeYfVAjEA8QRFj5BkXAPecMGHUgZc2pnRPAsmUNKc
aDcrRPi+hJ/ycPehhJeoRoy9zq3iqeYTAjEAlTXPAvbWojXHXr9MCgOmBMBSTusP
G21qdp67LVlZXC6breTBv3Mbar/QMyz5uxcBAjBKHS8q05MY/JOVht2O6qf5DN3b
oe4ZYnp4Taa/oeIsNGdAN9XySRSVBUiE2tHdvwMCMQCfBXABop3zntzNun2Gc854
mmeZxJykaYJelEzT3FW8iy9V79Flx+OSU9bWmkmEmno=
-----END RSA PRIVATE KEY-----'
  end

  let(:import_ssh_key_pair) do
    key = '-----BEGIN RSA PRIVATE KEY-----
MIIBzAIBAAJhAO6O+guiF3YQWkdBjZpxyM5Ivn0/U78pFwIdK8xITldZEd8ERo9n
1ZW5GWgSwk5qfpxu/9XR4AxdIJPRWBmcWPyPn7/VU2Ahga9bcFZeZ7u7PPhYnW+a
eihlK5pqwKtyzwIDAQABAmEAsLA9Cri3X76k3vnyg12bOQ4v6FwrDv7EB9DLS7aS
L6fM0L099aEWVJqP8sFSIPpF9i9cZfv84tiGFxShgFzTxlkfDASEpTFjXQNgHQY8
zaPfsCOCGYvP1GY9vun9IUVhAjEA/WOVZDsucQLStb/AzgIETRjGyaQSuRP+Hcdx
AkJF+vVVGQ0NZqprm5BPulsmeYfVAjEA8QRFj5BkXAPecMGHUgZc2pnRPAsmUNKc
aDcrRPi+hJ/ycPehhJeoRoy9zq3iqeYTAjEAlTXPAvbWojXHXr9MCgOmBMBSTusP
G21qdp67LVlZXC6breTBv3Mbar/QMyz5uxcBAjBKHS8q05MY/JOVht2O6qf5DN3b
oe4ZYnp4Taa/oeIsNGdAN9XySRSVBUiE2tHdvwMCMQCfBXABop3zntzNun2Gc854
mmeZxJykaYJelEzT3FW8iy9V79Flx+OSU9bWmkmEmno=
-----END RSA PRIVATE KEY-----'
    click_on 'add-new-ssh-key-pair'
    fill_in 'ssh_key_pair[name]', with: 'RSA Key'
    fill_in 'ssh_key_pair[description]', with: 'Valid RSA Key Pair'
    fill_in 'ssh_key_pair[username]', with: 'root'
    fill_in 'ssh_key_pair[key]', with: key
    click_on 'save-ssh-key-pair'
  end

  let(:import_invalid_ssh_key_pair) do
    key = '-----BEGIN RSA PRIVATE KEY-----
MIIByQIBAAJhAJRUM0EuLx/GSt9c9rG5M+TJg/RrU2pOcKx78bGeoqRdpUKr2Rmc
a9MpqFMJyrtXzwWyk9h5yD6LBwqodylikbI5RvZIrq8l7zEnL2uWS0nvGDe9aamw
zA2eJgRb/Zt9DwIDAQABAmAqHOncdK9edx8k4bEM7odESs6TI7GwOgsVfa82fdKx
lgDot//QnK+kaWxn/xy3KgunbIegtVZjdE62fOa9puIiZJ1p4PnUqir1ruKNtvaI
PBc0x1wvnIvWhfPFhvr2CqECMQDDGwPdV6Olc3oyHw0j62FVVhXVW5sazuNilD/I
+FPRcJ/bksPsMxliy2HJeexSK1ECMQDCn7hpyqOaeNUic5OeZNMXfoZ96a6j2Hmr
MJo+QdaEVNFdMM/Fp0FlAMUlLQsYSl8CMEe3dHI2NscobXBjDZd4fCy8GgZ1R8xQ
5DBmQhQDg/vmpDw39KCsH9a639UJZh7GIQIwYIzb75+Xigpnsa+ki+94yS77iXtk
J+Q8d7Ck1D3VLdDmDFUqveM7jd/T7Z1q46IBAjA/D5iZcO2w9vt9N2ni7b6+RiBW
-----END RSA PRIVATE KEY-----'
    click_on 'add-new-ssh-key-pair'
    fill_in 'ssh_key_pair[name]', with: 'RSA Key'
    fill_in 'ssh_key_pair[description]', with: 'Valid RSA Key Pair'
    fill_in 'ssh_key_pair[username]', with: 'root'
    fill_in 'ssh_key_pair[key]', with: key
    click_on 'save-ssh-key-pair'
  end

  def enter_ssh_key(key)
    click_on 'add-new-ssh-key-pair'
    fill_in 'ssh_key_pair[name]', with: 'RSA Key'
    fill_in 'ssh_key_pair[description]', with: 'Valid RSA Key Pair'
    fill_in 'ssh_key_pair[username]', with: 'root'
    fill_in 'ssh_key_pair[key]', with: key
  end

  scenario 'add ssh key pair', js: true do
    import_ssh_key_pair
    expect(page).to have_content('Valid RSA Key Pair')
  end

  scenario 'remove ssh key pair', js: true do
    import_ssh_key_pair
    expect(page).to have_content('Valid RSA Key Pair')
    accept_confirm do
      find('.delete-ssh-key-pair').click
    end
    expect(page).not_to have_content('Valid RSA Key Pair')
  end

  scenario 'add invalid ssh key pair', js: true do
    import_invalid_ssh_key_pair
    expect(page).to have_content('Unable to create SSH Key Pair')
  end

  scenario 'assign key pair to machine', js: true do
    import_ssh_key_pair
    machine = create_machine_helper(@org_slug)
    visit settings_machine_path(machine)
    expect(page).to have_button('Assign Key To Machine')

    select('RSA Key', from: 'ssh_key_pair_id')
    click_button 'Assign Key To Machine'

    # close "You have assigned an SSH Key Pair, but no SSH Services are registered for this machine" modal
    find(:css, '[aria-label="Close"]').click

    # users should be able view "fingerprint help" modal
    # to know how we generate Private Key Fingerprints
    find(:css, '[data-target="#fingerprint-command-modal"]').click
    expect(page).to have_content('Command used to generate Private Key Fingerprints')
    expect(page).to have_content('openssl rsa -in <PATH/TO/KEY> -pubout -outform DER | openssl sha256 -c')
    find(:css, '[aria-label="Close"]').click

    expect(page).to have_selector(:link_or_button, 'Unassign Key Pair')
  end

  scenario 'unassign key pair from machine', js: true do
    import_ssh_key_pair
    machine = create_machine_helper(@org_slug)
    visit settings_machine_path(machine)
    expect(page).to have_button('Assign Key To Machine')

    select('RSA Key', from: 'ssh_key_pair_id')
    click_button 'Assign Key To Machine'
    find('.close').click
    expect(page).to have_selector(:link_or_button, 'Unassign Key Pair')

    find("a[href=\"#{machine_ssh_key_pair_assignment_path(machine)}\"]").click
    expect(page).to have_button('Assign Key To Machine')
  end

  scenario 'inputting valid key does not generate error', js: true do
    enter_ssh_key(valid_ssh_private_key)
    expect(page).to_not have_content('Must be a private key for SSH protocol version 2 using the DSA or RSA algorithm.')
  end

  scenario 'inputting key without header generates error message', js: true do
    enter_ssh_key(ssh_private_key_without_header)
    expect(page).to have_content('Must be a private key for SSH protocol version 2 using the DSA or RSA algorithm.')
  end

  scenario 'inputting key without footer generates error message', js: true do
    enter_ssh_key(ssh_private_key_without_footer)
    expect(page).to have_content('Must be a private key for SSH protocol version 2 using the DSA or RSA algorithm.')
  end

  scenario 'inputting key without footer generates error message', js: true do
    enter_ssh_key(ssh_private_key_with_invalid_characters)
    expect(page).to have_content('Must be a private key for SSH protocol version 2 using the DSA or RSA algorithm.')
  end
end
