# frozen_string_literal: true

if Rails.env.production?
  # Add Errbit
  Airbrake.configure do |config|
    config.host = ENV.fetch('ERRBIT_URL')
    config.project_id = 1
    config.project_key = ENV.fetch('AIRBRAKE_API_KEY')
    config.app_version = ENV['NORAD_VERSION']
    # if we ever introduce a staging environment, this variable will be useful.
    config.environment = Rails.env
  end
  Airbrake.add_filter do |notice|
    blocked_notice = notice[:errors].any? do |error|
      error[:type] == 'ActiveRecord::RecordNotFound' ||
        error[:type] == 'ActionController::RoutingError'
    end
    notice.ignore! if blocked_notice
  end
end
