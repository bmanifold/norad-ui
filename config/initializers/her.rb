# frozen_string_literal: true

class NoradApiAuthentication < Faraday::Middleware
  def call(env)
    env[:request_headers]['Authorization'] = "Token token=#{RequestStore.store[:api_token]}"
    env[:request_headers]['HTTP-NORAD-UI-SECRET'] = ENV['NORAD_UI_SECRET'] if ENV['SSO_DOMAIN'].present?
    @app.call(env)
  end
end

module NoradApiExceptions
  class NoradError < StandardError; end

  # 400
  class BadRequestError < NoradError; end

  # 401
  class UnauthorizedError < NoradError; end

  # 403
  class ForbiddenError < NoradError; end

  # 404
  class NotFoundError < NoradError; end

  # 409
  class Conflict < NoradError; end

  # 500
  class ServerError < NoradError; end
end

class NoradApiParser < Faraday::Response::Middleware
  def parse(body)
    json = MultiJson.load(body, symbolize_keys: true)
    {
      data: json[:response] || {},
      # Her expects errors to be an array
      errors: json[:errors].to_a,
      metadata: json[:metadata] || {}
    }
  end

  # rubocop:disable Metrics/MethodLength, Metrics/CyclomaticComplexity
  def on_complete(env)
    env[:body] = case env[:status]
                 when 204
                   parse('{}')
                 when 400
                   raise NoradApiExceptions::BadRequestError, 'Invalid request'
                 when 401
                   raise NoradApiExceptions::UnauthorizedError, 'Unauthorized'
                 when 403
                   raise NoradApiExceptions::ForbiddenError, 'Forbidden'
                 when 404
                   raise NoradApiExceptions::NotFoundError, 'Not found'
                 when 409
                   raise NoradApiExceptions::Conflict, 'Conflict'
                 when 500
                   raise NoradApiExceptions::ServerError, 'Norad API returned a 500 error'
                 else
                   parse(env[:body])
                 end
  end
  # rubocop:enable Metrics/MethodLength, Metrics/CyclomaticComplexity
end

Her::API.setup url: ENV['API_URL'], ssl: { verify: false } do |c|
  # Request
  c.use NoradApiAuthentication
  c.use Faraday::Request::UrlEncoded
  c.use Her::Middleware::AcceptJSON

  # Response
  c.use NoradApiParser

  # Adapter
  c.use Faraday::Adapter::NetHttp
end
