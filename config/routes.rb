# frozen_string_literal: true

# rubocop:disable BlockLength
Rails.application.routes.draw do
  constraints(proc { AuthStrategy.reverse_proxy_auth? }) do
    get 'login'  => 'reverse_proxy_sessions#new'
    post 'login' => 'reverse_proxy_sessions#create'
    delete 'logout' => 'reverse_proxy_sessions#destroy'

    resources :users, only: AuthStrategy.reverse_proxy_user_routes do
      resource :api_token, only: :update
    end
  end
  constraints(proc { AuthStrategy.local_auth? }) do
    get 'login'  => 'local_sessions#new'
    post 'login' => 'local_sessions#create'
    delete 'logout' => 'local_sessions#destroy'

    resources :users, only: AuthStrategy.local_user_routes do
      resource :api_token, only: :update
    end
  end

  resources :organizations do
    resources :result_ignore_rules, only: %i[create edit update destroy]
    resources :machines, only: %i[create index new]
    resources :memberships, only: [:create]
    resource :organization_token, only: :update
    resources :relays, controller: 'docker_relays', only: [:index]
    resources :scan_schedules, only: %i[index create new], controller: :organization_scan_schedules
    resources :security_tests, controller: :organization_security_tests, only: %i[index create]
    resources :enforcements, controller: :organization_enforcements, only: [:create]
    resource :iaas_configuration, only: %i[show create]
    resources :ssh_key_pairs, only: %i[index create new]
    resources :result_export_queues, only: %i[new]
    resources :jira_export_queues, only: %i[new create]
    resources :infosec_export_queues, only: %i[new create]
    collection do
      post 'default'
    end
    member do
      post 'scan'
      get 'results'
      get 'history'
      get 'settings'
    end
  end

  get '/machines/:id', to: redirect('/machines/%{id}/results?scan_id=latest')
  resources :machines, only: %i[edit update destroy] do
    resources :ping_connectivity_checks, only: %i[create show]
    resources :ssh_connectivity_checks, only: %i[create show]
    resources :result_ignore_rules, only: %i[create edit update destroy]
    resources :scan_schedules, only: %i[create new], controller: :machine_scan_schedules
    resources :security_tests, controller: :machine_security_tests, only: %i[index create]
    resources :services, only: %i[index new create update edit]
    resources :service_discoveries, only: %i[index create]
    resource :ssh_key_pair_assignment, only: %i[create destroy]
    member do
      post 'scan'
      get 'results'
      get 'history'
      get 'settings'
    end
  end

  resources :docker_commands, only: :destroy do
    resources :assessments, only: :index
  end
  resources :result_ignore_rules, only: :destroy
  resources :result_exporters, only: :create
  resources :security_container_configs, only: %i[show update destroy]
  resources :relays, controller: 'docker_relays', only: %i[update destroy]
  resources :repository_security_containers, only: :destroy
  resources :organization_configurations, only: [:update]
  resources :memberships, only: [:destroy]
  resources :user_profiles, only: [:update]
  resources :organization_scan_schedules, only: %i[update destroy edit]
  resources :machine_scan_schedules, only: %i[update destroy edit]
  resources :enforcements, controller: :organization_enforcements, only: [:destroy]
  resources :iaas_configurations, only: %i[show update] do
    resources :iaas_discoveries, only: :create
  end
  resources :services, only: %i[show destroy]
  resources :ssh_key_pairs, only: [:destroy]
  resources :notification_channels, only: :update
  resources :service_discoveries, only: :show
  resources :security_test_repositories, except: :edit do
    resources :repository_security_containers, only: %i[new create]
    resources :repository_whitelist_entries, only: %i[create destroy]
    resources :repository_members, only: :create
  end

  resources :repository_members, only: :destroy

  resources :requirement_groups, except: %i[edit] do
    resources :requirements, only: [:create]
  end
  resources :requirements, only: %i[edit update destroy] do
    resources :provisions, only: %i[index create]
  end
  resources :password_resets, only: %i[new create edit update], param: :provided_token
  resources :email_confirmations, only: :show, param: :email_confirmation_token
  resources :provisions, only: :destroy
  resources :jira_export_queues, only: %i[edit update destroy]
  resources :infosec_export_queues, only: %i[edit update destroy]

  root to: 'home#index'
end
# rubocop:enable BlockLength
