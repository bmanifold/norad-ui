const { environment } = require('@rails/webpacker')
const webpack = require('webpack')


const ExtractTextPlugin = environment.plugins.get('ExtractText')

environment.loaders.set('less', {
  test: /\.less$/i,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      { loader: 'css-loader', options: { minimize: process.env.NODE_ENV === 'production' } },
      { loader: 'postcss-loader', options: { sourceMap: true } },
      'resolve-url-loader',
      { loader: 'less-loader', options: { sourceMap: true } }
    ]
  })
})

environment.loaders.set('jquery', {
  test: require.resolve('jquery'),
  use: [
    {
      loader: 'expose-loader',
      options: '$'
    },
    {
      loader: 'expose-loader',
      options: 'jQuery'
    }
  ]
})

module.exports = environment
